// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "OptiXGlassDefinitions.h"

#include "OpticalBench423GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class OPTICALBENCH423_API AOpticalBench423GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOpticalBench423GameModeBase(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage) override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<FSceneData>& GetOptiXSceneDataArray();

};

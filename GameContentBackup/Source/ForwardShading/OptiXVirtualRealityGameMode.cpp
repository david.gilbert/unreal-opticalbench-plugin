// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXVirtualRealityGameMode.h"

#include "OptiXModule.h"
#include "OptiXContextManager.h"

AOptiXVirtualRealityGameMode::AOptiXVirtualRealityGameMode(const FObjectInitializer& ObjectInitializer)
	: Super()
{
	PlayerControllerClass = AOptiXPlayerController::StaticClass();
	//DefaultPawnClass = ADefaultPawn::StaticClass();
}


void AOptiXVirtualRealityGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	// Init the context
	if (GetWorld() == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("GetWorld was null in gamemode"));
	}
	FOptiXModule::Get().Init();
}

void AOptiXVirtualRealityGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	//FOptiXModule::Get().GetOptiXContextManager()->EndPlay();
}
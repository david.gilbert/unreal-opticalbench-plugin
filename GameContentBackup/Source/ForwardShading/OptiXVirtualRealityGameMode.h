// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// RWTH VR
#include "VirtualRealityGameMode.h"

// OptiX
#include "OptiXContext.h"
#include "OptiXAcceleration.h"
#include "OptiXGroup.h"
#include "OptiXPlayerController.h"

#include "OptiXVirtualRealityGameMode.generated.h"

/**
 * 
 */
UCLASS()
class FORWARDSHADING_API AOptiXVirtualRealityGameMode : public AVirtualRealityGameMode
{
	GENERATED_BODY()

public:
	AOptiXVirtualRealityGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY()
	UOptiXContext* OptiXContext;

	UPROPERTY()
	UOptiXAcceleration* TopAcceleration;

	UPROPERTY()
	UOptiXGroup* TopObject;	
};

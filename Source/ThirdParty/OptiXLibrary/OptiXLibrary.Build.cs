using System.IO;
using UnrealBuildTool;


public class OptiXLibrary : ModuleRules
{
    public OptiXLibrary(ReadOnlyTargetRules Target) : base(Target)
    {
        Type = ModuleType.External;


        // TODO: Look at the Blast Plugin and copy dlls/libs/includes from local filesystem/install dirs instead of shipping them with the plugin!


        PublicDefinitions.Add("WITH_OPTIX=1");

        //PublicDependencyModuleNames.AddRange(
        //    new string[]
        //    {
        //        "CUDALibrary",
        //    }
        //    );


        string IncludeDir = ModuleDirectory + "/include";
        string BaseLibDir = ModuleDirectory + "/lib";
        string BaseBinDir = ModuleDirectory + "/../../../Binaries/ThirdParty";

        PublicIncludePaths.Add(IncludeDir);

        // For some reason we need the CUDA headers explicitly - the other gameworks plugins have those baked in somehow.
        // PublicIncludePaths.Add(IncludeDir + "/cuda/include");

        PublicSystemIncludePaths.Add(IncludeDir);
        //PublicSystemIncludePaths.Add(IncludeDir + "/cuda/");

        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            // Needed defines - WIP

            PublicDefinitions.Add("NOMINMAX");

            // OptiX
            //PublicLibraryPaths.Add(BaseLibDir + "/win64-old");
            //PublicAdditionalLibraries.Add("optix.51.lib");
            //PublicDelayLoadDLLs.Add("optix.51.dll");

            //// OptiX Prime
            //PublicLibraryPaths.Add(BaseLibDir + "/win64-old");
            //PublicAdditionalLibraries.Add("optix_prime.1.lib");
            //PublicDelayLoadDLLs.Add("optix_prime.1.dll");

            //// OptiXU

            //PublicLibraryPaths.Add(BaseLibDir + "/win64-old");
            //PublicAdditionalLibraries.Add("optixu.1.lib");
            //PublicDelayLoadDLLs.Add("optixu.1.dll");




            //// OptiX
            PublicLibraryPaths.Add(BaseLibDir + "/win64");
            PublicAdditionalLibraries.Add("optix.6.5.0.lib");
            PublicDelayLoadDLLs.Add("optix.6.5.0.dll");

            // OptiX Prime
            //PublicLibraryPaths.Add(BaseLibDir + "/win64");
            PublicAdditionalLibraries.Add("optix_prime.6.5.0.lib");
            PublicDelayLoadDLLs.Add("optix_prime.6.5.0.dll");

            // OptiXU

            //PublicLibraryPaths.Add(BaseLibDir + "/win64");
            PublicAdditionalLibraries.Add("optixu.6.5.0.lib");
            PublicDelayLoadDLLs.Add("optixu.6.5.0.dll");
            //PublicDelayLoadDLLs.Add("cudnn64_7.dll");



            // CUDA

            //PublicAdditionalLibraries.Add("cublas.lib");
            //PublicAdditionalLibraries.Add("cublasLt.lib");
            ////PublicAdditionalLibraries.Add("cublas_device.lib");
            //PublicAdditionalLibraries.Add("cuda.lib");
            //PublicAdditionalLibraries.Add("cudadevrt.lib");
            //PublicAdditionalLibraries.Add("cudart.lib");
            //PublicAdditionalLibraries.Add("cudart_static.lib");
            //PublicAdditionalLibraries.Add("cufft.lib");
            //PublicAdditionalLibraries.Add("cufftw.lib");
            //PublicAdditionalLibraries.Add("curand.lib");
            //PublicAdditionalLibraries.Add("cusolver.lib");
            //PublicAdditionalLibraries.Add("cusparse.lib");
            //PublicAdditionalLibraries.Add("nppc.lib");
            //PublicAdditionalLibraries.Add("nppial.lib");
            //PublicAdditionalLibraries.Add("nppicc.lib");
            //PublicAdditionalLibraries.Add("nppicom.lib");
            //PublicAdditionalLibraries.Add("nppidei.lib");
            //PublicAdditionalLibraries.Add("nppif.lib");
            //PublicAdditionalLibraries.Add("nppig.lib");
            //PublicAdditionalLibraries.Add("nppim.lib");
            //PublicAdditionalLibraries.Add("nppist.lib");
            //PublicAdditionalLibraries.Add("nppisu.lib");
            //PublicAdditionalLibraries.Add("nppitc.lib");
            //PublicAdditionalLibraries.Add("npps.lib");
            //PublicAdditionalLibraries.Add("nvblas.lib");
            //PublicAdditionalLibraries.Add("nvgraph.lib");
            //PublicAdditionalLibraries.Add("nvml.lib");
            //PublicAdditionalLibraries.Add("nvrtc.lib");
            //PublicAdditionalLibraries.Add("OpenCL.lib");

            //PublicDelayLoadDLLs.Add("cublas64_10.dll");
            //PublicDelayLoadDLLs.Add("cublasLt64_10.dll");
            //PublicDelayLoadDLLs.Add("cudart64_101.dll");
            //PublicDelayLoadDLLs.Add("cufft64_10.dll");
            //PublicDelayLoadDLLs.Add("cufftw64_10.dll");
            //PublicDelayLoadDLLs.Add("cuinj64_101.dll");
            //PublicDelayLoadDLLs.Add("curand64_10.dll");
            //PublicDelayLoadDLLs.Add("cusolver64_10.dll");
            //PublicDelayLoadDLLs.Add("cuparse64_10.dll");
            //PublicDelayLoadDLLs.Add("nppc64_10.dll");
            //PublicDelayLoadDLLs.Add("nppial64_10.dll");
            //PublicDelayLoadDLLs.Add("nppicc64_10.dll");
            //PublicDelayLoadDLLs.Add("nppicom64_10.dll");
            //PublicDelayLoadDLLs.Add("nppidei64_10.dll");
            //PublicDelayLoadDLLs.Add("nppif64_10.dll");
            //PublicDelayLoadDLLs.Add("nppig64_10.dll");
            //PublicDelayLoadDLLs.Add("nppim64_10.dll");
            //PublicDelayLoadDLLs.Add("nppist64_10.dll");
            //PublicDelayLoadDLLs.Add("nppisu64_10.dll");
            //PublicDelayLoadDLLs.Add("nppitc64_10.dll");
            //PublicDelayLoadDLLs.Add("npps64_10.dll");
            //PublicDelayLoadDLLs.Add("nvblas64_10.dll");
            //PublicDelayLoadDLLs.Add("nvgraph64_10.dll");
            //PublicDelayLoadDLLs.Add("nvrtc64_101_0.dll");
            //PublicDelayLoadDLLs.Add("nvrtc-builtins64_101.dll");

        }

        string BinariesDir = BaseBinDir + "/Win64/";

        string[] RuntimeDependenciesX64 =
            {
                "optix.6.5.0.dll",
                "optix_prime.6.5.0.dll",
                "optixu.6.5.0.dll",
                "cudart64_101.dll"
                //"optix_denoiser.6.0.0.dll",
                //"optix_ssim_predictor.6.0.0.dll",
                //"cudnn64_7.dll"
                //"optix.51.dll",
                //"optix_prime.1.dll",
                //"optixu.1.dll",
                //"avcodec-56.dll",
                //"avfilter-5.dll",
                //"avformat-56.dll",
                //"avutil-54.dll",
                //"cudart64_90.dll",
                //"cudnn64_7.dll",
                //"ffmpeg_video_decoder.dll",
                //"libdice.dll",
                //"nv_freeimage.dll",
                //"nvcuvid_video_decoder.dll",
                //"optix_denoiser.51.dll",
                //"screen_video.dll",
                //"swresample-1.dll",
                //"swscale-3.dll",
            };
        foreach (string RuntimeDependency in RuntimeDependenciesX64)
        {
            RuntimeDependencies.Add(BinariesDir + RuntimeDependency);
        }

    }
}
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXDeclarations.h"
#include "OptiXBuffer.h"

#include "OptiXTextureSampler.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginTextureSampler, Log, All);



UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXTextureSampler : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	optix::TextureSampler GetNativeTextureSampler()
	{
		return NativeTextureSampler;
	}

	void SetTextureSampler(optix::TextureSampler S)
	{
		NativeTextureSampler = S;
	}

	void DestroyOptiXObject();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void Validate();
	
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetMipLevelCount(uint8 NumMipLevels);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	uint8 GetMipLevelCount();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetArraySize(int32 NumTexturesInArray);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	int32 GetArraySize();

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetWrapMode(int32 Dim, RTwrapmode Wrapmode);

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	RTwrapmode GetWrapMode(int32 Dim);

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetFilteringModes(RTfiltermode Minification, RTfiltermode Magnification, RTfiltermode Mipmapping);

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void GetFilteringModes(RTfiltermode& Minification, RTfiltermode& Magnification, RTfiltermode& Mipmapping);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetMaxAnisotropy(float Value);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	float GetMaxAnisotropy();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetMipLevelClamp(float Min, float Max);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	FVector2D GetMipLevelClamp();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetMipLevelBias(float Value);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	float GetMipLevelBias();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	int32 GetId();

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetReadMode(RTtexturereadmode Readmode);

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	RTtexturereadmode GetReadMode();

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetIndexingMode(RTtextureindexmode Mode);

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	RTtextureindexmode GetIndexingMode();

	// Damn blueprints don't allow overloading :|
	// TODO Look into this and how it's actually used.
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetBufferWithTextureIndexAndMiplevel(int32 TextureArrayIndex, int MipLevel, UOptiXBuffer* Buffer);

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	UOptiXBuffer* GetBufferByTextureIndexAndMiplevel(int32 TextureArrayIndex, int MipLevel);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	void SetBuffer(UOptiXBuffer* Buffer);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTextureSampler")
	UOptiXBuffer* GetBuffer();

protected:

	optix::TextureSampler NativeTextureSampler;

	UPROPERTY()
	UOptiXBuffer* OptiXBuffer;

};
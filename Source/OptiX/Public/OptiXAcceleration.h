#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXAcceleration.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginAcceleration, Log, All);

UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXAcceleration : public UObject
{
	GENERATED_BODY()

public:

	virtual void BeginDestroy() override;

	optix::Acceleration GetNativeAcceleration()
	{
		return NativeAcceleration;
	}

	void SetNativeAcceleration(optix::Acceleration A)
	{
		NativeAcceleration = A;
	}

	void DestroyOptiXObject();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		void Validate();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		void MarkDirty();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		bool IsDirty();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		void SetProperty(FString Name, FString Value);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		FString GetProperty(FString Name);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		void SetBuilder(FString Builder);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		FString GetBuilder();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		void SetTraverser(FString Traverser);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXAcceleration")
		FString GetTraverser();

	RTsize GetDataSize();
	void GetData(void* Data);
	void SetData(void* Data, RTsize Size);

protected:

	optix::Acceleration NativeAcceleration;

};

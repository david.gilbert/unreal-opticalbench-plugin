// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"

#include "OptiXTargetComponent.h"

#include "OptiXTargetActor.generated.h"

/**
 * 
 */
UCLASS()
class OPTIX_API AOptiXTargetActor : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	AOptiXTargetActor(const FObjectInitializer& ObjectInitializer);

	//virtual void BeginPlay() override;
	//virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiX")
	UOptiXTargetComponent* OptiXTargetComponent;
};


#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXProgram.generated.h"
/*

Simple Program wrapper, should move to own file.
Needs some functionality!

*/

UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXProgram : public UObject
{
	GENERATED_BODY()
public:

	void SetProgram(optix::Program Prog)
	{
		NativeProgram = Prog;
	}

	optix::Program GetNativeProgram()
	{
		return NativeProgram;
	}

	virtual void BeginDestroy() override
	{
		UE_LOG(LogTemp, Warning, TEXT("OptiX Program BeginDestroy %s"), *this->GetFullName());
		//DestroyOptiXObject();


		//void UOptiXProgram::DestroyOptiXObject()
		//{
		//	if (NativeProgram != NULL)
		//	{
		//		FOptiXModule::Get().GetOptiXContextManager()->QueueNativeObjectForDestruction(NativeProgram);
		//		//NativeProgram->destroy();
		//	}

		//	NativeProgram = NULL;
		//}


		Super::BeginDestroy();
	}

	//void DestroyOptiXObject();



protected:

	optix::Program NativeProgram;

};
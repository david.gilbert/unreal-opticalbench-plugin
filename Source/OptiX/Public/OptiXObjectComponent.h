# pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Components/PrimitiveComponent.h"
#include "Runtime/Engine/Classes/Components/SceneCaptureComponentCube.h"

#include "OptiXContext.h"
#include "OptiXComponentInterface.h"

#include "OptiXObjectComponent.generated.h"


// The interface is sadly useless as unreal doesn't let me do what's possible in standard c++.

UCLASS(abstract) //Blueprintable, hidecategories = (Object), meta=(BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXObjectComponent : public USceneComponent, public IOptiXComponentInterface
{
	GENERATED_BODY()

public:

	// UObject interface

	UOptiXObjectComponent(const FObjectInitializer& ObjectInitializer);


	virtual void OnUnregister() override;

	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	// End of UObject interface

	// Begin UActorComponent interface
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	// End UActorComponent interface

	// USceneComponent interface
	virtual void OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport = ETeleportType::None) override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	void InitOptiXComponent(FRHICommandListImmediate & RHICmdList) override;

	virtual void RegisterOptiXComponent() override;

	virtual void QueueOptiXContextUpdate() override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void CleanOptiXComponent() override;

	virtual void SetUpdateQueued(bool UpdateQueued) override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void UpdateOptiXComponent() override {};

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGeometry() override {};

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXMaterial() override {};

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGroups() override {};

	FString OptiXPTXDir;

public:

	UPROPERTY(BlueprintReadOnly)
	UOptiXContext* OptiXContext;

	UPROPERTY(BlueprintReadOnly)
	UOptiXGeometryGroup* OptiXGeometryGroup;

	UPROPERTY(BlueprintReadOnly)
	UOptiXGeometryInstance* OptiXGeometryInstance;

	UPROPERTY(BlueprintReadOnly)
	UOptiXGeometry* OptiXGeometry;

	UPROPERTY(BlueprintReadOnly)
	UOptiXMaterial* OptiXMaterial;

protected:

	UPROPERTY(BlueprintReadOnly)
	bool bIsInitialized = false;

	FThreadSafeBool bUpdateQueued = false;
};


UCLASS(abstract) //Blueprintable, hidecategories = (Object), meta=(BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXCubemapComponent : public USceneCaptureComponentCube, public IOptiXComponentInterface
{
	GENERATED_BODY()

public:

	// UObject interface

	UOptiXCubemapComponent(const FObjectInitializer& ObjectInitializer);


	virtual void OnUnregister() override;

	// End of UObject interface

	// Begin UActorComponent interface
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	// End UActorComponent interface

	// USceneComponent interface
	virtual void OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport = ETeleportType::None) override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	void InitOptiXComponent(FRHICommandListImmediate & RHICmdList) override;


	virtual void RegisterOptiXComponent() override;

	virtual void QueueOptiXContextUpdate() override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void CleanOptiXComponent() override;

	virtual void SetUpdateQueued(bool UpdateQueued) override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void UpdateOptiXComponent() override {};

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGeometry() override {};

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXMaterial() override {};

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGroups() override {};

	virtual void InitCubemap(FRHICommandListImmediate & RHICmdList) {};
	virtual void UpdateCubemap(FRHICommandListImmediate & RHICmdList) {};


	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	void RequestCubemapUpdate();


	FString OptiXPTXDir;

public:

	UPROPERTY(BlueprintReadOnly)
	UOptiXContext* OptiXContext;

	UPROPERTY(BlueprintReadOnly)
	UOptiXGeometryGroup* OptiXGeometryGroup;

	UPROPERTY(BlueprintReadOnly)
	UOptiXGeometryInstance* OptiXGeometryInstance;

	UPROPERTY(BlueprintReadOnly)
	UOptiXGeometry* OptiXGeometry;

	UPROPERTY(BlueprintReadOnly)
	UOptiXMaterial* OptiXMaterial;

	UPROPERTY(BlueprintReadOnly)
	int32 OptiXCubemapId;

	// Cubemap Unreal Stuff

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UTextureRenderTargetCube* CubeRenderTarget;

	FThreadSafeBool bCubemapCaptured = false;

protected:

	UPROPERTY(BlueprintReadOnly)
	bool bIsInitialized = false;

	FThreadSafeBool bUpdateQueued = false;
};





// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"

#include "OptiXContext.h"

#include "OptiXLaserComponent.generated.h"


UENUM(BlueprintType)
enum class EPatternTypes : uint8
{
	CROSS	UMETA(DisplayName = "Cross"),
	POINTER	UMETA(DisplayName = "Pointer"),
	CIRCLE	UMETA(DisplayName = "Circle"),
	QUAD	UMETA(DisplayName = "Quad"),
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OPTIX_API UOptiXLaserComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOptiXLaserComponent();
	~UOptiXLaserComponent()
	{
		CleanOptiXObjects();
	}

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	

	virtual void OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport = ETeleportType::None) override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void CleanOptiXObjects()
	{
		//FOptiXModule::Get().GetOptiXContextManager()->bLaserIsInitialized.AtomicSet(true);

		UE_LOG(LogTemp, Warning, TEXT("OptiX Laser Component Cleaning up")); // TODO
	}

	void UpdateOptiXContextVariables();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void Init();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void UpdateLaserPosition();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void SetRayTIR(bool Active);

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	bool GetRayTIR() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void SetTargetBufferWrite(bool Flag); // Top naming...

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	bool GetTargetBufferWrite() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void SetTargetColorMode(bool Flag);

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	bool GetTargetColorMode() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void SetWavelength(float WL);

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	float GetWavelength() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void SetLaserWidth(float Width);

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	float GetLaserWidth() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void SetLaserPattern(EPatternTypes Pattern);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	void PreparePatternChange(EPatternTypes Pattern);

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLaserComponent")
	EPatternTypes GetLaserPattern() const;

public:

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UOptiXContext* OptiXContext;	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	int32 LaserMaxDepth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	int32 LaserBufferSize;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	int32 LaserBufferWidth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	int32 LaserBufferHeight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	int32 LaserTracesPerFrame;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	int32 LaserEntryPoint;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	TArray<int32> LaserIndices;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	TArray<FColor> LaserIndexColorMap;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = OptiX)
	TMap<EPatternTypes, UTexture2D*> Patterns;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = OptiX)
	TMap<EPatternTypes, UTexture2D*> PatternDirections;

	TArray<FString> PatternNames = { "Cross", "Pointer", "Circle", "Quad" };

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter = GetRayTIR, BlueprintSetter = SetRayTIR) // todo
	bool RayTIR;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter = GetTargetBufferWrite, BlueprintSetter = SetTargetBufferWrite) // todo
	bool TargetBufferWrite;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter = GetTargetColorMode, BlueprintSetter = SetTargetColorMode) // todo
	bool TargetColorMode;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter = GetWavelength, BlueprintSetter = SetWavelength) // todo
	float Wavelength;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter = GetLaserWidth, BlueprintSetter = SetLaserWidth) // todo
	float LaserWidth;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter = GetLaserPattern, BlueprintSetter = SetLaserPattern) // todo
	EPatternTypes CurrentLaserPattern;

private:
	FThreadSafeBool bUpdateQueued = true;	
	FThreadSafeBool bPatternChanged = true;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/SceneCaptureComponent2D.h"
#include "OptiXVRPawn.generated.h"



UCLASS()
class OPTIX_API AOptiXVRPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AOptiXVRPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void UpdateTranslation(UPrimitiveComponent* Interaction);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	FVector GetDistanceVector(UPrimitiveComponent* Other);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	UStaticMeshComponent* GetNearestMeshComponent(UPrimitiveComponent* Other);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	AActor* GetActorNearHand(UPrimitiveComponent* Hand);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	UMaterialInstanceDynamic* GetMIDOrtho();
	
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void RequestOrthoPass(const FMinimalViewInfo& ViewInfo);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void CaptureDeferredHelper(USceneCaptureComponent2D* SceneCapture);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void UIEventTranslation();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void UIEventRotation();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void UIEventDeselect();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void UIEventDelete();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void OnOverlapBeginWithLever(UPrimitiveComponent* Lever);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void OnOverlapEndWithLever(UPrimitiveComponent* Lever);

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* GrabbedLever;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float GrabDistanceX = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float GrabDistanceY = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float GrabDistanceZ = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Damping = 0.1f;
};

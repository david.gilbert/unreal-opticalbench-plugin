#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXAcceleration.h"
#include "OptiXTransform.h"
#include "OptiXGeometryInstance.h"
#include "OptiXGeometryGroup.h"

#include "OptiXGroup.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginGroup, Log, All);


UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXGroup : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	optix::Group GetNativeGroup()
	{
		return NativeGroup;
	}

	void SetNativeGroup(optix::Group Group)
	{
		NativeGroup = Group;
	}

	void DestroyOptiXObject();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	void SetAcceleration(UOptiXAcceleration* Accel);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	UOptiXAcceleration* GetAcceleration();


	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	void SetChildCount(uint8 Count);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	uint8 GetChildCount();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	void SetChild(uint8 Index, UObject* Child);

	RTobjecttype GetChildType(uint8 Index);
	
	////// Adds the material and returns the index to the added material.
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	UObject* GetChild(uint8 Index);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	uint8 AddChild(UObject* Child);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	uint8 RemoveChild(UObject* Child);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	void RemoveChildByIndex(uint8 Index);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGroup")
	uint8 GetChildIndex(UObject* Child);


	UPROPERTY(BlueprintReadOnly, Category = "OptiXGroup")
	int32 ChildCount = 0;

	// TODO Set up some nice inheritance so we don't need to use UObject here but maybe UOptiXObject
	// or some kind of interface.
	UPROPERTY()
	TArray<UObject*> Children;

	UPROPERTY()
	UOptiXAcceleration* Acceleration;

protected:

	optix::Group NativeGroup;


};
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXGeometry.h"
#include "OptiXGeometryInstance.h"
#include "OptiXGeometryGroup.h"
#include "OptiXMaterial.h"
#include "OptiXBuffer.h"
#include "OptiXProgram.h"
#include "OptiXTextureSampler.h"
#include "OptiXAcceleration.h"
#include "OptiXTransform.h"
#include "OptiXGroup.h"


#include "OptiXContext.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginContext, Log, All);


UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXContext : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

	// We can have a lot of functions here which can interact with OptiX. 
	// As mentioned in the OptiXObjects file, it might be really worth to keep those in the backend (FOptiXContextInstance e.g.)
	// The real issue is kind of the nature of the optix framework, as the variables can be completely arbitrary and dependent on the
	// shaders. Therefore there's no fixed set of UPROPERTYs which could act as an interface.
	// I *could* write another layer here which caches the variables in a TArray and then pushes them through, but this would require 
	// a lot of type-magic as the variables can be any type pretty much.

	// Setter Functions - apparently overloading isn't actually possible so I need to restructure EVERYTHING again
	// Only do the important ones for now as I might need to scrap that again.
public:

	UOptiXContext(const FObjectInitializer& ObjectInitializer);

	// Unreal Constructors are scary, evil and do their own magic in the background.
	// Use Init functions for everything until one day I maybe understand what Unreal is doing exactly...
	optix::Context Init();

	virtual void BeginDestroy() override;

	// Called by the rendering thread to savely update the variables that need to be updated each tick.
	// This is mostly just going to be the transforms.
	void UpdateVariables();

	// TODO Begin play?

	// TODO 
	optix::Context GetNativeContext()
	{
		return NativeContext;
	}

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void Reset();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXGeometry* CreateGeometry();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXMaterial* CreateMaterial();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXGeometryInstance* CreateGeometryInstance(UOptiXGeometry* Geometry, TArray<UOptiXMaterial*> Materials);

	UOptiXGeometryInstance* CreateGeometryInstance(UOptiXGeometry* Geometry, UOptiXMaterial* Material);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXGeometryGroup* CreateGeometryGroup();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXGroup* CreateGroup();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXTransform* CreateTransform();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXAcceleration* CreateAcceleration(FString Builder);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetFloat(FString string, float Var);

	//void SetFloat2D(FString string, FVector2D Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetFloat2D(FString string, float Var1, float Var2);

	// 3D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetFloat3DVector(FString string, FVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetFloat3D(FString string, float Var1, float Var2, float Var3);

	// 4D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetFloat4DVector(FString string, FVector4 Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4);


	// 1D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetInt(FString string, int32 Var);

	// 2D Int - wow Unreal your naming scheme is really top notch...
	//void SetInt2D(FString string, FIntPoint Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetInt2D(FString string, int32 Var1, int32 Var2);

	// 3D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetInt3DVector(FString string, FIntVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3);

	// 4D Int
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	//void SetInt4DVector(FString string, FIntVector4 Var);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetInt4D(FString string, int32 Var1, int32 Var2, int32 Var3, int32 Var4);

	// 1D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetUint(FString string, uint8 Var);

	// 2D Uint - wow Unreal your naming scheme is really top notch...
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetUint2D(FString string, uint8 Var1, uint8 Var2);

	// 3D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetUint3D(FString string, uint8 Var1, uint8 Var2, uint8 Var3);

	// 4D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetUint4D(FString string, uint8 Var1, uint8 Var2, uint8 Var3, uint8 Var4);


	// Matrices - careful Unreal uses ROW_MAJOR LAYOUT
	// TODO They will be a pain

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetMatrix(FString string, FMatrix Matrix);


	////
	//// Getters (gotta love wrappers)
	////

	// Can't overload return type so this is going to be ugly

	// Floats
	// 1D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	float GetFloat(FString string);

	// 2D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	FVector2D GetFloat2D(FString string);

	// 3D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	FVector GetFloat3D(FString string);

	// 4D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	FVector4 GetFloat4D(FString string);


	//// Ints

	// 1D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	int32 GetInt(FString string);

	// 2D Int - wow Unreal your naming scheme is really top notch...
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	FIntPoint GetInt2D(FString string);

	// 3D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	FIntVector GetInt3D(FString string);

	// 4D Int
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	//FIntVector4 GetInt4D(FString string);



	//// UInts are bad

	// 1D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	//uint8 GetUint(FString string);

	//// 2D UInt
	//// Have to do it per reference, TODO test me
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	//void GetUint2D(FString& string, uint8& Var1, uint8& Var2);

	//// 3D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	//void GetUint3D(FString string, uint8& Var1, uint8& Var2, uint8& Var3);

	//// 4D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	//FUintVector4 GetUint4D(FString string);


	//// BUFFER FUNCTIONALITY

	// A lot of those are using higher uints and ints, which is a bit iffy in blueprints.
	// Keep the higher types as C++ functions.


	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void Validate();

	// Lots of "Create" methods now, dunno if I really want them here or in the manager
	// - they shouldn't be needlessly exposed outside of this plugin.
	// CreateBuffer etc

	uint32 GetDeviceCount();
	FString GetDeviceName(int32 Ordinal);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetStackSize(int32 StackSize);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	int32 GetStackSize();

	void SetStackSize64(uint64 StackSize);
	uint64 GetStackSize64();

	// TODO TimeoutCallback
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetEntryPointCount(int32 NumEntryPoints);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	int32 GetEntryPointCount();


	// Program wrapper needed!
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetRayGenerationProgram(int32 EntryPointIndex, UOptiXProgram* Program);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXProgram* GetRayGenerationProgram(int32 EntryPointIndex);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetExceptionProgram(int32 EntryPointIndex, UOptiXProgram* Program);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXProgram* GetExceptionProgram(int32 EntryPointIndex);

	// TODO RTexception
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetExceptionEnabled(RTexception Exception, bool Enabled);

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	bool GetExceptionEnabled(RTexception Exception);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetRayTypeCount(int32 NumRayTypes);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	int32 GetRayTypeCount();

	// TODO
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetMissProgram(int32 RayTypeIndex, UOptiXProgram* Program);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXProgram* GetMissProgram(int32 RayTypeIndex);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void Compile();

	// Do not expose those for now!
	void Launch(int32 EntryPointIndex, uint64 ImageWidth);
	void Launch(int32 EntryPointIndex, uint64 ImageWidth, uint64 ImageHeight);
	void Launch(int32 EntryPointIndex, uint64 ImageWidth, uint64 ImageHeight, uint64 ImageDepth);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetBuffer(FString Name, UOptiXBuffer* Buffer);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetGeometryGroup(FString Name, UOptiXGeometryGroup* GeoGroup);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetGroup(FString Name, UOptiXGroup* Group);

	// TODO: Create Program Functions

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXProgram* CreateProgramFromPTXFile(FString Path, FString Name);

	// TODO: Create Buffer Functions

	// General, non-blueprint buffer function

	UOptiXBuffer* CreateBuffer(uint8 Type, RTformat Format, RTsize Width);
	UOptiXBuffer* CreateBuffer(uint8 Type, RTformat Format, RTsize Width, RTsize Height );
	UOptiXBuffer* CreateBuffer(uint8 Type, RTformat Format, RTsize Width, RTsize Height, RTsize Depth );

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateBufferSimple(uint8 Type);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateBufferUByte(uint8 Type);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateBufferUByte1D(uint8 Type, int32 Width);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateBufferUByte2D(uint8 Type, int32 Width, int32 Height);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateBufferUByte3D(uint8 Type, int32 Width, int32 Height, int32 Depth);

	// More Buffer Functions to avoid huge enums in blueprints:
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateOutputBufferColor(int32 Width, int32 Height);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateOutputBufferUByte3D(int32 Width, int32 Height, int32 Depth);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateOutputBufferDepth(int32 Width, int32 Height);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateOutputBufferIntersections(int32 Width, int32 Height);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateInputBufferFloat(int32 Width);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateInputBufferFloat2D(int32 Width, int32 Height);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateInputBufferFloat3D(int32 Width, int32 Height, int32 Depth);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* CreateCubemapBuffer(int32 Width, int32 Height);

	// Texture Sampler

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXTextureSampler* CreateTextureSampler();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetTextureSampler(FString Name, UOptiXTextureSampler* Sampler);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetSkybox(FString Name, UOptiXTextureSampler* Sampler);

	// Getters

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXBuffer* GetBuffer(FString Name);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXGeometryGroup* GetGeometryGroup(FString Name);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	UOptiXGroup* GetGroup(FString Name);

	// OptiX 6.0

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetMaxTraceDepth(int32 Depth);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	int32 GetMaxTraceDepth();





	// TODO: ALL ABOVE FUNCTIONS ARE NOT THREAD-SAFE, DO NOT USE WHEN CALLING THEM EVERY FRAME
	//	     USE THE CACHED ONES INSTEAD:


	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetFloat3DVectorThreadsafe(FString string, FVector Var);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXContext")
	void SetMatrixThreadsafe(FString string, FMatrix Matrix);

// In case I use this as a base class? TODO
private:

	/*
	Objects will not be added to the maps on creation but only on Set___().
	As the GeometryInstance keeps a reference to the underlying set of Geometries and 
	Materials, this should be safe from being GC'd.
	*/


	optix::Context NativeContext;

	UPROPERTY() // Just here for now so we the objects don't ge GC'd
	TMap<FString, UOptiXBuffer*> BufferMap;

	UPROPERTY() // Just here for now so we the objects don't ge GC'd
	TMap<FString, UOptiXMaterial*> MaterialMap;

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TMap<FString, UOptiXGeometry*> GeometryMap;

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TMap<FString, UOptiXGeometryInstance*> GeometryInstanceMap;

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TMap<FString, UOptiXGeometryGroup*> GeometryGroupMap;

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TMap<FString, UOptiXGroup*> GroupMap;

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TMap<FString, UOptiXTextureSampler*> TextureSamplerMap;

	UPROPERTY() // Keep those here on creation so we can update their transforms.
	TArray<UOptiXTransform*> TransformMap;

	//UPROPERTY() // Just here for now so the objects don't ge GC'd
	//TMap<FString, UOptiXProgram*> ProgramMap;
	UPROPERTY()
	TArray<UOptiXProgram*> RayGenerationPrograms;

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TArray<UOptiXProgram*> ExceptionPrograms;	

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TArray<UOptiXProgram*> MissPrograms;

	UPROPERTY()
	TMap<FString, FVector> VectorCache;

	UPROPERTY()
	TMap<FString, FMatrix> MatrixCache;
	// TODO:
	// Need maps for all data types tbh

};
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Runtime/Engine/Classes/Components/InstancedStaticMeshComponent.h"
//#include "Runtime/Engine/Classes/Materials/Material.h"

#include "OptiXLaserComponent.h"
#include "SelectableActorBase.h"
#include "LineInstancedStaticMeshComponent.h"

#include "OptiXLaserActor.generated.h"



/**
 * 
 */
UCLASS(Blueprintable, meta = (BlueprintSpawnableComponent))
class OPTIX_API AOptiXLaserActor : public ASelectableActorBase
{
	GENERATED_BODY()

public:

	AOptiXLaserActor(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	//virtual void Tick(float DeltaTime) override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	//void DisplayLines();	

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void UpdateLaserPattern(EPatternTypes Pattern);
	
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	//void InitInstancedMeshData();

	void SetLaserMaterial(UMaterialInstanceDynamic* DynamicLaserMaterial)
	{
		LineInstancedStaticMeshComponent->SetLaserMaterial(DynamicLaserMaterial);
	}

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void UpdateLaserWidth(float NewWidth);

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OptiX)
	bool bLaserTraceEnabled;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	ULineInstancedStaticMeshComponent* LineInstancedStaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	UOptiXLaserComponent* OptiXLaserComponent;

};




// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OptiXObjectComponent.h"
#include "OptiXGlassDefinitions.h"

#include "OptiXLensComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLensRadiusChanged, float, Radius);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLensThicknessChanged, float, Thickness);



///**
// * Glass Type Enum
// */
//UENUM(BlueprintType)
//enum class EGlassType : uint8 // TODO fix descriptions
//{
//	BK7 = 0					UMETA(DisplayName = "BK7"),
//	SF5 = 1					UMETA(DisplayName = "SF5"),
//	SF11 = 2				UMETA(DisplayName = "SF11"),
//	FUSED_SILICA = 3		UMETA(DisplayName = "Fused silicia"),
//	SK16 = 4				UMETA(DisplayName = "SK16"),
//	F2 = 5					UMETA(DisplayName = "F2")
//};

/**
 * Lens Component
 */
UCLASS(Blueprintable, hidecategories = (Object), meta = (BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXLensComponent : public UOptiXCubemapComponent
{
	GENERATED_BODY()
	
public:

	UOptiXLensComponent(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	// UOptiXObjectComponent Interface Start

	virtual void UpdateOptiXComponentVariables() override;
	
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void UpdateOptiXComponent() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGeometry() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXMaterial() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGroups() override;

	virtual void CleanOptiXComponent() override;
	// UOptiXObjectComponent Interface End

	virtual void InitCubemap(FRHICommandListImmediate & RHICmdList) override;

	virtual void UpdateCubemap(FRHICommandListImmediate & RHICmdList) override;


	// This sadly only works in the editor itself, leave it for now
	//virtual void PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent) override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
	void InitFromData(const FLensData& Data);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetThickness(float Thickness);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetThickness() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetRadius1(float Radius);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetRadius1() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetRadius2(float Radius);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetRadius2() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetLensRadius(float Radius);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetLensRadius() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetLensType1(ELensSideType Type);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		ELensSideType GetLensType1() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetLensType2(ELensSideType Type);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		ELensSideType GetLensType2() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
	void SetGlassType(FString Type);

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
	FString GetGlassType() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetWavelength(float WL);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetWavelength() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent") // TODO
	void MarkDirty();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent") // TODO
	TArray<FString> GetGlassDefinitionNames();

	UFUNCTION()
	void OnWavelengthChangedEvent(float WL);

	// Event callbacks:
	UPROPERTY(BlueprintAssignable, Category = "OptiXLensComponent")
	FOnLensRadiusChanged OnLensRadiusChanged;

	UPROPERTY(BlueprintAssignable, Category = "OptiXLensComponent")
	FOnLensThicknessChanged OnLensThicknessChanged;

public:

	// Lens Properties
	// Let's try some more Unreal magic with the BlueprintGetter=GetterFunctionName and BlueprintSetter=SetterFunctionName
	// functions!

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetThickness, BlueprintSetter=SetThickness) // todo
	float LensThickness;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetRadius1, BlueprintSetter=SetRadius1)
	float Radius1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetRadius2, BlueprintSetter=SetRadius2)
	float Radius2; // todo fix name

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetLensRadius, BlueprintSetter=SetLensRadius)
	float LensRadius; // todo fix name

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetLensType1, BlueprintSetter=SetLensType1)
	ELensSideType LensType1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetLensType2, BlueprintSetter=SetLensType2)
	ELensSideType LensType2; // todo fix names again gah

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetGlassType, BlueprintSetter=SetGlassType)
	FString GlassType;

	// Wavelength in NM
	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetWavelength, BlueprintSetter=SetWavelength)
	float CurrentWavelength;



	// Helper functions
	float GetCylinderLength(float Thickness) const;

private:

	// Not sure if we need to even save them here.
	UPROPERTY()
	UOptiXTransform* OptiXTransform;

	UPROPERTY()
	UOptiXAcceleration* OptiXAcceleration;

	UPROPERTY()
	UOptiXBuffer* CubemapBuffer;

	UPROPERTY()
	UOptiXTextureSampler* CubemapSampler;

	void RecalculateBoundingBox();




	// Okay, for some crazy reason glass definitions have been originally saved here... TODOOOOO
	//struct GlassDefinition
	//{
	//	FVector B;
	//	FVector C;
	//	GlassDefinition(FVector b, FVector c) : B(b), C(c)
	//	{}
	//};

	//const TArray<GlassDefinition> GlassDefinitions =
	//{
	//	GlassDefinition(FVector(1.03961212f, 0.231792344f, 1.01046945f), FVector(0.00600069867f, 0.0200179144f, 103.560653f)),	// BK7
	//	GlassDefinition(FVector(1.52481889f, 0.187085527f, 1.42729015f), FVector(0.011254756f, 0.0588995392f, 129.141675f)),	// SF5
	//	GlassDefinition(FVector(1.73759695f, 0.313747346f, 1.89878101f), FVector(0.013188707f, 0.0623068142f, 155.23629f)),		// SF11
	//	GlassDefinition(FVector(0.6961663f, 0.4079426f, 0.8974794f), FVector(0.0684043f, 0.1162414f, 9.896161f)),				// Fused S.
	//	GlassDefinition(FVector(1.34317774f, 0.241144399f, 0.994317969f), FVector(0.00704687339f, 0.0229005f, 92.7508526f)),	// SK16
	//	GlassDefinition(FVector(1.34533359f, 0.209073176f, 0.937357162f), FVector(0.00997743871f, 0.0470450767f, 111.886764f)),	// F2
	//};
};

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXGeometryInstance.h"
#include "OptiXAcceleration.h"

#include "OptiXGeometryGroup.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginGeometryGroup, Log, All);


UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXGeometryGroup : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	optix::GeometryGroup GetNativeGroup()
	{
		return NativeGeometryGroup;
	}

	void SetNativeGroup(optix::GeometryGroup Group)
	{
		NativeGeometryGroup = Group;
	}

	void DestroyOptiXObject();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	void SetAcceleration(UOptiXAcceleration* Accel);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	UOptiXAcceleration* GetAcceleration();
	

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	void SetChildCount(uint8 Count);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	uint8 GetChildCount();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	void SetChild(uint8 Index, UOptiXGeometryInstance* Child);

	////// Adds the material and returns the index to the added material.
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	UOptiXGeometryInstance* GetChild(uint8 Index);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	uint8 AddChild(UOptiXGeometryInstance* Child);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	uint8 RemoveChild(UOptiXGeometryInstance* Child);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	void RemoveChildByIndex(uint8 Index);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryGroup")
	uint8 GetChildIndex(UOptiXGeometryInstance* Child);

	UPROPERTY(BlueprintReadOnly, Category = "OptiXGeometryGroup")
	int32 ChildCount = 0;

	//// TODO Check if UPROPERTY is fine with the TARRAY
	UPROPERTY()
	TArray<UOptiXGeometryInstance*> OptiXGeometryInstances;

	UPROPERTY()
	UOptiXAcceleration* Acceleration;

protected:



	optix::GeometryGroup NativeGeometryGroup;


};
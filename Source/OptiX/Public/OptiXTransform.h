#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXAcceleration.h"
#include "OptiXGeometryInstance.h"
#include "OptiXGeometryGroup.h"

#include "OptiXTransform.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginTransform, Log, All);


UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXTransform : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	optix::Transform GetNativeTransform()
	{
		return NativeTransform;
	}

	void SetNativeGroup(optix::Transform T)
	{
		NativeTransform = T;
	}

	void DestroyOptiXObject();

	void UpdateTransform();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTransform")
	void SetChild(UObject* Child);

	RTobjecttype GetChildType();

	////// Adds the material and returns the index to the added material.
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTransform")
	UObject* GetChild();

	// Sets and caches the matrix, the rendering thread will then pass it to the optix context.
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTransform")
	void SetMatrix(FMatrix Matrix);

	// WARNING: NOT THREADSAFE
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTransform")
	void SetMatrixImmediate(FMatrix Matrix);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTransform")
	FMatrix GetMatrix();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTransform")
	void SetMotionRange(float TimeBegin, float TimeEnd);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTransform")
	FVector2D GetMotionRange();

	// MotionBorderMode TODO but who needs that even

	// TODO Set up some nice inheritance so we don't need to use UObject here but maybe UOptiXObject
	// or some kind of interface.
	UPROPERTY()
	UObject* OptiXChild;

	UPROPERTY()
	FMatrix TransformMatrix;

protected:

	optix::Transform NativeTransform;

};
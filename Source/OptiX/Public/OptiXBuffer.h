#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXDeclarations.h"

#include "OptiXBuffer.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginBuffer, Log, All);


// We probably don't need such huge buffers anyway (way too slow etc), so int32 *should* be enough for everything.
// That way, the whole thing becomes blueprint-able


struct RTsize2
{
public:
	RTsize X;
	RTsize Y;
};

struct RTsize3
{
public:
	RTsize X;
	RTsize Y;
	RTsize Z;
};


/*
A Wrapper for the optix::Buffer class. Keep this as simple wrapper for now, 
but it should probably be changed to allow Unreal to access the buffer data
in a more native format.
*/

UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXBuffer : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	optix::Buffer GetNativeBuffer()
	{
		return NativeBuffer;
	}

	void SetBuffer(optix::Buffer B)
	{
		NativeBuffer = B;
	}

	void DestroyOptiXObject();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void Validate();

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void SetFormat(RTformat Format); // TODO Enum

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	RTformat GetFormat();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void SetElementSize(int32 Size);

	void SetElementSizeNative(RTsize Size);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	int32 GetElementSize();

	RTsize GetElementSizeNative();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void MarkDirty();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void SetSize1D(int32 Width);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	int32 GetSize1D();


	void SetSize1DNative(RTsize Width);
	RTsize GetSize1DNative();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	int32 GetMipLevelSize1D(uint8 Level);

	RTsize GetMipLevelSize1DNative(uint8 Level);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void SetSize2D(int32 Width, int32 Height);
	
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	FIntPoint GetSize2D();

	void SetSize2DNative(RTsize Width, RTsize Height);
	RTsize2 GetSize2DNative();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	FIntPoint GetMipLevelSize2D(uint8 Level);

	RTsize2 GetMipLevelSize2DNative(uint8 Level);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void SetSize3D(int32 Width, int32 Height, int32 Depth);
	FIntVector GetSize3D();

	void SetSize3DNative(RTsize Width, RTsize Height, RTsize Depth);
	RTsize3 GetSize3DNative();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	FIntVector GetMipLevelSize3D(uint8 Level);


	RTsize3 GetMipLevelSize3DNative(uint8 Level);

	// No support for more than 3 dimensions for now!

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void SetMipLevelCount(uint8 Count);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	uint8 GetMipLevelCount();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	int GetId();

	// TODO: LOOK INTO WHAT THIS RETURNS AND IF WE NEED IT! RETURNS A void* ORIGINALLY
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void Map(uint8 Level = 0, uint8 MapFlags = 2);

	void* MapNative(uint8 Level = 0, uint8 MapFlags = RT_BUFFER_MAP_READ_WRITE, void* UserOwned = 0);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void Unmap(uint8 Level = 0);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	void BindProgressiveStream(UOptiXBuffer* Source);

	void GetProgressiveUpdateReady(int* Ready, unsigned int* SubframeCount, unsigned int* MaxSubframes);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXBuffer")
	bool GetProgressiveUpdateReady();

	void GetDevicePointer(int32 OptiXDeviceOrdinal, void** DevicePointer);
	void* GetDevicePointer(int32 OptiXDeviceOrdinal);

	void SetDevicePointer(int32 OptiXDeviceOrdinal, void* DevicePointer);

	bool GetProgressiveUpdateReady(unsigned int& SubframeCount);
	bool GetProgressiveUpdateReady(unsigned int& SubframeCount, unsigned int& MaxSubframes);

	void SetAttribute(RTbufferattribute Attrib, RTsize Size, void *P);
	void GetAttribute(RTbufferattribute Attrib, RTsize Size, void *P);

	FString Name; // TODO DEBUG ONLY

protected:

	optix::Buffer NativeBuffer;


};
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"

#include "OptiXLaserTargetComponent.h"
#include "SelectableActorBase.h"

#include "OptiXLaserDetectorActor.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, meta = (BlueprintSpawnableComponent))
class OPTIX_API AOptiXLaserDetectorActor : public ASelectableActorBase
{
	GENERATED_BODY()

public:

	AOptiXLaserDetectorActor(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION( /*meta = (BlueprintProtected)*/ Category = "OptiXDetectorActor")
	void RenderDataToTarget();

	UFUNCTION( /*meta = (BlueprintProtected)*/ Category = "OptiXDetectorActor")
	void Init();

	UFUNCTION(BlueprintCallable, Category = "OptiXDetectorActor")
	void Clear();

	UFUNCTION(BlueprintCallable, Category = "OptiXDetectorActor")
	void Save();

	UFUNCTION()
	void OnLaserTraceFinished();

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiXDetectorActor")
	TArray<UTexture2D*> SavedDetectorTextures;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OptiXDetectorActor")
	bool bIsEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OptiXDetectorActor")
	bool bIsColorMode = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OptiXDetectorActor")
	FColor HighlightColor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiXDetectorActor")
	UOptiXLaserTargetComponent* OptiXLaserTargetComponent;

	UPROPERTY(BlueprintReadOnly, Category = "OptiXDetectorActor")
	UTexture2D* DetectorResultTexture;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiXDetectorActor")
	UMaterialInstanceDynamic* DynamicScreenMaterial;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiXDetectorActor")
	float Max = 0;

	TUniquePtr<FUpdateTextureRegion2D> TextureRegion;
};

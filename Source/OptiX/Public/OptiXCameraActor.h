#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Runtime/Engine/Classes/Camera/PlayerCameraManager.h"
#include "OptiXContext.h"
#include "Runtime/Engine/Classes/Components/SceneCaptureComponent2D.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"
#include "Runtime/Engine/Classes/Components/SceneCaptureComponentCube.h"
#include "Engine/BlendableInterface.h"
#include "Runtime/Engine/Classes/Components/PostProcessComponent.h"

#include "OptiXCameraActor.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginCameraActor, Log, All);


UCLASS(Blueprintable, BlueprintType)
class OPTIX_API UOptiXBlendable : public UObject, public IBlendableInterface
{
	GENERATED_BODY()

public:

	UOptiXBlendable(const FObjectInitializer& ObjectInitializer);


	/** 0:no effect, 1:full effect */
	UPROPERTY(interp, Category = OptiXBlendable, BlueprintReadWrite, meta = (UIMin = "0.0", UIMax = "1.0"))
	float BlendWeight;

	// Begin interface IBlendableInterface
	virtual void OverrideBlendableSettings(class FSceneView& View, float InWeight) const override;
	// End interface IBlendableInterface

};

UCLASS(hidecategories = (Input))
class OPTIX_API AOptiXPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()

public:

	AOptiXPlayerCameraManager(const FObjectInitializer& ObjectInitializer);


	//AOptiXCameraActor(const FObjectInitializer& ObjectInitializer);


	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void Init();
	
	
public:


	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	TArray<UTextureRenderTarget2D*> CubemapRenderTargets;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	USceneCaptureComponent2D* SceneCaptureComponent;

	// Try the cubemap again

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	USceneCaptureComponentCube* SceneCaptureCubeComponent;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UTextureRenderTargetCube* CubeRenderTarget;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UPostProcessComponent* PostProcessComponent;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UMaterialInstanceDynamic* BlendableVR;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UMaterialInstanceDynamic* BlendableOrtho;


	
	FThreadSafeBool bCubemapCaptured = false;
	
private:


	void CaptureCubemap();

private:

	uint32 CubemapSize = 1024;

	uint32 C = 0;

};
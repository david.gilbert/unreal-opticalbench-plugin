#pragma once
/*

See optix_declarations.h

This defines the same enums as UENUMS to be used in blueprints. I hope this actually works.

*/

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

//
//
//UENUM()
//enum class URTformat : int32
//{
//	RT_FORMAT_UNKNOWN = 0x100,				/*!< Format unknown       */
//	RT_FORMAT_FLOAT,                        /*!< Float                */
//	RT_FORMAT_FLOAT2,                       /*!< sizeof(float)*2      */
//	RT_FORMAT_FLOAT3,                       /*!< sizeof(float)*3      */
//	RT_FORMAT_FLOAT4,                       /*!< sizeof(float)*4      */
//	RT_FORMAT_BYTE,                         /*!< BYTE                 */
//	RT_FORMAT_BYTE2,                        /*!< sizeof(CHAR)*2       */
//	RT_FORMAT_BYTE3,                        /*!< sizeof(CHAR)*3       */
//	RT_FORMAT_BYTE4,                        /*!< sizeof(CHAR)*4       */
//	RT_FORMAT_UNSIGNED_BYTE,                /*!< UCHAR                */
//	RT_FORMAT_UNSIGNED_BYTE2,               /*!< sizeof(UCHAR)*2      */
//	RT_FORMAT_UNSIGNED_BYTE3,               /*!< sizeof(UCHAR)*3      */
//	RT_FORMAT_UNSIGNED_BYTE4,               /*!< sizeof(UCHAR)*4      */
//	RT_FORMAT_SHORT,                        /*!< SHORT                */
//	RT_FORMAT_SHORT2,                       /*!< sizeof(SHORT)*2      */
//	RT_FORMAT_SHORT3,                       /*!< sizeof(SHORT)*3      */
//	RT_FORMAT_SHORT4,                       /*!< sizeof(SHORT)*4      */
//	RT_FORMAT_UNSIGNED_SHORT,               /*!< USHORT               */
//	RT_FORMAT_UNSIGNED_SHORT2,              /*!< sizeof(USHORT)*2     */
//	RT_FORMAT_UNSIGNED_SHORT3,              /*!< sizeof(USHORT)*3     */
//	RT_FORMAT_UNSIGNED_SHORT4,              /*!< sizeof(USHORT)*4     */
//	RT_FORMAT_INT,                          /*!< INT                  */
//	RT_FORMAT_INT2,                         /*!< sizeof(INT)*2        */
//	RT_FORMAT_INT3,                         /*!< sizeof(INT)*3        */
//	RT_FORMAT_INT4,                         /*!< sizeof(INT)*4        */
//	RT_FORMAT_UNSIGNED_INT,                 /*!< sizeof(UINT)         */
//	RT_FORMAT_UNSIGNED_INT2,                /*!< sizeof(UINT)*2       */
//	RT_FORMAT_UNSIGNED_INT3,                /*!< sizeof(UINT)*3       */
//	RT_FORMAT_UNSIGNED_INT4,                /*!< sizeof(UINT)*4       */
//	RT_FORMAT_USER,                         /*!< User Format          */
//	RT_FORMAT_BUFFER_ID,                    /*!< Buffer Id            */
//	RT_FORMAT_PROGRAM_ID,                   /*!< Program Id           */
//	RT_FORMAT_HALF,                         /*!< half float           */
//	RT_FORMAT_HALF2,                        /*!< sizeof(half float)*2 */
//	RT_FORMAT_HALF3,                        /*!< sizeof(half float)*3 */
//	RT_FORMAT_HALF4                         /*!< sizeof(half float)*4 */
//};
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXProgram.h"
#include "OptiXBuffer.h"

#include "OptiXGeometry.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXPluginGeometry, Log, All);

UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXGeometry : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	void SetGeometry(optix::Geometry Geom)
	{
		NativeGeometry = Geom;
	}

	void DestroyOptiXObject();

	// Setter Functions - apparently overloading isn't actually possible so I need to restructure EVERYTHING again
	// Only do the important ones for now as I might need to scrap that again.

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetFloat(FString string, float Var);

	//void SetFloat2D(FString string, FVector2D Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetFloat2D(FString string, float Var1, float Var2);

	// 3D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetFloat3DVector(FString string, FVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetFloat3D(FString string, float Var1, float Var2, float Var3);

	// 4D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetFloat4DVector(FString string, FVector4 Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4);


	// 1D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetInt(FString string, int32 Var);

	// 2D Int - wow Unreal your naming scheme is really top notch...
	//void SetInt2D(FString string, FIntPoint Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetInt2D(FString string, int32 Var1, int32 Var2);

	// 3D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetInt3DVector(FString string, FIntVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3);

	// 4D Int
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	//void SetInt4DVector(FString string, FIntVector4 Var);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetInt4D(FString string, int32 Var1, int32 Var2, int32 Var3, int32 Var4);



	// 1D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetUint(FString string, uint8 Var);

	// 2D Uint - wow Unreal your naming scheme is really top notch...
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetUint2D(FString string, uint8 Var1, uint8 Var2);

	// 3D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetUint3D(FString string, uint8 Var1, uint8 Var2, uint8 Var3);

	// 4D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetUint4D(FString string, uint8 Var1, uint8 Var2, uint8 Var3, uint8 Var4);


	// Matrices - careful Unreal uses ROW_MAJOR LAYOUT
	// TODO They will be a pain

	////
	//// Getters (gotta love wrappers)
	////

	// Can't overload return type so this is going to be ugly

	// Floats
	// 1D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	float GetFloat(FString string);

	// 2D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	FVector2D GetFloat2D(FString string);

	// 3D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	FVector GetFloat3D(FString string);

	// 4D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	FVector4 GetFloat4D(FString string);


	//// Ints

	// 1D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	int32 GetInt(FString string);

	// 2D Int - wow Unreal your naming scheme is really top notch...
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	FIntPoint GetInt2D(FString string);

	// 3D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	FIntVector GetInt3D(FString string);

	// 4D Int
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	//FIntVector4 GetInt4D(FString string);



	//// UInts are bad

	// 1D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	//uint8 GetUint(FString string);

	//// 2D UInt
	//// Have to do it per reference, TODO test me
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	//void GetUint2D(FString& string, uint8& Var1, uint8& Var2);

	//// 3D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	//void GetUint3D(FString string, uint8& Var1, uint8& Var2, uint8& Var3);

	//// 4D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	//FUintVector4 GetUint4D(FString string);


	// Geometry specific functions

	// Sadly we still need this!
	optix::Geometry GetNativeGeometry();

	// Not sure which of those are actually needed, but let's add a few:
	// Setters and getters are interleaved.
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetPrimitiveCount(int32 NumPrimitives);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	int32 GetPrimitiveCount();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetPrimitiveIndexOffset(int32 IndexOffsets);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	int32 GetPrimitiveIndexOffset();


	void SetPrimitiveCountUint(uint32 NumPrimitives);
	uint32 GetPrimitiveCountUint();

	void SetPrimitiveIndexOffsetUint(uint32 IndexOffsets);
	uint32 GetPrimitiveIndexOffsetUint();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetMotionRange(float TimeBegin, float TimeEnd);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	FVector2D GetMotionRange();

	//TODO: As those two take and return two optix-specific types, either wrap those or
	// make the whole thing only accessible through the actual optix object, as there's no real sense in wrapping this.
	void SetMotionBorderMode(RTmotionbordermode BeginMode, RTmotionbordermode EndMode);
	void GetMotionBorderMode(RTmotionbordermode& BeginMode, RTmotionbordermode& EndMode);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetMotionSteps(int32 N);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	int32 GetMotionSteps();


	void SetMotionStepsUint(uint32 N);
	uint32 GetMotionStepsUint();

	// Todo: Program def. needs to be wrapped as well and made available to the blueprint editor even!
	void SetBoundingBoxProgram(UOptiXProgram* Program);
	UOptiXProgram* GetBoundingBoxProgram();

	void SetIntersectionProgram(UOptiXProgram* Program);
	UOptiXProgram* GetIntersectionProgram();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void MarkDirty();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	bool IsDirty();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometry")
	void SetBuffer(FString Name, UOptiXBuffer* Buffer);

	// In case I use this as a base class? TODO
protected:

	optix::Geometry NativeGeometry;
	
	UPROPERTY()
	UOptiXProgram* IntersectionProgram;

	UPROPERTY()
	UOptiXProgram* BoundingBoxProgram;

	UPROPERTY() // Just here for now so we the objects don't ge GC'd
	TMap<FString, UOptiXBuffer*> BufferMap;
};
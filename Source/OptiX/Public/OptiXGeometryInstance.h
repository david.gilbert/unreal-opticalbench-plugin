#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXIncludes.h"

#include "OptiXMaterial.h"
#include "OptiXGeometry.h"
#include "OptiXTextureSampler.h"
#include "OptiXBuffer.h"

#include "OptiXGeometryInstance.generated.h"

UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXGeometryInstance : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	optix::GeometryInstance GetNativeInstance()
	{
		return NativeGeometryInstance;
	}

	void SetNativeInstance(optix::GeometryInstance Instance)
	{
		NativeGeometryInstance = Instance;
	}

	void DestroyOptiXObject();

	// Maybe keep this but tbh we don't want to have an optix::Geometry object running around without an associated wrapper
	//void SetNativeGeometry(Geometry Geometry);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetGeometry(UOptiXGeometry* OptiXGeometryPtr);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	UOptiXGeometry* GetGeometry();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetMaterialCount(uint8 Count);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	uint8 GetMaterialCount();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetMaterial(uint8 Idx, UOptiXMaterial*  Material);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	UOptiXMaterial* GetMaterial(uint8 Idx);

	////// Adds the material and returns the index to the added material.
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	uint8 AddMaterial(UOptiXMaterial* OptiXMaterialPtr);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetFloat(FString string, float Var);

	//void SetFloat2D(FString string, FVector2D Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetFloat2D(FString string, float Var1, float Var2);

	// 3D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetFloat3DVector(FString string, FVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetFloat3D(FString string, float Var1, float Var2, float Var3);

	// 4D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetFloat4DVector(FString string, FVector4 Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4);


	// 1D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetInt(FString string, int32 Var);

	// 2D Int - wow Unreal your naming scheme is really top notch...
	//void SetInt2D(FString string, FIntPoint Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetInt2D(FString string, int32 Var1, int32 Var2);

	// 3D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetInt3DVector(FString string, FIntVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetTextureSampler(FString Name, UOptiXTextureSampler* Sampler);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXGeometryInstance")
	void SetBuffer(FString Name, UOptiXBuffer* Buffer);

	UPROPERTY()
	UOptiXGeometry* OptiXGeometry; // Maybe make this a weak obj pointer?

	//// TODO Check if UPROPERTY is fine with the TARRAY
	UPROPERTY()
	TArray<UOptiXMaterial*> OptiXMaterials;

protected:

	optix::GeometryInstance NativeGeometryInstance;

	UPROPERTY() // Just here for now so the objects don't ge GC'd
	TMap<FString, UOptiXTextureSampler*> TextureSamplerMap;

	UPROPERTY() // Just here for now so we the objects don't ge GC'd
	TMap<FString, UOptiXBuffer*> BufferMap;
};
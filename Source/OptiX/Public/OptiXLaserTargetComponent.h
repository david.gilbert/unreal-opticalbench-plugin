// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OptiXObjectComponent.h"
#include "OptiXBuffer.h"
#include "OptiXLaserTargetComponent.generated.h"

/**
 *  
 */
UCLASS(Blueprintable, hidecategories = (Object), meta = (BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXLaserTargetComponent : public UOptiXObjectComponent
{
	GENERATED_BODY()

public:
	UOptiXLaserTargetComponent(const FObjectInitializer& ObjectInitializer);
	
	virtual void UpdateOptiXComponentVariables() override;

	/* OptiXObjectComponent Interface Start */
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void UpdateOptiXComponent() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGeometry() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXMaterial() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGroups() override;

	virtual void CleanOptiXComponent() override;
	/*OptiXObjectComponent Interface End*/

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	float GetMaxFromBuffer();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	void UpdateBufferData();

	/* Standard Component Interface Start */
	//virtual void BeginPlay() override;
	//virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	/* Standard Component Interface End */

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OptiX")
	bool bIsColorMode;

	UPROPERTY(BlueprintReadOnly, Category = "OptiX")
	UOptiXTransform* OptiXTransform;

	UPROPERTY(BlueprintReadOnly, Category = "OptiX")
	UOptiXAcceleration* OptiXAcceleration;

	UPROPERTY(BlueprintReadOnly, Category = "OptiX")
	UOptiXBuffer* TargetBuffer;

	UPROPERTY(BlueprintReadOnly, Category = "OptiX")
	UOptiXBuffer* TargetBufferMax;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiX")
	float TargetRes;

	// Think about this later a bit - is there even a reason to save it like this?
	// It seems much easier to have optix write the already correct colors in the buffer and then just do a FMemory:MemCpy?
	TArray<FColor, TFixedAllocator<512 * 512>> ColorData; // Needs to be FColor so we can do an easy mem copy

	float HighlightColorValue = 0.8f;

	FColor HighlightColor;

	TArray<FColor, TFixedAllocator<256>> ColorLUT;
	UTexture2D* LUTTexture;
};

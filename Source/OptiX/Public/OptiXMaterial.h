#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXProgram.h"
//#include "OptiXModule.h"

#include "OptiXMaterial.generated.h"

UCLASS(hidecategories = Object, BlueprintType)
class OPTIX_API UOptiXMaterial : public UObject
{
	GENERATED_BODY() // Apparently GENERATED_UCLASS_BODY is deprecated?

public:

	virtual void BeginDestroy() override;

	void SetMaterial(optix::Material Mat)
	{
		NativeMaterial = Mat;
	}

	void DestroyOptiXObject();

	optix::Material GetNativeMaterial()
	{
		return NativeMaterial;
	}
	// Setter Functions - apparently overloading isn't actually possible so I need to restructure EVERYTHING again
	// Only do the important ones for now as I might need to scrap that again.

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetFloat(FString string, float Var);

	//void SetFloat2D(FString string, FVector2D Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetFloat2D(FString string, float Var1, float Var2);

	// 3D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetFloat3DVector(FString string, FVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetFloat3D(FString string, float Var1, float Var2, float Var3);

	// 4D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetFloat4DVector(FString string, FVector4 Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4);


	// 1D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetInt(FString string, int32 Var);

	// 2D Int - wow Unreal your naming scheme is really top notch...
	//void SetInt2D(FString string, FIntPoint Var)
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetInt2D(FString string, int32 Var1, int32 Var2);

	// 3D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetInt3DVector(FString string, FIntVector Var);
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3);

	// 4D Int
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
	//void SetInt4DVector(FString string, FIntVector4 Var);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetInt4D(FString string, int32 Var1, int32 Var2, int32 Var3, int32 Var4);



	// 1D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetUint(FString string, uint8 Var);

	// 2D Uint - wow Unreal your naming scheme is really top notch...
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetUint2D(FString string, uint8 Var1, uint8 Var2);

	// 3D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetUint3D(FString string, uint8 Var1, uint8 Var2, uint8 Var3);

	// 4D Uint
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		void SetUint4D(FString string, uint8 Var1, uint8 Var2, uint8 Var3, uint8 Var4);


	// Matrices - careful Unreal uses ROW_MAJOR LAYOUT
	// TODO They will be a pain

	////
	//// Getters (gotta love wrappers)
	////

	// Can't overload return type so this is going to be ugly

	// Floats
	// 1D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		float GetFloat(FString string);

	// 2D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		FVector2D GetFloat2D(FString string);

	// 3D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		FVector GetFloat3D(FString string);

	// 4D Float
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		FVector4 GetFloat4D(FString string);


	//// Ints

	// 1D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		int32 GetInt(FString string);

	// 2D Int - wow Unreal your naming scheme is really top notch...
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		FIntPoint GetInt2D(FString string);

	// 3D Int
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
		FIntVector GetInt3D(FString string);

	// 4D Int
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
	//FIntVector4 GetInt4D(FString string);



	//// UInts are bad

	// 1D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
	//uint8 GetUint(FString string);

	//// 2D UInt
	//// Have to do it per reference, TODO test me
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
	//void GetUint2D(FString& string, uint8& Var1, uint8& Var2);

	//// 3D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
	//void GetUint3D(FString string, uint8& Var1, uint8& Var2, uint8& Var3);

	//// 4D UInt
	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXMaterial")
	//FUintVector4 GetUint4D(FString string);




	// Todo: Program def. needs to be wrapped as well and made available to the blueprint editor even!
	void SetClosestHitProgram(uint32 RayTypeIndex, UOptiXProgram* Program);
	UOptiXProgram* GetClosestHitProgram(uint32 RayTypeIndex);

	void SetAnyHitProgram(uint32 RayTypeIndex, UOptiXProgram* Program);
	UOptiXProgram* GetAnyHitProgram(uint32 RayTypeIndex);

protected:

	optix::Material NativeMaterial;

	UPROPERTY()
	TArray<UOptiXProgram*> ClosestHitPrograms;

	UPROPERTY()
	UOptiXProgram* AnyHitProgram;

};
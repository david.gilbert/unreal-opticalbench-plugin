using UnrealBuildTool;
using System.IO;

public class OptiX : ModuleRules
{
    public OptiX(ReadOnlyTargetRules Target) : base(Target)
    {

        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        bEnableExceptions = true;   
        

        string EnginePath = Path.GetFullPath(Target.RelativeEnginePath);
        string ModulePath = ModuleDirectory;

        Definitions.Add("MALLOC_LEAKDETECTION=0");


        PublicIncludePaths.AddRange(
            new string[] {
                ModulePath + "/Public"
            }
            );


        PrivateIncludePaths.AddRange(
            new string[] {
                ModulePath + "/Private",
                EnginePath + "/Source/Runtime/Engine/Private",
                EnginePath + "/Source/Runtime/Renderer/Private",
            }
            );

        PublicDependencyModuleNames.AddRange(
            new string[] 
            {
                "Core",
                //"Projects",
                "CoreUObject",
                "CUDALibrary",
                "OptiXLibrary",
                "Engine",
                "RHI",
                "RenderCore",
                "HeadMountedDisplay",
                "UMG",
                "Slate",
                "SlateCore",
            }
            );
        PrivateDependencyModuleNames.AddRange(
            new string[] 
            {
                "Core",
                "CoreUObject",
                "Projects",
                "CUDALibrary",
                "OptiXLibrary",
                "Engine",
                "HeadMountedDisplay",
                "Json",
                "JsonUtilities",
                "RenderCore",
                "RHI"
            }
            );

        DynamicallyLoadedModuleNames.AddRange(
            new string[]
            {
                // ... add any modules that your module loads dynamically here ...
            }
            );
    }
}
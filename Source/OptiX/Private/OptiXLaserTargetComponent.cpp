// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLaserTargetComponent.h"

#include "OptiXModule.h"

#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"


UOptiXLaserTargetComponent::UOptiXLaserTargetComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Target Component Constructor"));

	TargetRes = 512;
	bIsColorMode = true;
	HighlightColor = FLinearColor(1.0f, 0.0f, 0.0f).ToFColor(false);

	ColorLUT.SetNumZeroed(256);
	ColorData.SetNumZeroed(512*512);
	
	static ConstructorHelpers::FObjectFinder<UTexture2D> LT(TEXT("Texture2D'/OptiX/Targets/Target_LUT.Target_LUT'"));
	if (LT.Object != NULL)
	{
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Target Component: Initialized LUT Texture"));

		LUTTexture = LT.Object;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("OptiX Laser Target Component Constructor: LUT TEXTURE ASSET NOT FOUND"));
	}

}

void UOptiXLaserTargetComponent::UpdateOptiXComponentVariables()
{
}

void UOptiXLaserTargetComponent::UpdateOptiXComponent()
{
	if (OptiXGeometry != nullptr && OptiXTransform != nullptr && OptiXAcceleration != nullptr)
	{
		FMatrix T = GetComponentToWorld().ToMatrixNoScale();
		OptiXTransform->SetMatrix(T);
		//OptiXAcceleration->MarkDirty();
		OptiXContext->GetGroup("top_object")->GetAcceleration()->MarkDirty();
		FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();
	}
}

void UOptiXLaserTargetComponent::InitOptiXGeometry()
{
	OptiXGeometry = OptiXContext->CreateGeometry();
	OptiXGeometry->SetPrimitiveCount(1u);
	UOptiXProgram* BB = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/laser_target.ptx", "bounds");
	UOptiXProgram* IP = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/laser_target.ptx", "intersect");

	OptiXGeometry->SetBoundingBoxProgram(BB);
	OptiXGeometry->SetIntersectionProgram(IP);
}

void UOptiXLaserTargetComponent::InitOptiXMaterial()
{
	UOptiXProgram* AHPerspective = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/laser_target_material.ptx", "any_hit_radiance");
	UOptiXProgram* CHIterative = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/laser_target_material.ptx", "closest_hit_iterative");

	OptiXMaterial = OptiXContext->CreateMaterial();
	OptiXMaterial->SetAnyHitProgram(0, AHPerspective);
	OptiXMaterial->SetClosestHitProgram(1, CHIterative);
}

void UOptiXLaserTargetComponent::InitOptiXGroups()
{
	// Buffers:

	TargetBuffer = OptiXContext->CreateBuffer(RT_BUFFER_INPUT_OUTPUT, RT_FORMAT_FLOAT, TargetRes, TargetRes);
	TargetBufferMax = OptiXContext->CreateBuffer(RT_BUFFER_INPUT_OUTPUT, RT_FORMAT_UNSIGNED_INT, 1);

	// Clear target buffers?

	// Color lookup table here


	OptiXGeometryInstance = OptiXContext->CreateGeometryInstance(OptiXGeometry, OptiXMaterial);
	//OptiXAcceleration->SetProperty("refit", "1");



	FVector P1 = FVector(2.89f, 0.0f, 0.0f); // TODO WHAT IS THIS THING

	FVector P2 = FVector(-0.129f, 0.0f, 0.08f) * 1; // TODO WHAT IS THIS THING

	OptiXGeometryInstance->SetFloat3DVector("p1", P1 * 1.0f); // Detector
	OptiXGeometryInstance->SetFloat3DVector("p2", P2 * 1000); 

	OptiXGeometryInstance->SetFloat2D("stretchXY1", 0.05f * 100.0f, 0.05f * 100.0f); // Detector
	OptiXGeometryInstance->SetFloat2D("stretchXZ2", 0.05f * 10.0f, 0.05f * 10.0f); // Corresponds to P2

	//OptiXGeometryInstance->SetInt("targetBufferWrite", 1);

	OptiXGeometryInstance->SetBuffer("targetBuffer", TargetBuffer);
	OptiXGeometryInstance->SetBuffer("targetBufferMax", TargetBufferMax);

	OptiXGeometryInstance->SetFloat2D("targetBufferDim", TargetRes, TargetRes);

	OptiXTransform = OptiXContext->CreateTransform();

	FMatrix WorldTransform = GetComponentToWorld().ToMatrixNoScale();
	OptiXTransform->SetMatrix(WorldTransform);

	OptiXGeometryGroup = OptiXContext->CreateGeometryGroup();
	OptiXGeometryGroup->AddChild(OptiXGeometryInstance);

	OptiXAcceleration = OptiXContext->CreateAcceleration("NoAccel"); // Seems to be faster for now
	OptiXGeometryGroup->SetAcceleration(OptiXAcceleration);
	OptiXTransform->SetChild(OptiXGeometryGroup);

	OptiXContext->GetGroup("top_object")->AddChild(OptiXTransform);
	//FMatrix T = GetComponentToWorld().ToMatrixNoScale();
	//OptiXTransform->SetMatrix(T);

	OptiXContext->GetGroup("top_object")->GetAcceleration()->MarkDirty();


	// Init LUT:

	FTexture2DMipMap& IndexMip = LUTTexture->PlatformData->Mips[0];

	FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_ONLY));

	for (uint32 i = 0; i < 256; i++)
	{
		ColorLUT[i] = TextureData[i];
	}
	IndexMip.BulkData.Unlock();
	UE_LOG(LogTemp, Display, TEXT("Finished filling color LUT for the laser target"));

}

void UOptiXLaserTargetComponent::CleanOptiXComponent()
{
	if(OptiXContext->GetGroup("top_object") != NULL)
		OptiXContext->GetGroup("top_object")->RemoveChild(OptiXTransform);

	OptiXTransform = nullptr;
	Super::CleanOptiXComponent();
	OptiXGeometryGroup = nullptr;
}

float UOptiXLaserTargetComponent::GetMaxFromBuffer()
{
	float Max = (float)*static_cast<unsigned int*>(TargetBufferMax->MapNative(0, RT_BUFFER_MAP_READ));
	TargetBufferMax->Unmap();
	if (Max == 0)
	{
		Max = 1; // TODO WHY?
	}
	return Max;
}

void UOptiXLaserTargetComponent::UpdateBufferData()
{

	check(IsInRenderingThread());

	float Max = GetMaxFromBuffer();
	UE_LOG(LogTemp, Warning, TEXT("UPDATING DEPRECATED BUFFER DATA"));


	float* Data = static_cast<float*>(TargetBuffer->MapNative(0, RT_BUFFER_MAP_READ));

	// Just use the previous algorithm for now without thinking too much about it
	// But honestly doing this on the cpu hurts so much...
	// TODO: Either
	// 1) Make optix just output the correct colors directly OR
	// 2) move the LUT stuff into the actual material...

	float CurrentVal = 0;
	FColor CurrentColor;
	bool Tmp = false;

	if (!bIsColorMode)
	{
		for (uint32 i = 0; i < TargetRes * TargetRes; i++)
		{
			CurrentVal = Data[i] / Max;
			Tmp = (CurrentVal >= HighlightColorValue);
			ColorData[i] = (Tmp) ? HighlightColor : FLinearColor(CurrentVal, CurrentVal, CurrentVal).ToFColor(false);
		}
	}
	else
	{
		for (uint32 i = 0; i < TargetRes * TargetRes; i++)
		{
			CurrentVal = Data[i] / Max;

			CurrentColor = ColorLUT[(int)(255.0f * CurrentVal)]; // Uh oh todo
			ColorData[i] = CurrentColor;
		}

	}
	TargetBuffer->Unmap();
}


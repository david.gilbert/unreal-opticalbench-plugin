// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLaserComponent.h"
#include "OptiXModule.h"

#include "UObject/ConstructorHelpers.h"


// Sets default values for this component's properties
UOptiXLaserComponent::UOptiXLaserComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false; 
	bWantsOnUpdateTransform = true; // why the hell do I need this here but not on the others?

	// ...

	LaserMaxDepth = 20;
	LaserEntryPoint = 1; // Default, will be overwritten anyway

	LaserBufferWidth = 50 * 50;
	LaserBufferHeight = LaserMaxDepth * 2;

	LaserBufferSize = LaserBufferHeight * LaserBufferWidth;


	RayTIR = false;
	TargetBufferWrite = 1; //todo
	TargetColorMode = 0; // todo
	Wavelength = 450.0f;
	LaserWidth = 0.025f;
	CurrentLaserPattern = EPatternTypes::POINTER;
	LaserTracesPerFrame = 20;

	// Find and load default patterns:

	static ConstructorHelpers::FObjectFinder<UTexture2D> CrossTexture(TEXT("Texture2D'/OptiX/Laser/Cross.Cross'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrossTextureDir(TEXT("Texture2D'/OptiX/Laser/Cross_dir.Cross_dir'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> PointerTexture(TEXT("Texture2D'/OptiX/Laser/Pointer.Pointer'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> PointerTextureDir(TEXT("Texture2D'/OptiX/Laser/Pointer_dir.Pointer_dir'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> CircleTexture(TEXT("Texture2D'/OptiX/Laser/Circle.Circle'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> CircleTextureDir(TEXT("Texture2D'/OptiX/Laser/Circle_dir.Circle_dir'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> QuadTexture(TEXT("Texture2D'/OptiX/Laser/Quad.Quad'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> QuadTextureDir(TEXT("Texture2D'/OptiX/Laser/Quad_dir.Quad_dir'"));



	if (CrossTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::CROSS, CrossTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Cross Pattern."));
	}
	if (CrossTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::CROSS, CrossTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Cross Dir Pattern."));
	}
	if (PointerTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::POINTER, PointerTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Pointer Pattern."));
	}
	if (PointerTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::POINTER, PointerTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Pointer Dir Pattern."));
	}	
	if (CircleTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::CIRCLE, CircleTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Circle Pattern."));
	}	
	if (CircleTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::CIRCLE, CircleTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Circle Dir Pattern."));
	}
	if (QuadTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::QUAD, QuadTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Quad Pattern."));
	}
	if (QuadTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::QUAD, QuadTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Quad Dir Pattern."));
	}	

	// Init the # of rays and their indices

	// This crashes on packaging for some reason, apparently Mips has length 0.

	if (Patterns[CurrentLaserPattern]->PlatformData->Mips.Num() != 0)
	{
		FTexture2DMipMap& IndexMip = Patterns[CurrentLaserPattern]->PlatformData->Mips[0];

		UE_LOG(LogTemp, Display, TEXT("Got Texture mips"));


		FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_WRITE));


		int X = Patterns[CurrentLaserPattern]->GetSizeX();
		int Y = Patterns[CurrentLaserPattern]->GetSizeY();


		// Save the texture indices for the direction:

		// Texture index conversion is a real pain...
		for (int32 i = 0; i < X; ++i) {
			for (int32 j = 0; j < Y; ++j) {

				int32 TextureIndex = (X * Y - 1) - i * X - j;
				int32 BufferIndex = ((j)*(X)+i);

				if (TextureData[TextureIndex].R || TextureData[TextureIndex].G || TextureData[TextureIndex].B)
				{
					LaserIndices.Add(BufferIndex);
					LaserIndexColorMap.Add(TextureData[TextureIndex]);
				}
			}
		}

		IndexMip.BulkData.Unlock();

		UE_LOG(LogTemp, Display, TEXT("Finished Laser Component texture conversion."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Texture Mip array is empty in OptiXLaserComponent Constructor. Skipping laser texture reading, hopefully fixed on beginplay()."));
	}
}


// Called when the game starts
void UOptiXLaserComponent::BeginPlay()
{
	Super::BeginPlay();
	Init();
}

void UOptiXLaserComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	UE_LOG(LogTemp, Warning, TEXT("OptiX Laser Component EndPlay"));

	CleanOptiXObjects();
}


void UOptiXLaserComponent::OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport/* = ETeleportType::None*/)
{
	Super::OnUpdateTransform(EUpdateTransformFlags::SkipPhysicsUpdate, Teleport);
	UpdateLaserPosition();
}

void UOptiXLaserComponent::UpdateOptiXContextVariables()
{
	check(IsInRenderingThread());

	if (bUpdateQueued)
	{
		OptiXContext->SetInt("allowTir", RayTIR);
		OptiXContext->SetInt("targetBufferWrite", static_cast<int32>(TargetBufferWrite));
		OptiXContext->SetFloat("laserWaveLength", Wavelength);

		OptiXContext->SetFloat("laserSize", 3.5f); // Hardcode this for now

		OptiXContext->SetFloat("laserBeamWidth", LaserWidth);


		// Upload buffer data

		if (bPatternChanged)
		{

			int X = Patterns[CurrentLaserPattern]->GetSizeX();
			int Y = Patterns[CurrentLaserPattern]->GetSizeY();

			// Those get set on the game thread already
			//LaserIndices.Empty();
			//LaserIndexColorMap.Empty();


			UOptiXBuffer* LaserIndexBuffer = OptiXContext->GetBuffer("laserIndex");
			UOptiXBuffer* LaserDirectionBuffer = OptiXContext->GetBuffer("laserDir");
			{
				UE_LOG(LogTemp, Display, TEXT("New Pattern: %i"), static_cast<uint8>(CurrentLaserPattern));

				int32* BufferIndexData = static_cast<int32*>(LaserIndexBuffer->MapNative(0, RT_BUFFER_MAP_WRITE));
				// Reset the buffer explicitly 
				FMemory::Memset(BufferIndexData, 0u, X * Y * 4);

				FTexture2DMipMap& IndexMip = Patterns[CurrentLaserPattern]->PlatformData->Mips[0];

				FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_WRITE));

				// Save the texture indices for the direction:

				TArray<int32> TextureIndices;

				// Texture index conversion is a real pain...
				for (int32 i = 0; i < X; ++i) {
					for (int32 j = 0; j < Y; ++j) {

						int32 TextureIndex = (X * Y - 1) - i * X - j;
						int32 BufferIndex = ((j)*(X)+i);

						BufferIndexData[BufferIndex] = 0;

						if (TextureData[TextureIndex].R || TextureData[TextureIndex].G || TextureData[TextureIndex].B)
						{
							BufferIndexData[BufferIndex] = 1;
							//LaserIndices.Add(BufferIndex);
							TextureIndices.Add(TextureIndex);
							//LaserIndexColorMap.Add(BufferIndex, TextureData[TextureIndex]);
						}
						else
						{
							BufferIndexData[BufferIndex] = -1; // do not cast ray
						}
					}
				}

				UE_LOG(LogTemp, Display, TEXT("# Ray Indices in Pattern %i"), static_cast<uint8>(TextureIndices.Num()));

				IndexMip.BulkData.Unlock();
				LaserIndexBuffer->Unmap();
			}

			{

				// TODO: It should really suffice to just copy the respective texture index. If we don't shoot a ray, the direction won't matter anyway

				float* BufferDirectionData = static_cast<float*>(LaserDirectionBuffer->MapNative(0, RT_BUFFER_MAP_WRITE));

				FTexture2DMipMap& DirectionMip = PatternDirections[CurrentLaserPattern]->PlatformData->Mips[0];

				FColor* TextureData = static_cast<FColor*>(DirectionMip.BulkData.Lock(LOCK_READ_WRITE));

				// Texture index conversion is a real pain...
				for (int32 i = 0; i < X; ++i) {
					for (int32 j = 0; j < Y; ++j) {

						int32 TextureIndex = (X * Y - 1) - i * X - j;
						int32 BufferIndex = ((j)*(X)+i) * 3;

						//UE_LOG(LogTemp, Display, TEXT("Values: %i"), TextureData[BufferIndex]);

						FVector DirNormalSpace = FVector();
						DirNormalSpace.X = (TextureData[TextureIndex].R / 256.0f) * 2.0f - 1.0f;
						DirNormalSpace.Y = (TextureData[TextureIndex].G / 256.0f) * 2.0f - 1.0f;
						DirNormalSpace.Z = (TextureData[TextureIndex].B / 256.0f);

						// We need to rotate this to face forward (1, 0, 0):

						FMatrix Mat = FRotationMatrix::MakeFromX(FVector(0, 0, 1));
						FVector Dir = Mat.TransformVector(DirNormalSpace);

						BufferDirectionData[BufferIndex] = Dir.X;
						BufferDirectionData[BufferIndex + 1] = Dir.Y;
						BufferDirectionData[BufferIndex + 2] = Dir.Z;

					}
				}
				DirectionMip.BulkData.Unlock();
				LaserDirectionBuffer->Unmap();
			}
			bPatternChanged.AtomicSet(false);
		}


		FTransform CurrentTransform = GetComponentTransform();
		FVector Translation = CurrentTransform.GetTranslation();
		FVector Forward = GetForwardVector();
		FVector Right = GetRightVector();
		FVector Up = GetUpVector();
			//FMatrix Rotation = GetComponentRotation().;

			// Hard code this for now: laser is around 10x10x10 cube

		OptiXContext->SetFloat3DVectorThreadsafe("laser_origin", Translation);
		OptiXContext->SetFloat3DVectorThreadsafe("laser_forward", Forward);
		OptiXContext->SetFloat3DVectorThreadsafe("laser_right", Right);
		OptiXContext->SetFloat3DVectorThreadsafe("laser_up", Up);


		OptiXContext->SetMatrixThreadsafe("laser_rot", CurrentTransform.ToMatrixNoScale());


		FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();

		bUpdateQueued.AtomicSet(false);

	}
}

void UOptiXLaserComponent::Init()
{
	OptiXContext = FOptiXModule::Get().GetContext();
	FString OptiXPTXDir = FOptiXModule::Get().OptiXPTXDir;

	UE_LOG(LogTemp, Display, TEXT("Init Laser, got context instance."));

	// Setup buffers and stuff, make sure to make this compatible with an arbitrary # of lasers.
	// Use the entry points to do this?


	SetRayTIR(RayTIR);
	SetTargetBufferWrite(TargetBufferWrite);
	SetTargetColorMode(TargetColorMode);
	SetWavelength(Wavelength);
	SetLaserWidth(LaserWidth);

	OptiXContext->SetInt("allowTir", RayTIR);
	OptiXContext->SetInt("targetBufferWrite", static_cast<int>(TargetBufferWrite));
	OptiXContext->SetFloat("laserWaveLength", Wavelength);

	OptiXContext->SetFloat("laserSize", 3.5f); // Hardcode this for now

	OptiXContext->SetFloat("laserBeamWidth", LaserWidth);

	SetLaserPattern(CurrentLaserPattern);

	FOptiXModule::Get().GetOptiXContextManager()->bLaserIsInitialized.AtomicSet(true);

}


void UOptiXLaserComponent::SetRayTIR(bool Active)
{
	RayTIR = Active;
	bUpdateQueued.AtomicSet(true);
}

bool UOptiXLaserComponent::GetRayTIR() const
{
	return RayTIR;
}

void UOptiXLaserComponent::SetTargetBufferWrite(bool Flag)
{
	TargetBufferWrite = Flag;
	bUpdateQueued.AtomicSet(true);
}

bool UOptiXLaserComponent::GetTargetBufferWrite() const
{
	return TargetBufferWrite;
}

void UOptiXLaserComponent::SetTargetColorMode(bool Flag)
{
	TargetColorMode = Flag;
	bUpdateQueued.AtomicSet(true);
}

bool UOptiXLaserComponent::GetTargetColorMode() const
{
	return TargetColorMode;
}

void UOptiXLaserComponent::SetWavelength(float WL)
{
	Wavelength = WL;
	FOptiXModule::Get().GetOptiXContextManager()->BroadcastWavelengthChange(WL);
	bUpdateQueued.AtomicSet(true);
}

float UOptiXLaserComponent::GetWavelength() const
{
	return Wavelength;
}

void UOptiXLaserComponent::SetLaserWidth(float Width)
{
	LaserWidth = Width;
	bUpdateQueued.AtomicSet(true);
}

float UOptiXLaserComponent::GetLaserWidth() const
{
	return LaserWidth;
}

void UOptiXLaserComponent::SetLaserPattern(EPatternTypes Pattern)
{

	/*
	The original way of setting up the buffer and pattern seems a bit wasteful, as the whole 50 * 50 * 20 buffer gets allocated,
	even tho only a very tiny amount of rays actually get cast. For now, keep it that way on the optix side, but actually save the 
	number of rays and their index here, so we don't have to copy and draw the whole result buffer. 
	*/

	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Queuing Laser Pattern Change"));

	CurrentLaserPattern = Pattern;

	// Update the base indices first so that the game thread can directly create the insanced lines instead of having to wait for the render thread
	// The actual positions will get updated when the render thread catches up.
	
	bUpdateQueued.AtomicSet(true);
	bPatternChanged.AtomicSet(true);
}

void UOptiXLaserComponent::PreparePatternChange(EPatternTypes Pattern)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Preparing Laser Pattern Change"));

	if (Patterns[Pattern]->PlatformData->Mips.Num() != 0)
	{

		LaserIndices.Empty();
		LaserIndexColorMap.Empty();

		FTexture2DMipMap& IndexMip = Patterns[Pattern]->PlatformData->Mips[0];
		UE_LOG(LogTemp, Display, TEXT("Got Texture mips"));
		FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_WRITE));

		int X = Patterns[Pattern]->GetSizeX();
		int Y = Patterns[Pattern]->GetSizeY();

		for (int32 i = 0; i < X; ++i) {
			for (int32 j = 0; j < Y; ++j) {

				int32 TextureIndex = (X * Y - 1) - i * X - j;
				int32 BufferIndex = ((j)*(X)+i);

				if (TextureData[TextureIndex].R || TextureData[TextureIndex].G || TextureData[TextureIndex].B)
				{
					LaserIndices.Add(BufferIndex);
					LaserIndexColorMap.Add(TextureData[TextureIndex]);
				}
			}
		}
		IndexMip.BulkData.Unlock();
	}
}

EPatternTypes UOptiXLaserComponent::GetLaserPattern() const
{
	return CurrentLaserPattern;
}


void UOptiXLaserComponent::UpdateLaserPosition()
{
	//UE_LOG(LogTemp, Warning, TEXT("OptiX Laser Component Updating Position"));
	bUpdateQueued.AtomicSet(true);
}


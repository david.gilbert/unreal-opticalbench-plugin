#include "OptiXTextureSampler.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXModule.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

DEFINE_LOG_CATEGORY(OptiXPluginTextureSampler);

void UOptiXTextureSampler::BeginDestroy()
{
	DestroyOptiXObject();

	Super::BeginDestroy();
}

void UOptiXTextureSampler::DestroyOptiXObject()
{
	if (NativeTextureSampler != NULL)
	{
		try
		{
			FOptiXModule::Get().GetOptiXContextManager()->TextureSamplersToDeleteQueue.Enqueue(NativeTextureSampler);
			//NativeTextureSampler->destroy();
		}
		catch (optix::Exception& E)
		{
			FString Message = FString(E.getErrorString().c_str());
			UE_LOG(OptiXPluginTextureSampler, Fatal, TEXT("OptiX Error: %s"), *Message);
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
		}
	}
	OptiXBuffer = nullptr; // Don't explicitly destroy the buffer here
	NativeTextureSampler = NULL;
}

void UOptiXTextureSampler::Validate()
{
	try
	{
		NativeTextureSampler->validate();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXTextureSampler::SetMipLevelCount(uint8 NumMipLevels)
{
	try
	{
		NativeTextureSampler->setMipLevelCount(NumMipLevels);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

uint8 UOptiXTextureSampler::GetMipLevelCount()
{
	uint8 Count = 0;
	try
	{
		Count = NativeTextureSampler->getMipLevelCount();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Count;
}

void UOptiXTextureSampler::SetArraySize(int32 NumTexturesInArray)
{
	try
	{
		NativeTextureSampler->setArraySize(NumTexturesInArray);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

int32 UOptiXTextureSampler::GetArraySize()
{
	int32 Count = 0;
	try
	{
		Count = NativeTextureSampler->getArraySize();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Count;
}

void UOptiXTextureSampler::SetWrapMode(int32 Dim, RTwrapmode Wrapmode)
{
	try
	{
		NativeTextureSampler->setWrapMode(Dim, Wrapmode);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

RTwrapmode UOptiXTextureSampler::GetWrapMode(int32 Dim)
{
	RTwrapmode Mode = RT_WRAP_REPEAT;
	try
	{
		Mode = NativeTextureSampler->getWrapMode(Dim);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Mode;
}

void UOptiXTextureSampler::SetFilteringModes(RTfiltermode Minification, RTfiltermode Magnification, RTfiltermode Mipmapping)
{
	try
	{
		NativeTextureSampler->setFilteringModes(Minification, Magnification, Mipmapping);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXTextureSampler::GetFilteringModes(RTfiltermode & Minification, RTfiltermode & Magnification, RTfiltermode & Mipmapping)
{
	try
	{
		NativeTextureSampler->getFilteringModes(Minification, Magnification, Mipmapping);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXTextureSampler::SetMaxAnisotropy(float Value)
{
	try
	{
		NativeTextureSampler->setMaxAnisotropy(Value);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

float UOptiXTextureSampler::GetMaxAnisotropy()
{
	float Count = 0;
	try
	{
		Count = NativeTextureSampler->getMaxAnisotropy();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Count;
}

void UOptiXTextureSampler::SetMipLevelClamp(float Min, float Max)
{
	try
	{
		NativeTextureSampler->setMipLevelClamp(Min, Max);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

FVector2D UOptiXTextureSampler::GetMipLevelClamp()
{
	float Min;
	float Max;
	FVector2D MinMax;
	try
	{
		NativeTextureSampler->getMipLevelClamp(Min, Max);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	MinMax.X = Min;
	MinMax.Y = Max;
	return MinMax;
}

void UOptiXTextureSampler::SetMipLevelBias(float Value)
{
	try
	{
		NativeTextureSampler->setMipLevelBias(Value);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

float UOptiXTextureSampler::GetMipLevelBias()
{
	float Bias = 0;
	try
	{
		Bias = NativeTextureSampler->getMipLevelBias();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Bias;
}

int32 UOptiXTextureSampler::GetId()
{
	int32 Id = 0;
	try
	{
		Id = NativeTextureSampler->getId();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Id;
}

void UOptiXTextureSampler::SetReadMode(RTtexturereadmode Readmode)
{
	try
	{
		NativeTextureSampler->setReadMode(Readmode);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

RTtexturereadmode UOptiXTextureSampler::GetReadMode()
{
	RTtexturereadmode Mode = RT_TEXTURE_READ_ELEMENT_TYPE;
	try
	{
		Mode = NativeTextureSampler->getReadMode();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Mode;
}

void UOptiXTextureSampler::SetIndexingMode(RTtextureindexmode Mode)
{
	try
	{
		NativeTextureSampler->setIndexingMode(Mode);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

RTtextureindexmode UOptiXTextureSampler::GetIndexingMode()
{
	RTtextureindexmode Mode = RT_TEXTURE_INDEX_NORMALIZED_COORDINATES;
	try
	{
		Mode = NativeTextureSampler->getIndexingMode();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Mode;
}

void UOptiXTextureSampler::SetBufferWithTextureIndexAndMiplevel(int32 TextureArrayIndex, int MipLevel, UOptiXBuffer * Buffer)
{
	try
	{
		NativeTextureSampler->setBuffer(TextureArrayIndex, MipLevel, Buffer->GetNativeBuffer());
		OptiXBuffer = Buffer;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXBuffer * UOptiXTextureSampler::GetBufferByTextureIndexAndMiplevel(int32 TextureArrayIndex, int MipLevel)
{
	UE_LOG(OptiXPluginTextureSampler, Error, 
		TEXT("GetBufferByTextureIndexAndMiplevel not implemented yet. Returning regular buffer."));
	try
	{
		// Just check if it's actually possible to get the buffer - maybe do an equals here:
		optix::Buffer B = NativeTextureSampler->getBuffer(TextureArrayIndex, MipLevel);
		if (B != OptiXBuffer->GetNativeBuffer())
		{
			UE_LOG(OptiXPluginTextureSampler, Error, TEXT("Buffer Mismatch in Texture Sampler"));
			//return nullptr;
		}
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return OptiXBuffer;
}

void UOptiXTextureSampler::SetBuffer(UOptiXBuffer * Buffer)
{
	try
	{
		NativeTextureSampler->setBuffer(Buffer->GetNativeBuffer());
		OptiXBuffer = Buffer;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXBuffer * UOptiXTextureSampler::GetBuffer()
{
	try
	{
		// Just check if it's actually possible to get the buffer - maybe do an equals here:
		optix::Buffer B = NativeTextureSampler->getBuffer();
		if (B != OptiXBuffer->GetNativeBuffer())
		{
			UE_LOG(OptiXPluginTextureSampler, Error, TEXT("Buffer Mismatch in Texture Sampler"));
			return nullptr;
		}
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTextureSampler, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return OptiXBuffer;
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "helpers.h"
#include "random.h"
#include "prd.h"

using namespace optix;

rtDeclareVariable(Matrix4x4,     invViewProjection, , );
rtDeclareVariable(Matrix4x4,     viewProjection, , );

rtDeclareVariable(float,         scene_epsilon, , );
rtDeclareVariable(rtObject,      top_object, , );

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );
rtDeclareVariable(uint2, launch_dim,   rtLaunchDim, );

rtBuffer<uchar4, 2>   result_color;
rtBuffer<float, 2>   result_depth;

//Do not need to be the real far/near values, just need to be the same as in the opengl rendering
__device__ float convertZToLinear(float depth)
{	
	float n = 0.1f; // camera z near
	float f = 1000.0f; // camera z far
	return (2.0f * n) / (f + n - depth * (f - n));	
}

RT_PROGRAM void pinhole_camera()
{

	float2 d = make_float2(launch_index) / make_float2(launch_dim) * 2.f - 1.f;
	
	float4 rayStart_ = invViewProjection * make_float4(d.x, d.y, -1.0f, 1.0f);
	float4 rayEnd_ = invViewProjection * make_float4(d.x, d.y, 0.0f, 1.0f);
	
	float3 rayStart = make_float3(rayStart_ / rayStart_.w);
	float3 rayEnd = make_float3(rayEnd_ / rayEnd_.w);
	float3 rayDir = normalize(rayEnd - rayStart);
	
	optix::Ray ray = optix::make_Ray(rayStart, rayDir, 0, scene_epsilon, RT_DEFAULT_MAX);

	PerRayData_radiance prd;
	prd.importance = 1.f;
	prd.depth = 0;
	prd.miss = false;
	prd.hit_depth = 900.0f;
	prd.hit_lens = false; //track if the ray ever hit the lens

	rtTrace(top_object, ray, prd);
	
	float4 hitpoint = viewProjection * make_float4(rayStart + rayDir * prd.hit_depth, 1.0f);
	hitpoint = hitpoint / hitpoint.w;
	
	result_color[launch_index] = make_color(make_float4(prd.result, 1.0f));
	result_depth[launch_index] = convertZToLinear(hitpoint.z *0.5f + 0.5f);
}
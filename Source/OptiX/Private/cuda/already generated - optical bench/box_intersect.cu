//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optix_world.h>
#include "lens_intersections.h"

using namespace optix;

rtDeclareVariable(float3,  size, , );
rtDeclareVariable(float,   scene_epsilon, , );

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

rtDeclareVariable(float2, texture_coord, attribute texture_coord, );

RT_PROGRAM void intersect(int primIdx)
{
	float3 bounds[] = {-size/2.0f, size/2.0f};
	float3 invDir = 1.0f / ray.direction;
	uint3 sign = {invDir.x < 0, invDir.y < 0, invDir.z < 0};

	float tmin = (bounds[sign.x].x - ray.origin.x) * invDir.x;
	float tmax = (bounds[1 - sign.x].x - ray.origin.x) * invDir.x;
	float tymin = (bounds[sign.y].y - ray.origin.y) * invDir.y;
	float tymax = (bounds[1 - sign.y].y - ray.origin.y) * invDir.y;

	if ((tmin > tymax) || (tymin > tmax)) return;
	if (tymin > tmin) tmin = tymin;
	if (tymax < tmax) tmax = tymax;

	float tzmin = (bounds[sign.z].z - ray.origin.z) * invDir.z;
	float tzmax = (bounds[1 - sign.z].z - ray.origin.z) * invDir.z;

	if ((tmin > tzmax) || (tzmin > tmax)) return;
	if (tzmin > tmin) tmin = tzmin;
	//if (tzmax < tmax) tmax = tzmax;
	
	if(rtPotentialIntersection(tmin)) {
		float3 hit_point = ray.origin + ray.direction * tmin;
		texture_coord = make_float2(-1.0f, -1.0f);
		if(tmin != tzmin && tmin != tymin){ //x-side
			texture_coord = (make_float2(hit_point.y, hit_point.z) - make_float2(-size.y / 2.0f, -size.z / 2.0f))
							/make_float2(size.y, size.z);
			//rtPrintf("%f, %f\n", texture_coord.x, texture_coord.y);
		}
		rtReportIntersection(0);
	}
}

RT_PROGRAM void bounds (int, optix::Aabb* aabb)
{
	aabb->m_min = -size/2.0f;
	aabb->m_max = size/2.0f;
}

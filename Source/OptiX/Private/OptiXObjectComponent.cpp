#include "OptiXObjectComponent.h"

#include "Runtime/Engine/Classes/Engine/TextureRenderTargetCube.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectIterator.h"



#include "OptiXLaserActor.h"
#include "OptiXTargetComponent.h"
#include "OptiXModule.h"

UOptiXObjectComponent::UOptiXObjectComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Register the component with OptiX and get the currently singular context. 
	// If this works, I should move the Context from the singleton away somehow into the scene to make it accessible via blueprints etc.
	// Maybe have 1 context per level? That would make most sense and allow for different scenes in a world, not that this is needed anyway.
	// But the singleton thing seems really limiting.

	// Only as a safety measure because I have no idea how the editor factory actually sets this stuff.

	bIsInitialized = false;

	UE_LOG(LogTemp, Display, TEXT("OptiX Component Constructor"));
	PrimaryComponentTick.bCanEverTick = false; // Don't need to tick 
	bWantsOnUpdateTransform = true;

}


void UOptiXObjectComponent::OnUnregister()
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Component OnUnregister"));

	Super::OnUnregister();

	//if (OptiXGeometryGroup != nullptr)
	//{
	//	OptiXGeometryGroup->RemoveChild(OptiXGeometryInstance);
	//}

	//OptiXGeometry = nullptr;
	//OptiXContext = nullptr;
	//OptiXGeometryGroup = nullptr; // Dunno if that's needed?
	//OptiXGeometryInstance = nullptr; // Dunno if that's needed?
}


void UOptiXObjectComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Display, TEXT("OptiX Component BeginPlay"));

	// Create and Init the optix stuff here

	//InitOptiXComponent();	
	RegisterOptiXComponent();
	QueueOptiXContextUpdate();
}

void UOptiXObjectComponent::RegisterOptiXComponent()
{
	FOptiXModule::Get().GetOptiXContextManager()->RegisterOptiXComponent(this);
}

void UOptiXObjectComponent::QueueOptiXContextUpdate()
{
	if (!bUpdateQueued)
	{
		FOptiXModule::Get().GetOptiXContextManager()->QueueComponentUpdate(this);
	}
}

void UOptiXObjectComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	UE_LOG(LogTemp, Display, TEXT(" ---------------- OptiX Component EndPlay, starting cleanup."));

	CleanOptiXComponent();
}

void UOptiXObjectComponent::OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport/* = ETeleportType::None*/)
{
	Super::OnUpdateTransform(EUpdateTransformFlags::SkipPhysicsUpdate, Teleport);

	UpdateOptiXComponent();
}


void UOptiXObjectComponent::InitOptiXComponent(FRHICommandListImmediate & RHICmdList)
{
	check(IsInRenderingThread());

	OptiXContext = FOptiXModule::Get().GetContext();
	OptiXPTXDir = FOptiXModule::Get().OptiXPTXDir;

	IOptiXComponentInterface::InitOptiXComponent(RHICmdList); // Calls the other stuff

	bIsInitialized = true;
	SetComponentTickEnabled(false);
	UE_LOG(LogTemp, Display, TEXT("Initialized Object Component"));
}

void UOptiXObjectComponent::CleanOptiXComponent()
{

	// Remove all the optix stuff again here from top to bottom


	OptiXGeometryGroup->RemoveChild(OptiXGeometryInstance);
	//OptiXGeometryGroup = nullptr; // This should trigger the GC and eat the object if this is the last reference

	OptiXGeometryInstance->RemoveFromRoot();
	//OptiXGeometryInstance->DestroyOptiXObject();
	OptiXGeometryInstance = nullptr;

	OptiXGeometry->RemoveFromRoot();
	OptiXGeometry->DestroyOptiXObject();
	OptiXGeometry = nullptr;

	OptiXMaterial->RemoveFromRoot();
	OptiXMaterial->DestroyOptiXObject();
	OptiXMaterial = nullptr;
}

void UOptiXObjectComponent::SetUpdateQueued(bool UpdateQueued)
{
	bUpdateQueued.AtomicSet(UpdateQueued);
}




UOptiXCubemapComponent::UOptiXCubemapComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Register the component with OptiX and get the currently singular context. 
	// If this works, I should move the Context from the singleton away somehow into the scene to make it accessible via blueprints etc.
	// Maybe have 1 context per level? That would make most sense and allow for different scenes in a world, not that this is needed anyway.
	// But the singleton thing seems really limiting.

	// Only as a safety measure because I have no idea how the editor factory actually sets this stuff.

	bIsInitialized = false;

	UE_LOG(LogTemp, Display, TEXT("OptiX Cubemap Component Constructor"));
	PrimaryComponentTick.bCanEverTick = false; // Don't need to tick 
	bWantsOnUpdateTransform = true;


	bCaptureEveryFrame = false;
	bCaptureOnMovement = false;


}


void UOptiXCubemapComponent::OnUnregister()
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Component OnUnregister"));

	Super::OnUnregister();

	//if (OptiXGeometryGroup != nullptr)
	//{
	//	OptiXGeometryGroup->RemoveChild(OptiXGeometryInstance);
	//}

	//OptiXGeometry = nullptr;
	//OptiXContext = nullptr;
	//OptiXGeometryGroup = nullptr; // Dunno if that's needed?
	//OptiXGeometryInstance = nullptr; // Dunno if that's needed?
}


void UOptiXCubemapComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Display, TEXT("OptiX Component BeginPlay"));

	// Create and Init the optix stuff here

	TArray<AActor*> FoundLaserActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AOptiXLaserActor::StaticClass(), FoundLaserActors);

	for (AActor* Actor : FoundLaserActors)
	{
		HideComponent(Cast<AOptiXLaserActor>(Actor)->LineInstancedStaticMeshComponent);
	}


	for (TObjectIterator<UOptiXCubemapComponent> Itr; Itr; ++Itr)
	{
		// Filter out objects not contained in the target world.
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}
		HideActorComponents(Itr->GetOwner());
	}

	for (TObjectIterator<UOptiXTargetComponent> Itr; Itr; ++Itr)
	{
		// Filter out objects not contained in the target world.
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}
		HideActorComponents(Itr->GetOwner());
	}

	for (TObjectIterator<UWidgetComponent> Itr; Itr; ++Itr)
	{
		// Filter out objects not contained in the target world.
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}

		if (Itr->GetWidgetClass() != NULL && Itr->GetWidgetClass()->GetName().Contains("ScreenWidget"))
		{
			HideActorComponents(Itr->GetOwner());
		}
	}


	HideActorComponents(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	HideActorComponents(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	CubeRenderTarget = NewObject<UTextureRenderTargetCube>();
	CubeRenderTarget->Init(1024, PF_B8G8R8A8);
	CubeRenderTarget->UpdateResource();
	TextureTarget = CubeRenderTarget;

	CaptureScene();
	CaptureSceneDeferred();
	CubeRenderTarget->UpdateResource();
	bCubemapCaptured.AtomicSet(true);

	//InitOptiXComponent();	
	RegisterOptiXComponent();
	QueueOptiXContextUpdate();


}

void UOptiXCubemapComponent::RegisterOptiXComponent()
{
	FOptiXModule::Get().GetOptiXContextManager()->RegisterOptiXComponent(this);
}

void UOptiXCubemapComponent::QueueOptiXContextUpdate()
{
	if (!bUpdateQueued)
	{
		FOptiXModule::Get().GetOptiXContextManager()->QueueComponentUpdate(this);
	}
}

void UOptiXCubemapComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	UE_LOG(LogTemp, Display, TEXT(" ---------------- OptiX Component EndPlay, starting cleanup."));

	CleanOptiXComponent();
}

void UOptiXCubemapComponent::OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport/* = ETeleportType::None*/)
{
	Super::OnUpdateTransform(EUpdateTransformFlags::SkipPhysicsUpdate, Teleport);

	UpdateOptiXComponent();
}


void UOptiXCubemapComponent::InitOptiXComponent(FRHICommandListImmediate & RHICmdList)
{
	check(IsInRenderingThread());

	OptiXContext = FOptiXModule::Get().GetContext();
	OptiXPTXDir = FOptiXModule::Get().OptiXPTXDir;

	OptiXCubemapId = FOptiXModule::Get().GetOptiXContextManager()->RequestCubemapId();


	IOptiXComponentInterface::InitOptiXComponent(RHICmdList); // Calls the other stuff
	InitCubemap(RHICmdList);

	bIsInitialized = true;
	SetComponentTickEnabled(false);
	UE_LOG(LogTemp, Display, TEXT("Initialized Object Component"));
}

void UOptiXCubemapComponent::CleanOptiXComponent()
{

	FOptiXModule::Get().GetOptiXContextManager()->DeleteCubemapId(OptiXCubemapId);

	// Remove all the optix stuff again here from top to bottom

	if (OptiXContext == NULL)
		return;
	//OptiXGeometryGroup = nullptr; // This should trigger the GC and eat the object if this is the last reference

	OptiXGeometryInstance->RemoveFromRoot();
	OptiXGeometryInstance->DestroyOptiXObject();
	OptiXGeometryInstance = nullptr;

	OptiXGeometry->RemoveFromRoot();
	OptiXGeometry->DestroyOptiXObject();
	OptiXGeometry = nullptr;

	OptiXMaterial->RemoveFromRoot();
	OptiXMaterial->DestroyOptiXObject();
	OptiXMaterial = nullptr;
}

void UOptiXCubemapComponent::SetUpdateQueued(bool UpdateQueued)
{
	bUpdateQueued.AtomicSet(UpdateQueued);
}

void UOptiXCubemapComponent::RequestCubemapUpdate()
{
	CaptureScene();
	FOptiXModule::Get().GetOptiXContextManager()->RequestCubemapUpdate(this);
}

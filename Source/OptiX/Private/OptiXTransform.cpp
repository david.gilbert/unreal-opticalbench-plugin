#include "OptiXTransform.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXModule.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

DEFINE_LOG_CATEGORY(OptiXPluginTransform);

void UOptiXTransform::BeginDestroy()
{
	// Tell optix to clean up
	UE_LOG(LogTemp, Warning, TEXT("OptiX Transform BeginDestroy"));

	DestroyOptiXObject();
	Super::BeginDestroy();
}

void UOptiXTransform::DestroyOptiXObject()
{
	if (NativeTransform != NULL)
	{
		FOptiXModule::Get().GetOptiXContextManager()->TransformsToDeleteQueue.Enqueue(NativeTransform);
		//NativeTransform->destroy();
	}

	OptiXChild = nullptr; // Don't explicitly delete the child, maybe we should TODO
	NativeTransform = NULL;
}

void UOptiXTransform::UpdateTransform()
{
	check(IsInRenderingThread());
	try
	{
		// According to the optix doc false == row major
		FMatrix Inverse = TransformMatrix.Inverse();
		NativeTransform->setMatrix(true, &TransformMatrix.M[0][0], &Inverse.M[0][0]); // TODO - find out if false or true is column or row major
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTransform, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}

}

void UOptiXTransform::SetChild(UObject * Child)
{
	if (UOptiXGeometryGroup* GeomGroup = Cast<UOptiXGeometryGroup>(Child))
	{
		NativeTransform->setChild(GeomGroup->GetNativeGroup());
		OptiXChild = Child;
	}
	else if (UOptiXGeometryInstance* GeomInstance = Cast<UOptiXGeometryInstance>(Child))
	{
		NativeTransform->setChild(GeomInstance->GetNativeInstance());
		OptiXChild = Child;
	}
	else return;
}

RTobjecttype UOptiXTransform::GetChildType()
{
	return NativeTransform->getChildType();
}

UObject * UOptiXTransform::GetChild()
{
	return OptiXChild;
}

void UOptiXTransform::SetMatrix(FMatrix Matrix)
{	
	// TODO This should maybe be a critical section?
	TransformMatrix = Matrix;
}


void UOptiXTransform::SetMatrixImmediate(FMatrix Matrix)
{
	// WARNING! NON THREAD SAFE
	try
	{
		// According to the optix doc false == row major
		TransformMatrix = Matrix;
		FMatrix Inverse = TransformMatrix.Inverse();
		NativeTransform->setMatrix(true, &TransformMatrix.M[0][0], &Inverse.M[0][0]); // TODO - find out if false or true is column or row major
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTransform, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}


FMatrix UOptiXTransform::GetMatrix()
{
	return TransformMatrix;
}

void UOptiXTransform::SetMotionRange(float TimeBegin, float TimeEnd)
{
	try
	{
		NativeTransform->setMotionRange(TimeBegin, TimeEnd);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTransform, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

FVector2D UOptiXTransform::GetMotionRange()
{
	FVector2D V = FVector2D();
	float X;
	float Y;
	try
	{
		NativeTransform->getMotionRange(X, Y);
		V.X = X;
		V.Y = Y;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginTransform, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}

	return V;
}

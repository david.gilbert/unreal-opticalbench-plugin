#include "OptiXAcceleration.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

#include "OptiXModule.h"

DEFINE_LOG_CATEGORY(OptiXPluginAcceleration);

void UOptiXAcceleration::BeginDestroy()
{
	Super::BeginDestroy();
	DestroyOptiXObject();
}

void UOptiXAcceleration::DestroyOptiXObject()
{
	if (NativeAcceleration != NULL)
	{
		//NativeAcceleration->destroy();
		FOptiXModule::Get().GetOptiXContextManager()->AccelerationsToDeleteQueue.Enqueue(NativeAcceleration);
	}
	NativeAcceleration = NULL;
}

void UOptiXAcceleration::Validate()
{
	try
	{
		NativeAcceleration->validate();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXAcceleration::MarkDirty()
{
	try
	{
		NativeAcceleration->markDirty();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

bool UOptiXAcceleration::IsDirty()
{
	bool B = false;
	try
	{
		B = NativeAcceleration->isDirty();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return B;
}

void UOptiXAcceleration::SetProperty(FString Name, FString Value)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		std::string V = std::string(TCHAR_TO_ANSI(*Value));

		NativeAcceleration->setProperty(N, V);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

FString UOptiXAcceleration::GetProperty(FString Name)
{
	FString Property;
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		std::string V = NativeAcceleration->getProperty(N);
		Property = FString(V.c_str());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Property;
}

void UOptiXAcceleration::SetBuilder(FString Builder)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Builder));

		NativeAcceleration->setBuilder(N);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

FString UOptiXAcceleration::GetBuilder()
{
	FString Property;
	try
	{
		std::string V = NativeAcceleration->getBuilder();
		Property = FString(V.c_str());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Property;
}

void UOptiXAcceleration::SetTraverser(FString Traverser)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Traverser));

		NativeAcceleration->setTraverser(N);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

FString UOptiXAcceleration::GetTraverser()
{
	FString Property;
	try
	{
		std::string V = NativeAcceleration->getTraverser();
		Property = FString(V.c_str());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Property;
}

RTsize UOptiXAcceleration::GetDataSize()
{
	RTsize Size = 0;
	try
	{
		Size = NativeAcceleration->getDataSize();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Size;
}

void UOptiXAcceleration::GetData(void * Data)
{
	try
	{
		NativeAcceleration->getData(Data);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXAcceleration::SetData(void * Data, RTsize Size)
{
	try
	{
		NativeAcceleration->setData(Data, Size);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginAcceleration, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

#include "OptiXGeometryGroup.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXGeometryGroup.h"
#include "OptiXModule.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

DEFINE_LOG_CATEGORY(OptiXPluginGeometryGroup);

// TODO LOOK INTO CONSTRUCTORS

void UOptiXGeometryGroup::BeginDestroy()
{
	Super::BeginDestroy();

	// Tell optix to clean up
	UE_LOG(LogTemp, Warning, TEXT("OptiX Geometry Group BeginDestroy"));
	// Remove all the children:
	DestroyOptiXObject();
	UE_LOG(LogTemp, Warning, TEXT("OptiX Geometry Group Finished BeginDestroy"));

}

void UOptiXGeometryGroup::DestroyOptiXObject()
{
	for (UOptiXGeometryInstance* I : OptiXGeometryInstances)
	{
		//RemoveChild(I);
		// For some godforsaken reason this crashes optix...
	}

	if (NativeGeometryGroup != NULL)
	{
		//NativeGeometryGroup->destroy();
		FOptiXModule::Get().GetOptiXContextManager()->GeometryGroupToDeleteQueue.Enqueue(NativeGeometryGroup);

	}

	OptiXGeometryInstances.Empty();
	NativeGeometryGroup = NULL;
}

void UOptiXGeometryGroup::SetAcceleration(UOptiXAcceleration* Accel)
{
	try
	{
		NativeGeometryGroup->setAcceleration(Accel->GetNativeAcceleration());
		Acceleration = Accel;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometryGroup, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXAcceleration* UOptiXGeometryGroup::GetAcceleration()
{
	UOptiXAcceleration* Ptr = nullptr;
	try
	{
		optix::Acceleration Native = NativeGeometryGroup->getAcceleration();

		if (Native != Acceleration->GetNativeAcceleration())
		{
			FString Message = "Acceleration Mismatch in Geometry Group!";
			UE_LOG(OptiXPluginGeometryGroup, Error, TEXT("OptiX Error: %s"), *Message);
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
		}

		Ptr = Acceleration;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometryGroup, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Ptr;
}

void UOptiXGeometryGroup::SetChildCount(uint8 Count)
{
	NativeGeometryGroup->setChildCount(Count);
	ChildCount = Count;

	int32 Diff = Count - OptiXGeometryInstances.Num();

	if (Diff > 0)
	{
		OptiXGeometryInstances.SetNum(Count);
	}
	else if (Diff < 0)
	{
		// Theoretically remove children but don't do this for now! TODOOOO
	}
}

uint8 UOptiXGeometryGroup::GetChildCount()
{
	// Check if our count is the same
	uint8 OptiXCount = NativeGeometryGroup->getChildCount();
	if (OptiXCount != ChildCount)
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGeometryGroup Child Counts are mismatched: Native: %i - %i UObject \n"), OptiXCount, ChildCount);
	}
	return OptiXCount;
}

void UOptiXGeometryGroup::SetChild(uint8 Index, UOptiXGeometryInstance * Child)
{
	// This will probably mess with child counts but shouldn't.

	if (!OptiXGeometryInstances.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGeometryGroup: Trying to insert child in non-existing place! \n"));
		return;
	}
	NativeGeometryGroup->setChild(Index, Child->GetNativeInstance());
	OptiXGeometryInstances.Insert(Child, Index);
}

UOptiXGeometryInstance * UOptiXGeometryGroup::GetChild(uint8 Index)
{
	if (!OptiXGeometryInstances.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGeometryGroup: Child does not exist! \n"));
	}

	return OptiXGeometryInstances[Index];
}

uint8 UOptiXGeometryGroup::AddChild(UOptiXGeometryInstance * Child)
{
	ChildCount++;
	uint8 NativeIndex = static_cast<uint8>(NativeGeometryGroup->addChild(Child->GetNativeInstance()));
	uint8 Index = OptiXGeometryInstances.Add(Child);
	if (NativeIndex != Index)
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGeometryGroup: Index Mismatch while adding! \n"));
	}
	return Index;
}

uint8 UOptiXGeometryGroup::RemoveChild(UOptiXGeometryInstance * Child)
{
	uint8 Index = GetChildIndex(Child);
	RemoveChildByIndex(Index);
	return Index;
}

void UOptiXGeometryGroup::RemoveChildByIndex(uint8 Index)
{
	if (!OptiXGeometryInstances.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGeometryGroup: Index Mismatch while removing!"));
		return;
	}

	UE_LOG(LogTemp, Display, TEXT("OptiXGeometryGroup: Removing optix child!"));
	UE_LOG(LogTemp, Display, TEXT("Child Count: %i "), GetChildCount());
	UE_LOG(LogTemp, Display, TEXT("Index to remove: %i "), Index);
	UE_LOG(LogTemp, Display, TEXT("Size of TArray: %i "), OptiXGeometryInstances.Num());


	try
	{
		if (IsInGameThread())
		{
			FOptiXModule::Get().GetOptiXContextManager()->GeometryGroupChildrenToRemoveQueue.Enqueue(TPair<optix::GeometryGroup, uint32>(NativeGeometryGroup, Index));
		}
		else
		{
			NativeGeometryGroup->removeChild(static_cast<unsigned int>(Index));
		}
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometryGroup, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	ChildCount--; // Not sure if OptiX internally resizes the array or if the child count stays the same?

	UE_LOG(LogTemp, Display, TEXT("Done Removing Native"), OptiXGeometryInstances.Num());
	UE_LOG(LogTemp, Display, TEXT("Child Count After remove native: %i "), GetChildCount());
	FName Name = OptiXGeometryInstances[Index]->GetFName();
	UE_LOG(LogTemp, Display, TEXT("Name of child to remove: %s"), *Name.ToString());

	OptiXGeometryInstances.RemoveAt(Index); // Will shuffle the remaining indices correctly - hopefully.
	UE_LOG(LogTemp, Display, TEXT("Finished Removing \n"));

}

uint8 UOptiXGeometryGroup::GetChildIndex(UOptiXGeometryInstance * Child)
{
	return OptiXGeometryInstances.Find(Child);
}

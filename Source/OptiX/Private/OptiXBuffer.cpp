#include "OptiXBuffer.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

//#include "OptiXModule.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

#include "OptiXModule.h"

DEFINE_LOG_CATEGORY(OptiXPluginBuffer);

void UOptiXBuffer::BeginDestroy()
{
	UE_LOG(LogTemp, Warning, TEXT("OptiX Buffer BeginDestroy"));
	DestroyOptiXObject();
	Super::BeginDestroy();
}

void UOptiXBuffer::DestroyOptiXObject()
{
	if (NativeBuffer != NULL)
	{
		UE_LOG(LogTemp, Display, TEXT("Buffer Name: %s!"), *Name);
		try
		{
			//NativeBuffer->destroy();
			FOptiXModule::Get().GetOptiXContextManager()->BuffersToDeleteQueue.Enqueue(NativeBuffer);
		}
		catch (optix::Exception& E)
		{
			FString Message = FString(E.getErrorString().c_str());
			UE_LOG(OptiXPluginBuffer, Fatal, TEXT("OptiX Error: %s"), *Message);
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
		}
	}

	NativeBuffer = NULL;
}

void UOptiXBuffer::Validate()
{
	try
	{
		NativeBuffer->validate();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::SetFormat(RTformat Format)
{
	try
	{
		NativeBuffer->setFormat(Format);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

RTformat UOptiXBuffer::GetFormat()
{
	return NativeBuffer->getFormat();
}

void UOptiXBuffer::SetElementSize(int32 Size)
{
	try
	{
		NativeBuffer->setElementSize(Size);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::SetElementSizeNative(RTsize Size)
{
	try
	{
		NativeBuffer->setElementSize(Size);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}


RTsize UOptiXBuffer::GetElementSizeNative()
{
	RTsize S = 0;
	try
	{
		S = NativeBuffer->getElementSize();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return S;
}

int32 UOptiXBuffer::GetElementSize()
{
	int32 S = 0;
	try
	{
		S = NativeBuffer->getElementSize();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return S;
}

void UOptiXBuffer::MarkDirty()
{
	try
	{
		NativeBuffer->markDirty();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::SetSize1D(int32 Width)
{
	try
	{
		NativeBuffer->setSize(Width);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::SetSize1DNative(RTsize Width)
{
	try
	{
		NativeBuffer->setSize(Width);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

RTsize UOptiXBuffer::GetSize1DNative()
{
	RTsize Width;
	try
	{
		NativeBuffer->getSize(Width);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Width;
}

int32 UOptiXBuffer::GetSize1D()
{
	RTsize Width;
	try
	{
		NativeBuffer->getSize(Width);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return static_cast<int32>(Width);
}

RTsize UOptiXBuffer::GetMipLevelSize1DNative(uint8 Level)
{
	RTsize Width;
	try
	{
		NativeBuffer->getMipLevelSize(Level, Width);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Width;
}

int32 UOptiXBuffer::GetMipLevelSize1D(uint8 Level)
{
	RTsize Width;
	try
	{
		NativeBuffer->getMipLevelSize(Level, Width);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return static_cast<int32>(Width);
}

void UOptiXBuffer::SetSize2D(int32 Width, int32 Height)
{
	try
	{
		NativeBuffer->setSize(Width, Height);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::SetSize2DNative(RTsize Width, RTsize Height)
{
	try
	{
		NativeBuffer->setSize(Width, Height);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

RTsize2 UOptiXBuffer::GetSize2DNative()
{
	RTsize2 S;
	RTsize A;
	RTsize B;
	NativeBuffer->getSize(A, B);
	S.X = A;
	S.Y = B;
	return S;
}

FIntPoint UOptiXBuffer::GetSize2D()
{
	FIntPoint S;
	RTsize A;
	RTsize B;
	try
	{
		NativeBuffer->getSize(A, B);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	S.X = A;
	S.Y = B;
	return S;
}

FIntPoint UOptiXBuffer::GetMipLevelSize2D(uint8 Level)
{
	FIntPoint S;
	RTsize A = 0;
	RTsize B = 0;
	try
	{
		NativeBuffer->getMipLevelSize(A, B);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}	
	S.X = A;
	S.Y = B;
	return S;
}

RTsize2 UOptiXBuffer::GetMipLevelSize2DNative(uint8 Level)
{
	RTsize2 S;
	RTsize A = 0;
	RTsize B = 0;
	try
	{
		NativeBuffer->getMipLevelSize(A, B);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}	
	S.X = A;
	S.Y = B;
	return S;
}

void UOptiXBuffer::SetSize3DNative(RTsize Width, RTsize Height, RTsize Depth)
{
	try
	{
		NativeBuffer->setSize(Width, Height, Depth);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::SetSize3D(int32 Width, int32 Height, int32 Depth)
{
	try
	{
		NativeBuffer->setSize(Width, Height, Depth);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

FIntVector UOptiXBuffer::GetSize3D()
{
	FIntVector S;
	RTsize A;
	RTsize B;
	RTsize C;
	try
	{
		NativeBuffer->getSize(A, B, C);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	S.X = A;
	S.Y = B;	
	S.Z = C;
	return S;
}


RTsize3 UOptiXBuffer::GetSize3DNative()
{
	RTsize3 S;
	RTsize A;
	RTsize B;
	RTsize C;
	try
	{
		NativeBuffer->getSize(A, B, C);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	S.X = A;
	S.Y = B;
	S.Z = C;
	return S;
}

FIntVector UOptiXBuffer::GetMipLevelSize3D(uint8 Level)
{
	FIntVector S;
	RTsize A = 0;
	RTsize B = 0;
	RTsize C = 0;
	try
	{
		NativeBuffer->getMipLevelSize(A, B, C);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	S.X = A;
	S.Y = B;
	S.Z = C;
	return S;
}

RTsize3 UOptiXBuffer::GetMipLevelSize3DNative(uint8 Level)
{
	RTsize3 S;
	RTsize A = 0;
	RTsize B = 0;
	RTsize C = 0;
	try
	{
		NativeBuffer->getMipLevelSize(A, B, C);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	S.X = A;
	S.Y = B;
	S.Z = C;
	return S;
}

void UOptiXBuffer::SetMipLevelCount(uint8 Count)
{
	try
	{
		NativeBuffer->setMipLevelCount(Count);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

uint8 UOptiXBuffer::GetMipLevelCount()
{
	uint8 Count = 0;
	try
	{
		Count = NativeBuffer->getMipLevelCount();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Count;
}

int UOptiXBuffer::GetId()
{
	int Id = 0;
	try
	{
		Id = NativeBuffer->getId();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Id;
}

void UOptiXBuffer::Map(uint8 Level, uint8 MapFlags)
{
	try
	{
		NativeBuffer->map(Level, MapFlags);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}


void * UOptiXBuffer::MapNative(uint8 Level, uint8 MapFlags, void * UserOwned)
{
	void * Ptr = nullptr;
	try
	{
		Ptr = NativeBuffer->map(Level, MapFlags, UserOwned);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Ptr;
}

void UOptiXBuffer::Unmap(uint8 Level)
{
	try
	{
		NativeBuffer->unmap(Level);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

// TODO: Wrap those a bit better

void UOptiXBuffer::BindProgressiveStream(UOptiXBuffer * Source)
{
	try
	{
		NativeBuffer->bindProgressiveStream(Source->GetNativeBuffer());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::GetProgressiveUpdateReady(int * Ready, unsigned int * SubframeCount, unsigned int * MaxSubframes)
{
	try
	{
		NativeBuffer->getProgressiveUpdateReady(Ready, SubframeCount, MaxSubframes);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

bool UOptiXBuffer::GetProgressiveUpdateReady()
{
	bool Ready = false;
	try
	{
		Ready = NativeBuffer->getProgressiveUpdateReady();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Ready;
}

void UOptiXBuffer::GetDevicePointer(int32 OptiXDeviceOrdinal, void ** DevicePointer)
{
	try
	{
		NativeBuffer->getDevicePointer(OptiXDeviceOrdinal, DevicePointer);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void * UOptiXBuffer::GetDevicePointer(int32 OptiXDeviceOrdinal)
{
	void* Ptr = nullptr;
	try
	{
		Ptr = NativeBuffer->getDevicePointer(OptiXDeviceOrdinal);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Ptr;
}

void UOptiXBuffer::SetDevicePointer(int32 OptiXDeviceOrdinal, void * DevicePointer)
{
	try
	{
		NativeBuffer->setDevicePointer(OptiXDeviceOrdinal, DevicePointer);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

bool UOptiXBuffer::GetProgressiveUpdateReady(unsigned int & SubframeCount)
{
	bool Ready = false;
	try
	{
		Ready = NativeBuffer->getProgressiveUpdateReady(SubframeCount);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Ready;
}

bool UOptiXBuffer::GetProgressiveUpdateReady(unsigned int & SubframeCount, unsigned int & MaxSubframes)
{
	bool Ready = false;
	try
	{
		Ready = NativeBuffer->getProgressiveUpdateReady(SubframeCount, MaxSubframes);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Ready;
}

void UOptiXBuffer::SetAttribute(RTbufferattribute Attrib, RTsize Size, void * P)
{
	try
	{
		NativeBuffer->setAttribute(Attrib, Size, P);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXBuffer::GetAttribute(RTbufferattribute Attrib, RTsize Size, void * P)
{
	try
	{
		NativeBuffer->getAttribute(Attrib, Size, P);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginBuffer, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "OptiXVRPawn.h"
#include "Components/StaticMeshComponent.h"

#include "PickupActorInterface.h"
#include "OptiXModule.h"



// Sets default values
AOptiXVRPawn::AOptiXVRPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AOptiXVRPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AOptiXVRPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AOptiXVRPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AOptiXVRPawn::UpdateTranslation(UPrimitiveComponent* Interaction)
{
	if (GrabbedLever == NULL)
	{
		return;
	}
	FString N = GrabbedLever->GetName();
	FVector Distance = GetDistanceVector(Interaction);
	FVector Loc;
	float ScaleX;

	if (N.EndsWith("X"))
	{
		ScaleX = Distance.X;
		Loc = FVector(Distance.X - GrabDistanceX, 0, 0) * Damping;

	}
	else if (N.EndsWith("Y"))
	{
		ScaleX = Distance.Y;
		Loc = FVector(0, Distance.Y - GrabDistanceY, 0) * Damping;
	}
	else if (N.EndsWith("Z"))
	{
		ScaleX = Distance.Z;
		Loc = FVector(0, 0, Distance.Z - GrabDistanceZ) * Damping;
	}
	else
	{
		return;
	}

	GrabbedLever->GetOwner()->SetActorLocation(GrabbedLever->GetOwner()->GetActorLocation() + Loc);

	TArray<USceneComponent*> Parents;
	GrabbedLever->GetParentComponents(Parents);

	if (ScaleX < 0)
	{
		GrabbedLever->SetWorldScale3D(FVector(-0.6, -0.6, -0.6));
	}
	else
	{
		GrabbedLever->SetWorldScale3D(FVector(0.6, 0.6, 0.6));
	}

	Parents[0]->SetWorldScale3D(FVector(ScaleX / 39.0, 0.6, 0.6));


	FVector NewDist = GetDistanceVector(Interaction);
	GrabDistanceX = NewDist.X;
	GrabDistanceY = NewDist.Y;
	GrabDistanceZ = NewDist.Z;
}

FVector AOptiXVRPawn::GetDistanceVector(UPrimitiveComponent * Other)
{
	return Other->GetComponentLocation() - GrabbedLever->GetOwner()->GetActorLocation();
}

UMaterialInstanceDynamic* AOptiXVRPawn::GetMIDOrtho()
{
	return FOptiXModule::Get().GetOptiXContextManager()->GetOptiXMIDOrtho();
}

void AOptiXVRPawn::RequestOrthoPass(const FMinimalViewInfo& ViewInfo)
{

	FSceneViewProjectionData ProjectionData;
	ProjectionData.ViewOrigin = ViewInfo.Location;
	ProjectionData.ViewRotationMatrix = FInverseRotationMatrix(ViewInfo.Rotation) * FMatrix(
		FPlane(0, 0, 1, 0),
		FPlane(1, 0, 0, 0),
		FPlane(0, 1, 0, 0),
		FPlane(0, 0, 0, 1));

	ProjectionData.ProjectionMatrix = ViewInfo.CalculateProjectionMatrix();
	ProjectionData.SetConstrainedViewRectangle(FOptiXModule::Get().GetOptiXContextManager()->GetViewRectanglePerEye());
	
	FOptiXModule::Get().GetOptiXContextManager()->OrthoMatrix = ProjectionData.ComputeViewProjectionMatrix();	
	FOptiXModule::Get().GetOptiXContextManager()->bRequestOrthoPass.AtomicSet(true);
}

UStaticMeshComponent * AOptiXVRPawn::GetNearestMeshComponent(UPrimitiveComponent * Other)
{
	TArray<UPrimitiveComponent*> OverlappingComponents;
	Other->GetOverlappingComponents(OverlappingComponents);

	UPrimitiveComponent* Nearest = nullptr;
	float NearestOverlap = 10000;
	FVector OtherLoc = Other->GetComponentLocation();

	for (UPrimitiveComponent* Comp : OverlappingComponents)
	{
		if (Comp->GetOwner()->GetClass()->ImplementsInterface(UNewPickupActorInterface::StaticClass()))
		{
			float Dist = (Comp->GetComponentLocation() - OtherLoc).Size();
			if (Dist < NearestOverlap)
			{
				Nearest = Comp;
				NearestOverlap = Dist;
			}
			
		}
	}
	return Cast<UStaticMeshComponent>(Nearest);
}

AActor * AOptiXVRPawn::GetActorNearHand(UPrimitiveComponent * Hand)
{
	TArray<AActor*> OverlappingActors;
	Hand->GetOverlappingActors(OverlappingActors);

	AActor* Nearest = nullptr;
	float NearestOverlap = 10000;
	FVector OtherLoc = Hand->GetComponentLocation();

	for (AActor* Actor : OverlappingActors)
	{
		if (Actor->GetClass()->ImplementsInterface(UNewPickupActorInterface::StaticClass()))
		{
			float Dist = (Actor->GetActorLocation() - OtherLoc).Size();
			if (Dist < NearestOverlap)
			{
				Nearest = Actor;
				NearestOverlap = Dist;
			}

		}
	}
	return Nearest;
}

void AOptiXVRPawn::CaptureDeferredHelper(USceneCaptureComponent2D* SceneCapture)
{
	SceneCapture->CaptureSceneDeferred();
}
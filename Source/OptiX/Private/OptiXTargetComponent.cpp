// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXTargetComponent.h"

#include "UObject/ConstructorHelpers.h"

#include "OptiXModule.h"



UOptiXTargetComponent::UOptiXTargetComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{	
	CurrentTexturePattern = ETexturePattern::TP_CHECKER;
	//FString N = "Texture2D'/OptiX/Targets/Checker.Checker'";
	//Texture = LoadObject<UTexture2D>(this, *N);

	static ConstructorHelpers::FObjectFinder<UTexture2D> TextureLoader(TEXT("Texture2D'/OptiX/Targets/Checker.Checker'"));
	Texture = TextureLoader.Object;

	if (Texture != NULL)
	{
		TextureSize = FIntPoint(Texture->GetSizeX(), Texture->GetSizeY());
		TargetSize = FVector(2.0f, 20.0f, 20.0f * static_cast<float>(TextureSize.Y) / static_cast<float>(TextureSize.X));
	}
}

// God sometimes unreal is annoying
void UOptiXTargetComponent::BeginPlay()
{
	//CurrentTexturePattern = ETexturePattern::TP_CIRCLES;

	FString N = "Texture2D'/OptiX/Targets/Checker.Checker'";
	if (CurrentTexturePattern == ETexturePattern::TP_BLACK)
	{
		N = "Texture2D'/OptiX/Targets/Black.Black'";
	}
	else if (CurrentTexturePattern == ETexturePattern::TP_CIRCLES)
	{
		N = "Texture2D'/OptiX/Targets/Circles.Circles'";
	}
	else if (CurrentTexturePattern == ETexturePattern::TP_GRID)
	{
		N = "Texture2D'/OptiX/Targets/Raster.Raster'";
	}
	Texture = nullptr;
	Texture = LoadObject<UTexture2D>(this, *N);
	TextureSize = FIntPoint(Texture->GetSizeX(), Texture->GetSizeY());
	TargetSize = FVector(2.0f, 20.0f, 20.0f * static_cast<float>(TextureSize.Y) / static_cast<float>(TextureSize.X));
	Super::BeginPlay();

}

void UOptiXTargetComponent::UpdateOptiXComponent()
{
	if (OptiXGeometry != nullptr && OptiXTransform != nullptr && OptiXAcceleration != nullptr)
	{
		FMatrix T = GetComponentToWorld().ToMatrixNoScale();
		OptiXTransform->SetMatrix(T);
		//OptiXAcceleration->MarkDirty();
		OptiXContext->GetGroup("top_object")->GetAcceleration()->MarkDirty();
		FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();
	}

}

void UOptiXTargetComponent::InitOptiXGeometry()
{
	OptiXGeometry = OptiXContext->CreateGeometry();
	OptiXGeometry->SetPrimitiveCount(1u);
	UOptiXProgram* BB = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/box_intersect.ptx", "bounds");
	UOptiXProgram* IP = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/box_intersect.ptx", "intersect");

	OptiXGeometry->SetBoundingBoxProgram(BB);
	OptiXGeometry->SetIntersectionProgram(IP);
}

void UOptiXTargetComponent::InitOptiXMaterial()
{
	UOptiXProgram* CHPerspective = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/frame_material.ptx", "closest_hit_radiance");
	UOptiXProgram* CHIterative = OptiXContext->CreateProgramFromPTXFile(OptiXPTXDir + "generated/frame_material.ptx", "closest_hit_iterative");

	OptiXMaterial = OptiXContext->CreateMaterial();
	OptiXMaterial->SetClosestHitProgram(0, CHPerspective);
	OptiXMaterial->SetClosestHitProgram(1, CHIterative);

	/*OptiXMaterial->SetFloat("importance_cutoff", 1e-2f);
	OptiXMaterial->SetFloat3D("cutoff_color", 0.035f, 0.102f, 0.169f);
	OptiXMaterial->SetFloat("fresnel_exponent", 3.0f);
	OptiXMaterial->SetFloat("fresnel_minimum", 0.1f);
	OptiXMaterial->SetFloat("fresnel_maximum", 1.0f);
	OptiXMaterial->SetFloat("refraction_index", 1.4f);

	OptiXMaterial->SetFloat3D("refraction_color", 1.0f, 1.0f, 1.0f);
	OptiXMaterial->SetFloat3D("reflection_color", 1.0f, 1.0f, 1.0f);

	OptiXMaterial->SetInt("refraction_maxdepth", 10);
	OptiXMaterial->SetInt("reflection_maxdepth", 5);

	OptiXMaterial->SetFloat3D("extinction_constant", FMath::Loge(0.83f), FMath::Loge(0.83f), FMath::Loge(0.83f));*/
}

void UOptiXTargetComponent::InitOptiXGroups()
{
	OptiXGeometryInstance = OptiXContext->CreateGeometryInstance(OptiXGeometry, OptiXMaterial);
	
	SetSize(TargetSize);

	UOptiXTextureSampler* Sampler = OptiXContext->CreateTextureSampler();
	//Sampler->AddToRoot();
	Sampler->SetWrapMode(0, RT_WRAP_CLAMP_TO_EDGE);
	Sampler->SetWrapMode(1, RT_WRAP_CLAMP_TO_EDGE);
	Sampler->SetWrapMode(2, RT_WRAP_CLAMP_TO_EDGE);
	Sampler->SetIndexingMode(RT_TEXTURE_INDEX_NORMALIZED_COORDINATES);
	Sampler->SetReadMode(RT_TEXTURE_READ_NORMALIZED_FLOAT);
	Sampler->SetMaxAnisotropy(1.0f);
	Sampler->SetMipLevelCount(1u);
	Sampler->SetArraySize(1u);



	UE_LOG(LogTemp, Display, TEXT("Texture with Size: (%i, %i)"), TextureSize.X, TextureSize.Y);

	TextureBuffer = OptiXContext->CreateBufferUByte2D(RT_BUFFER_INPUT, TextureSize.X, TextureSize.Y);
	
	TextureSize = FIntPoint(Texture->GetSizeX(), Texture->GetSizeY());
	TargetSize = FVector(2.0f, 20.0f, 20.0f * static_cast<float>(TextureSize.Y) / static_cast<float>(TextureSize.X));

	optix::uchar4* BufferData = static_cast<optix::uchar4*>(TextureBuffer->MapNative());


	FTexture2DMipMap& Mip = Texture->PlatformData->Mips[0];

	FColor* TextureData = static_cast<FColor*>(Mip.BulkData.Lock(LOCK_READ_WRITE));

	// Texture index conversion is a real pain...
	for (int32 i = 0; i < TextureSize.X; ++i) {
		for (int32 j = 0; j < TextureSize.Y; ++j) {

			int32 TextureIndex = (TextureSize.X * TextureSize.Y - 1) - i * TextureSize.X - j;
			int32 BufferIndex = ((j)*(TextureSize.X) + i);
			//UE_LOG(LogTemp, Display, TEXT("Values: %i"), TextureData[BufferIndex]);

			BufferData[BufferIndex].x = TextureData[TextureIndex].R;
			BufferData[BufferIndex].y = TextureData[TextureIndex].G;
			BufferData[BufferIndex].z = TextureData[TextureIndex].B;
			BufferData[BufferIndex].w = TextureData[TextureIndex].A;

		}
	}

	// TODO: Check if we can copy automatically via Memcpy
	//FMemory::Memcpy(BufferData + (X * Y), TextureData, (X * Y * sizeof(FColor))); // Try copying the buffer data directly here TODO
	Mip.BulkData.Unlock();
	TextureBuffer->Unmap();
	

	Sampler->SetBufferWithTextureIndexAndMiplevel(0u, 0u, TextureBuffer);
	Sampler->SetFilteringModes(RT_FILTER_NEAREST, RT_FILTER_NEAREST, RT_FILTER_NONE);

	OptiXGeometryInstance->SetTextureSampler("frameTexture", Sampler);

	FMatrix T = GetComponentToWorld().ToMatrixNoScale();
	OptiXTransform = OptiXContext->CreateTransform();
	OptiXTransform->SetMatrix(T);

	OptiXGeometryGroup = OptiXContext->CreateGeometryGroup();
	OptiXGeometryGroup->AddChild(OptiXGeometryInstance);

	OptiXAcceleration = OptiXContext->CreateAcceleration("NoAccel"); // This should be faster
	//OptiXAcceleration->SetProperty("refit", "1");
	//OptiXAcceleration->MarkDirty();

	OptiXGeometryGroup->SetAcceleration(OptiXAcceleration);

	OptiXTransform->SetChild(OptiXGeometryGroup);

	OptiXContext->GetGroup("top_object")->AddChild(OptiXTransform);
	OptiXContext->GetGroup("top_object")->GetAcceleration()->MarkDirty();

}

void UOptiXTargetComponent::UpdateOptiXComponentVariables()
{
	check(IsInRenderingThread());


	// Size
	OptiXGeometryInstance->SetFloat3DVector("size", TargetSize);	
}


//// TODO: Do it this way for now, alternatively custom getters and setters might be better/less clutter-y?
//void UOptiXLensComponent::PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent)
//{
//
//	FName PropertyName = (PropertyChangedEvent.Property != NULL) ? PropertyChangedEvent.Property->GetFName() : NAME_None;
//
//	// Huge case distinction coming now, I don't really like this, seems like a lot of boilerplate code compared to setters/getters but w/e
//
//
//
//
//
//	Super::PostEditChangeProperty(PropertyChangedEvent);
//}

void UOptiXTargetComponent::CleanOptiXComponent()
{
	if (OptiXContext == NULL)
	{
		Super::CleanOptiXComponent();
		return;
	}

	OptiXContext->GetGroup("top_object")->RemoveChild(OptiXTransform);
	
	OptiXTransform = nullptr;
	OptiXAcceleration = nullptr;

	Super::CleanOptiXComponent();

	OptiXGeometryGroup = nullptr;
}

void UOptiXTargetComponent::SetSize(FVector NewSize)
{
	TargetSize = NewSize;
	QueueOptiXContextUpdate();
}

FVector UOptiXTargetComponent::GetSize()
{
	return TargetSize;
}

#include "OptiXGroup.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXModule.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

DEFINE_LOG_CATEGORY(OptiXPluginGroup);

// TODO LOOK INTO CONSTRUCTORS

void UOptiXGroup::BeginDestroy()
{
	// Tell optix to clean up
	UE_LOG(LogTemp, Warning, TEXT("OptiX Group BeginDestroy"));

	DestroyOptiXObject();

	Super::BeginDestroy();
}


void UOptiXGroup::DestroyOptiXObject()
{
	for (UObject* I : Children)
	{
		//RemoveChild(I);
	}

	if (NativeGroup != NULL)
	{
		//NativeGroup->destroy();
		FOptiXModule::Get().GetOptiXContextManager()->GroupsToDeleteQueue.Enqueue(NativeGroup);
	}

	Children.Empty();
	NativeGroup = NULL;
}

void UOptiXGroup::SetAcceleration(UOptiXAcceleration * Accel)
{
	try
	{
		NativeGroup->setAcceleration(Accel->GetNativeAcceleration());
		Acceleration = Accel;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGroup, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXAcceleration * UOptiXGroup::GetAcceleration()
{
	UOptiXAcceleration* Ptr = nullptr;
	try
	{
		optix::Acceleration Native = NativeGroup->getAcceleration();

		if (Native != Acceleration->GetNativeAcceleration())
		{
			FString Message = "Acceleration Mismatch in Group!";
			UE_LOG(OptiXPluginGroup, Error, TEXT("OptiX Error: %s"), *Message);
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
		}

		Ptr = Acceleration;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGroup, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Ptr;
}

void UOptiXGroup::SetChildCount(uint8 Count)
{
	NativeGroup->setChildCount(Count);
	ChildCount = Count;

	int32 Diff = Count - Children.Num();

	if (Diff > 0)
	{
		Children.SetNum(Count);
	}
	else if (Diff < 0)
	{
		// Theoretically remove children but don't do this for now! TODOOOO
	}
}

uint8 UOptiXGroup::GetChildCount()
{
	// Check if our count is the same
	uint8 OptiXCount = NativeGroup->getChildCount();
	if (OptiXCount != ChildCount)
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGroup Child Counts are mismatched! \n"));
	}
	return OptiXCount;
}

void UOptiXGroup::SetChild(uint8 Index, UObject * Child)
{
	// This will probably mess with child counts but shouldn't.

	if (!Children.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Trying to insert child in non-existing place! \n"));
		return;
	}

	// Check the different child types - don't do all for now just add the code when needed
	// Doing GeometryGroup, GeometryInstance and Transform for now

	if (UOptiXGeometryGroup* GeomGroup = Cast<UOptiXGeometryGroup>(Child))
	{
		NativeGroup->setChild(Index, GeomGroup->GetNativeGroup());
		Children.Insert(Child, Index);
	}
	else if (UOptiXGeometryInstance* GeomInstance = Cast<UOptiXGeometryInstance>(Child))
	{
		NativeGroup->setChild(Index, GeomInstance->GetNativeInstance());
		Children.Insert(Child, Index);
	}
	else if (UOptiXTransform* Transform = Cast<UOptiXTransform>(Child))
	{
		NativeGroup->setChild(Index, Transform->GetNativeTransform());
		Children.Insert(Child, Index);
	}
}

RTobjecttype UOptiXGroup::GetChildType(uint8 Index)
{
	RTobjecttype Type = RT_OBJECTTYPE_UNKNOWN;
	if (!Children.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Trying to get child type in non-existing place! \n"));
		return Type;
	}
	
	return NativeGroup->getChildType(Index);
}

UObject * UOptiXGroup::GetChild(uint8 Index)
{
	if (!Children.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Child does not exist! \n"));
	}

	return Children[Index];
}

uint8 UOptiXGroup::AddChild(UObject * Child)
{
	if (UOptiXGeometryGroup* GeomGroup = Cast<UOptiXGeometryGroup>(Child))
	{
		uint8 NativeIndex = static_cast<uint8>(NativeGroup->addChild(GeomGroup->GetNativeGroup()));
		uint8 Index = Children.Add(Child);
		if (NativeIndex != Index)
		{
			UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Index Mismatch while adding! \n"));
		}
		return Index;
	}
	else if (UOptiXGeometryInstance* GeomInstance = Cast<UOptiXGeometryInstance>(Child))
	{
		uint8 NativeIndex = static_cast<uint8>(NativeGroup->addChild(GeomInstance->GetNativeInstance()));
		uint8 Index = Children.Add(Child);
		if (NativeIndex != Index)
		{
			UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Index Mismatch while adding! \n"));
		}
		return Index;
	}
	else if (UOptiXTransform* Transform = Cast<UOptiXTransform>(Child))
	{
		uint8 NativeIndex = static_cast<uint8>(NativeGroup->addChild(Transform->GetNativeTransform()));
		uint8 Index = Children.Add(Child);
		if (NativeIndex != Index)
		{
			UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Index Mismatch while adding! \n"));
		}
		return Index;
	}
	else return 0; // Dangerous!	
}

uint8 UOptiXGroup::RemoveChild(UObject * Child)
{
	uint8 Index = GetChildIndex(Child);
	RemoveChildByIndex(Index);
	return Index;
}

void UOptiXGroup::RemoveChildByIndex(uint8 Index)
{
	if (!Children.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Index Mismatch while removing! \n"));
		return;
	}

	if (NativeGroup == NULL)
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGroup: Native group is NULL when removing child! \n"));
		return;
	}

	ChildCount--; // Not sure if OptiX internally resizes the array or if the child count stays the same?
	Children.RemoveAt(Index); // Will shuffle the remaining indices correctly - hopefully.

	// Can't do that in game thread!
	if (IsInGameThread())
	{
		FOptiXModule::Get().GetOptiXContextManager()->GroupChildrenToRemoveQueue.Enqueue(TPair<optix::Group, uint32>(NativeGroup, Index ));
	}
	else
	{
		NativeGroup->removeChild(static_cast<unsigned int>(Index));
	}
}

uint8 UOptiXGroup::GetChildIndex(UObject * Child)
{
	return Children.Find(Child);
}

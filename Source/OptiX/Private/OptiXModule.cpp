#include "OptiXModule.h"
#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"

#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Json.h"
#include "JsonObjectConverter.h"
#include "Misc/FileHelper.h"


#include "OptiXContextManager.h"

#define LOCTEXT_NAMESPACE "FOptixModule"

IMPLEMENT_MODULE(FOptiXModule, OptiX)

DEFINE_LOG_CATEGORY(OptiXPlugin);

FOptiXModule* FOptiXModule::Singleton = NULL;

// Similar to https://github.com/EpicGames/UnrealEngine/blob/76085d1106078d8988e4404391428252ba1eb9a7/Engine/Source/Runtime/Online/HTTP/Private/HttpModule.cpp
// Global singleton getter
FOptiXModule& FOptiXModule::Get()
{
	if (Singleton == NULL) 
	{
		check(IsInGameThread());
		FModuleManager::LoadModuleChecked<FOptiXModule>("OptiX");
	}
	check(Singleton != NULL);
	return *Singleton;
}

// Returns a pointer to the scene view extension / context manager - accessible anywhere. Not the best design, 
// but seems to be fairly common in Unreal. 
FOptiXContextManager * FOptiXModule::GetOptiXContextManager()
{
	if (!OptiXContextManager.IsValid())
	{
		// If it doesn't exist yet, register it. This is the correct (and probably only) way to
		// register a FSceneViewExtension, and sets up the callbacks etc.
		OptiXContextManager = FSceneViewExtensions::NewExtension<FOptiXContextManager>();
	}
	return OptiXContextManager.Get();
}

// Start up the plugin module
void FOptiXModule::StartupModule()
{
	// Load optix and cuda dlls.
	LoadDLLs();
	Singleton = this;

	LoadGlassDefinitions();
	LoadSceneData();

	// Set optix compiled shader path
	OptiXPTXDir = FPaths::ProjectContentDir() / TEXT("ptx/");
}

void FOptiXModule::ShutdownModule()
{
	UnloadDLLs();
	UE_LOG(LogTemp, Warning, TEXT("Shutting down OptiX Module"));

	OptiXContextManager.Reset();
	Singleton = nullptr;
}

FString FOptiXModule::GetDir()
{
	auto Plugin = IPluginManager::Get().FindPlugin(TEXT("OptiX"));
	check(Plugin.IsValid());
	return Plugin->GetBaseDir();
}

void FOptiXModule::LoadDLLs()
{
	// Load DLLs

	{
		FString OptiXBinariesDir = GetDir() / TEXT("Binaries/ThirdParty");

		// https://github.com/NvPhysX/UnrealEngine/blob/FleX-4.19.2/Engine/Plugins/GameWorks/Flex/Source/Flex/Private/FlexModule.cpp
		auto LoadDLL([](const FString& Path) -> void*
		{
			void* Handle = FPlatformProcess::GetDllHandle(*Path);
			if (Handle == nullptr)
			{
				UE_LOG(LogTemp, Fatal, TEXT("Failed to load module '%s'."), *Path);
			}
			return Handle;
		});


		// x64 is currently the only supported platform
		FString OptiXBinariesPath = OptiXBinariesDir / TEXT("Win64/");

		CudaRtHandle = LoadDLL(*(OptiXBinariesPath + "cudart64_101.dll"));
		OptixHandle = LoadDLL(*(OptiXBinariesPath + "optix.6.5.0.dll"));
		OptixUHandle = LoadDLL(*(OptiXBinariesPath + "optixu.6.5.0.dll"));

		//
		//CudaRtHandle = LoadDLL(*(OptiXBinariesPath + "cudart64_90.dll"));
		//OptixHandle = LoadDLL(*(OptiXBinariesPath + "optix.51.dll"));
		//OptixUHandle = LoadDLL(*(OptiXBinariesPath + "optixu.1.dll"));
		
	}
}

void FOptiXModule::UnloadDLLs()
{
	FPlatformProcess::FreeDllHandle(CudaRtHandle);
	FPlatformProcess::FreeDllHandle(OptixHandle);
	FPlatformProcess::FreeDllHandle(OptixUHandle);
}

void FOptiXModule::LoadGlassDefinitions()
{
	FString JsonString;
	//FString Path = IPluginManager::Get().FindPlugin(TEXT("OptiX"))->GetContentDir().Append("/GlassDefinitions/GlassDefinitions.json");
	FString Path = FPaths::ProjectContentDir() / TEXT("GlassDefinitions/GlassDefinitions.json");
	FFileHelper::LoadFileToString(JsonString, *Path);

	TArray<FRawGlassDefinition> Array;

	FJsonObjectConverter::JsonArrayStringToUStruct<FRawGlassDefinition>(JsonString, &Array, 0, 0);

	for (const FRawGlassDefinition& Def : Array)
	{
		GlassDefinitionsMap.Emplace(Def.Name, FGlassDefinition(FVector(Def.B1, Def.B2, Def.B3), FVector(Def.C1, Def.C2, Def.C3)));
	}
}
 
void FOptiXModule::LoadSceneData()
{
	FString SceneDataDirectory = FPaths::ProjectContentDir() / TEXT("SceneData");

	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
	if (!PlatformFile.DirectoryExists(*SceneDataDirectory))
	{
		UE_LOG(LogTemp, Error, TEXT("Scene Data Directory does not exist! '%s'."), *SceneDataDirectory);
		return;
	}

	TArray<FString> FoundFiles;



	FSceneData EmptyScene;


	EmptyScene.SceneName = "EmptyScene";
	EmptyScene.LaserPosition = 0;
	EmptyScene.Wavelength = 500;

	SceneDataArray.Push(EmptyScene);

	PlatformFile.FindFiles(FoundFiles, *SceneDataDirectory, TEXT(".zmx"));

	for (const FString& SceneFile : FoundFiles)
	{
		UE_LOG(LogTemp, Display, TEXT("Found scene file: '%s'."), *SceneFile);

		FSceneData SceneData;
		

		SceneData.SceneName = SceneFile;
		SceneData.SceneName.RemoveFromEnd(".zmx");
		SceneData.SceneName.RemoveFromStart(SceneDataDirectory);
		SceneData.SceneName.RemoveFromStart("/");

		TArray<FString> Lines;
		FFileHelper::LoadFileToStringArray(Lines, *SceneFile);

		float AccDist = NAN;
		float Curvature = NAN;
		float Pos = NAN;
		float Diameter = NAN;

		float Curvature2 = NAN;
		float Thickness = NAN;
		float Diameter2 = NAN;

		bool bFirstSide = true;

		FString GlassType = "";

		for (const FString& Line : Lines)
		{
			TArray<FString> Arguments;

			Line.ParseIntoArrayWS(Arguments); // Apparently O(n^2)
			if (Arguments.Num() == 0)
				continue;

			if (Arguments[0] == "WAVM")
			{
				if(Arguments.IsValidIndex(2))
					SceneData.Wavelength = FCString::Atof(*Arguments[2]) *1000.0f;
				else
					UE_LOG(LogTemp, Warning, TEXT("Could not parse wavelength, using default of 500.0. Line is: '%s'."), *Line);
			}
			else if (Arguments[0] == "SURF")
			{
				if (bFirstSide) // Gathering values for side 1 of the lense
				{
					// Check if we have gathered all values
					if (!isnan(Curvature) && !isnan(Diameter) && GlassType.Len() > 0)
						bFirstSide = false; // Start gathering data for second side
					else // reset, not a lens
					{
						bFirstSide = true;
						GlassType = "";
						Curvature = NAN;
						Curvature2 = NAN;
						Diameter = NAN;
						Diameter2 = NAN;
						Thickness = NAN;
						Pos = NAN;
					}
				}
				else // first side is completed, check if second side is valid
				{
					if (!isnan(Curvature2) && !isnan(Diameter2) && !isnan(Thickness) && GlassType.Len() > 0)
					{
						FLensData LensData;
						LensData.GlassType = GlassType;
						LensData.Position = Pos;
						LensData.LensRadius = Diameter / 2.0;
						LensData.Radius1 = 1.0f / FMath::Abs(Curvature);
						LensData.Radius2 = 1.0f / FMath::Abs(Curvature2);
						if (Curvature == 0)
							LensData.LensTypeSide1 = ELensSideType::PLANE;
						else if (Curvature < 0)
							LensData.LensTypeSide1 = ELensSideType::CONVEX;
						else
							LensData.LensTypeSide1 = ELensSideType::CONCAVE;

						if (Curvature2 == 0)
							LensData.LensTypeSide2 = ELensSideType::PLANE;
						else if (Curvature2 < 0)
							LensData.LensTypeSide2 = ELensSideType::CONVEX;
						else
							LensData.LensTypeSide2 = ELensSideType::CONCAVE;

						LensData.Thickness = Thickness;
						SceneData.LensData.Push(LensData);

						bFirstSide = true;
						GlassType = "";
						Curvature = NAN;
						Curvature2 = NAN;
						Diameter = NAN;
						Diameter2 = NAN;
						Thickness = NAN;
						Pos = NAN;

					}
				}
			}
			else if (Arguments[0] == "CURV")
			{
				if (Arguments.IsValidIndex(1))
				{
					if (bFirstSide)
						Curvature = FCString::Atof(*Arguments[1]);
					else 
						Curvature2 = FCString::Atof(*Arguments[1]);
				}
				else
					UE_LOG(LogTemp, Warning, TEXT("Could not parse curvature, Line is: '%s'."), *Line);
			}
			else if (Arguments[0] == "DISZ")
			{
				if (Arguments.IsValidIndex(1))
				{
					if (bFirstSide)
					{
						Thickness = FCString::Atof(*Arguments[1]) / 10.0f;	// in cm
						Pos = isnan(AccDist) ? 0 : AccDist;
					}
					if(isnan(AccDist))
						AccDist = FCString::Atof(*Arguments[1]) / 10.0f;	// in cm
					else
						AccDist += FCString::Atof(*Arguments[1]) / 10.0f;	// in cm
				}
				else
					UE_LOG(LogTemp, Warning, TEXT("Could not parse disz, Line is: '%s'."), *Line);
			}
			else if (Arguments[0] == "DIAM")
			{
				if (Arguments.IsValidIndex(1))
				{
					if (bFirstSide)
						Diameter = FCString::Atof(*Arguments[1]) / 10.0f; // in cm
					else
						Diameter2 = FCString::Atof(*Arguments[1]) / 10.0f;	// in cm
				}
				else
					UE_LOG(LogTemp, Warning, TEXT("Could not parse diameter, Line is: '%s'."), *Line);
			}
			else if (Arguments[0] == "GLAS")
			{
				if (Arguments.IsValidIndex(1))
				{
					GlassType = Arguments[1];
				}
				else
					UE_LOG(LogTemp, Warning, TEXT("Could not parse glass, Line is: '%s'."), *Line);
			}

		}

		SceneData.LaserPosition = isnan(Pos) ? Pos : 0.0f;

		SceneDataArray.Push(SceneData);

	}


	// Debug:

	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("ELensSideType"), true);

	for (const FSceneData& Scene: SceneDataArray)
	{
		UE_LOG(LogTemp, Display, TEXT("Imported Scene with Name: '%s'."), *Scene.SceneName);
		UE_LOG(LogTemp, Display, TEXT("  Wavelength: %f"), Scene.Wavelength);
		UE_LOG(LogTemp, Display, TEXT("  Laser Position: %f"), Scene.LaserPosition);
		UE_LOG(LogTemp, Display, TEXT("  Lens Number: %i"), Scene.LensData.Num());
		UE_LOG(LogTemp, Display, TEXT("  Lens Data: \n---------------------------------------------------"));
		for (const FLensData& Lens : Scene.LensData)
		{
			FString Type1 = (EnumPtr == nullptr) ? FString::FromInt(static_cast<uint8>(Lens.LensTypeSide1)) : EnumPtr->GetNameStringByValue(static_cast<int64>(Lens.LensTypeSide1));
			FString Type2 = (EnumPtr == nullptr) ? FString::FromInt(static_cast<uint8>(Lens.LensTypeSide2)) : EnumPtr->GetNameStringByValue(static_cast<int64>(Lens.LensTypeSide1));

			UE_LOG(LogTemp, Display, TEXT("    Lens Position: %f"), Lens.Position);
			UE_LOG(LogTemp, Display, TEXT("    Lens Radius: %f"), Lens.LensRadius);
			UE_LOG(LogTemp, Display, TEXT("    Lens Radius Side 1: %f"), Lens.Radius1);
			UE_LOG(LogTemp, Display, TEXT("    Lens Radius Side 2: %f"), Lens.Radius2);
			UE_LOG(LogTemp, Display, TEXT("    Lens Type Side 1: %s"), *Type1);
			UE_LOG(LogTemp, Display, TEXT("    Lens Type Side 2: %s"), *Type2);
			UE_LOG(LogTemp, Display, TEXT("    Lens Thickness: %f"), Lens.Thickness);
			UE_LOG(LogTemp, Display, TEXT("    Lens Glass: %s"), *Lens.GlassType);
			UE_LOG(LogTemp, Display, TEXT("    ---------------------------------------------------"));
		}
	}
}

// Init the context manager. This is called from the game thread in initgame, NOT on module startup.
void FOptiXModule::Init()
{
	GetOptiXContextManager()->Init();
}

#undef LOCTEXT_NAMESPACE
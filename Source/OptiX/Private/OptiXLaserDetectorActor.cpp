// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLaserDetectorActor.h"


#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "OptiXModule.h"
#include "OptiXLaserActor.h"

AOptiXLaserDetectorActor::AOptiXLaserDetectorActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Detector Actor Constructor"));

	PrimaryActorTick.bCanEverTick = true;
	bIsEnabled = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/OptiX/Targets/target.target'"));
	UStaticMesh* Asset = MeshAsset.Object;

	GetStaticMeshComponent()->SetStaticMesh(Asset);
	//SetActorEnableCollision(false);

	OptiXLaserTargetComponent = CreateDefaultSubobject<UOptiXLaserTargetComponent>(TEXT("LaserTargetComponent"));
	OptiXLaserTargetComponent->SetupAttachment(GetStaticMeshComponent());

	HighlightColor = FColor(255, 255, 255);

}

void AOptiXLaserDetectorActor::BeginPlay()
{
	Super::BeginPlay();
	Init();
}

void AOptiXLaserDetectorActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	// Might need to remove the delegate thingy here?
}

void AOptiXLaserDetectorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	OnLaserTraceFinished();
}

void AOptiXLaserDetectorActor::Init()
{
	// Subscribe to the laser actor:
	TArray<AActor*> FoundLaserActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AOptiXLaserActor::StaticClass(), FoundLaserActors);
	
	//FOptiXModule::Get().GetOptiXContextManager()->LaserTraceFinishedEvent.AddUFunction(this, "OnLaserTraceFinished");

	// Setup DIM

	DynamicScreenMaterial = UMaterialInstanceDynamic::Create(GetStaticMeshComponent()->GetMaterial(1), this);
	GetStaticMeshComponent()->SetMaterial(1, DynamicScreenMaterial);

	// Set up the texture

	int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);

	DetectorResultTexture = UTexture2D::CreateTransient(Size, Size, PF_R32_FLOAT);
	//OutputTexture->AddToRoot();
	//// Allocate the texture HRI
	DetectorResultTexture->UpdateResource();


	TextureRegion = MakeUnique<FUpdateTextureRegion2D>();
	TextureRegion->Height = Size;
	TextureRegion->Width = Size;
	TextureRegion->SrcX = 0;
	TextureRegion->SrcY = 0;
	TextureRegion->DestX = 0;
	TextureRegion->DestY = 0;

	FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();

}

void AOptiXLaserDetectorActor::OnLaserTraceFinished()
{
	//UE_LOG(LogTemp, Warning, TEXT("lasertracefinished"));

	if (bIsEnabled)
	{
		//OptiXLaserTargetComponent->UpdateBufferData();
		RenderDataToTarget();
	}
}

void AOptiXLaserDetectorActor::RenderDataToTarget()
{		

	// Let's try something else:

	if (OptiXLaserTargetComponent->OptiXContext == nullptr)
	{
		return;
	}

	int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);

	Max = OptiXLaserTargetComponent->GetMaxFromBuffer();

	float* Data = static_cast<float*>(OptiXLaserTargetComponent->TargetBuffer->MapNative(0, RT_BUFFER_MAP_READ));
	DetectorResultTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)Data);
	OptiXLaserTargetComponent->TargetBuffer->Unmap();

	if (DynamicScreenMaterial != nullptr)
	{
		DynamicScreenMaterial->SetScalarParameterValue("ColorMode", bIsColorMode ? 1.0f : 0.0f);
		DynamicScreenMaterial->SetVectorParameterValue("ColorMode", HighlightColor);
		DynamicScreenMaterial->SetTextureParameterValue("ResultTexture", DetectorResultTexture);
		DynamicScreenMaterial->SetScalarParameterValue("ResultMax", Max);
	}

	//int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);
	//DetectorResultTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)OptiXLaserTargetComponent->ColorData.GetData());

	//if (DynamicScreenMaterial != nullptr)
	//{
	//	DynamicScreenMaterial->SetTextureParameterValue("Texture", DetectorResultTexture);
	//}

}

void AOptiXLaserDetectorActor::Clear()
{
	uint32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);

	TArray<FColor> Black;
	Black.AddZeroed(Size * Size);

	DetectorResultTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)Black.GetData());

	if (DynamicScreenMaterial != nullptr)
	{
		DynamicScreenMaterial->SetTextureParameterValue("ResultTexture", DetectorResultTexture);
		DynamicScreenMaterial->SetScalarParameterValue("ResultMax", 0);
	}

	float* Data = static_cast<float*>(OptiXLaserTargetComponent->TargetBuffer->MapNative(0, RT_BUFFER_MAP_WRITE));
	FMemory::Memset(Data, 0u, Size * Size * 4);
	OptiXLaserTargetComponent->TargetBuffer->Unmap();

}

void AOptiXLaserDetectorActor::Save()
{
	// Hope this makes a copy - it doesn't
	UTexture2D* NewTexture = UTexture2D::CreateTransient(DetectorResultTexture->GetSizeX(), DetectorResultTexture->GetSizeY(), DetectorResultTexture->GetPixelFormat());
	NewTexture->UpdateResource();
	
	int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);
	//Max = OptiXLaserTargetComponent->GetMaxFromBuffer();

	float* Data = static_cast<float*>(OptiXLaserTargetComponent->TargetBuffer->MapNative(0, RT_BUFFER_MAP_READ));
	NewTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)Data);
	OptiXLaserTargetComponent->TargetBuffer->Unmap();

	SavedDetectorTextures.Add(NewTexture);
}

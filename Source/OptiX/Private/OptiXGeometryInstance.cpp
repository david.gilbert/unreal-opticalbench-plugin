#include "OptiXGeometryInstance.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXModule.h"

#include "OptiXGeometryInstance.h"

// TODO LOOK INTO CONSTRUCTORS

void UOptiXGeometryInstance::BeginDestroy()
{
	// Tell optix to clean up
	UE_LOG(LogTemp, Warning, TEXT("OptiX Geometry Instance BeginDestroy"));
	// This should also remove the optix::geometry if nowhere referenced
	DestroyOptiXObject();
	Super::BeginDestroy();
}

void UOptiXGeometryInstance::DestroyOptiXObject()
{
	if (NativeGeometryInstance != NULL)
	{
		//NativeGeometryInstance->destroy();
		FOptiXModule::Get().GetOptiXContextManager()->GeometryInstancesToDeleteQueue.Enqueue(NativeGeometryInstance);
	}

	BufferMap.Empty();
	OptiXMaterials.Empty();
	TextureSamplerMap.Empty();
	NativeGeometryInstance = NULL;
}

void UOptiXGeometryInstance::SetGeometry(UOptiXGeometry * OptiXGeometryPtr)
{
	OptiXGeometry = OptiXGeometryPtr;
	NativeGeometryInstance->setGeometry(OptiXGeometryPtr->GetNativeGeometry()); // I hate passing the native geometry around here
}

UOptiXGeometry * UOptiXGeometryInstance::GetGeometry()
{
	return OptiXGeometry;
}

void UOptiXGeometryInstance::SetMaterialCount(uint8 Count)
{
	NativeGeometryInstance->setMaterialCount(Count);
}

uint8 UOptiXGeometryInstance::GetMaterialCount()
{
	return static_cast<uint8>(NativeGeometryInstance->getMaterialCount());
}

void UOptiXGeometryInstance::SetMaterial(uint8 Idx, UOptiXMaterial * Material)
{
	if (!OptiXMaterials.IsValidIndex(Idx))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGeometryInstance: Trying to insert Material in non-existing place! \n"));
		return;
	}
	NativeGeometryInstance->setMaterial(Idx, Material->GetNativeMaterial());
	OptiXMaterials.Insert(Material, Idx);
}

UOptiXMaterial * UOptiXGeometryInstance::GetMaterial(uint8 Idx)
{
	if (!OptiXMaterials.IsValidIndex(Idx))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXGeometryInstance: Trying to get Material in non-existing place! \n"));
		return nullptr;
	}
	return OptiXMaterials[Idx];
}

uint8 UOptiXGeometryInstance::AddMaterial(UOptiXMaterial * OptiXMaterialPtr)
{
	NativeGeometryInstance->addMaterial(OptiXMaterialPtr->GetNativeMaterial());
	return OptiXMaterials.Add(OptiXMaterialPtr);
}


// Setters - ugh

void UOptiXGeometryInstance::SetFloat(FString string, float Var)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var);
}

void UOptiXGeometryInstance::SetFloat2D(FString string, float Var1, float Var2)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2);

}

void UOptiXGeometryInstance::SetFloat3DVector(FString string, FVector Var)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z);

}

void UOptiXGeometryInstance::SetFloat3D(FString string, float Var1, float Var2, float Var3)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3);

}

void UOptiXGeometryInstance::SetFloat4DVector(FString string, FVector4 Var)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z, Var.W);

}

void UOptiXGeometryInstance::SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3, Var4);

}

void UOptiXGeometryInstance::SetInt(FString string, int32 Var)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var);

}

void UOptiXGeometryInstance::SetInt2D(FString string, int32 Var1, int32 Var2)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2);

}

void UOptiXGeometryInstance::SetInt3DVector(FString string, FIntVector Var)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var.X, Var.Y, Var.Z);

}

void UOptiXGeometryInstance::SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3)
{
	NativeGeometryInstance[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2, Var3);

}

void UOptiXGeometryInstance::SetTextureSampler(FString Name, UOptiXTextureSampler * Sampler)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		NativeGeometryInstance[N]->set(Sampler->GetNativeTextureSampler());
		TextureSamplerMap.Add(Name, Sampler);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(LogTemp, Error, TEXT("OptiX Error: %s"), *Message);
	}
}

void UOptiXGeometryInstance::SetBuffer(FString Name, UOptiXBuffer* Buffer)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		NativeGeometryInstance[N]->setBuffer(Buffer->GetNativeBuffer());
		BufferMap.Add(Name, Buffer);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(LogTemp, Error, TEXT("OptiX Error: %s"), *Message);
	}
}

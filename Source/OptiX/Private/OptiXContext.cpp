#include "OptiXContext.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

DEFINE_LOG_CATEGORY(OptiXPluginContext);


UOptiXContext::UOptiXContext(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

optix::Context UOptiXContext::Init()
{
	UOptiXGeometryGroup* Group = NewObject<UOptiXGeometryGroup>(this, UOptiXGeometryGroup::StaticClass());
	try
	{
		NativeContext = optix::Context::create();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Fatal, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	UE_LOG(LogTemp, Warning, TEXT("OptiX Context Init"));
	return NativeContext;
}

void UOptiXContext::UpdateVariables()
{
	check(IsInRenderingThread());

	for (UOptiXTransform* T : TransformMap)
	{
		T->UpdateTransform();
	}

	for (TPair<FString, FVector>& V : VectorCache)
	{
		SetFloat3DVector(V.Key, V.Value);
	}
	VectorCache.Empty();
	for (TPair<FString, FMatrix>& M : MatrixCache)
	{
		SetMatrix(M.Key, M.Value);
	}
	VectorCache.Empty();
}

void UOptiXContext::BeginDestroy()
{
	// Tell optix to clean up
	UE_LOG(LogTemp, Warning, TEXT("OptiX Context BeginDestroy"));
	
	Reset();

	if (NativeContext != NULL)
	{
		// To make sure the context will be destroyed LAST, after the UObject is gone, the Context Manager needs to make sure 
		// it actually saves a ptr to the optix::Context and destroys that manually!
		//NativeContext->destroy();

	}
	Super::BeginDestroy();

}

/*
Creator functions. Those functions are used to create the wrapper object and the respective wrapped 
optix:: object. The UObjects will *not* be saved, so the caller of those functions is responsible
to keep them from getting eaten by the garbage collection. They can be registered by using the 
SetObject functions below, which will keep them referenced in the context instance.
*/

void UOptiXContext::Reset()
{
	BufferMap.Empty();
	MaterialMap.Empty();
	GeometryMap.Empty();
	GeometryInstanceMap.Empty();
	GeometryGroupMap.Empty();
	GroupMap.Empty();
	TextureSamplerMap.Empty();
	RayGenerationPrograms.Empty();
	ExceptionPrograms.Empty();
	MissPrograms.Empty();
	TransformMap.Empty();
}

UOptiXGeometry* UOptiXContext::CreateGeometry()
{
	// Create the object - maybe move outer to the caller of CreateGeometry so the actor actually owns the Geometry? TODO
	UOptiXGeometry* Geometry = NewObject<UOptiXGeometry>(this, UOptiXGeometry::StaticClass());
	Geometry->SetGeometry(NativeContext->createGeometry());
	return Geometry;
}

UOptiXMaterial * UOptiXContext::CreateMaterial()
{
	// Create the object - maybe move outer to the caller of CreateGeometry so the actor actually owns the Geometry? TODO
	UOptiXMaterial* Material = NewObject<UOptiXMaterial>(this, UOptiXMaterial::StaticClass());
	Material->SetMaterial(NativeContext->createMaterial());
	return Material;
}

UOptiXGeometryInstance * UOptiXContext::CreateGeometryInstance(UOptiXGeometry* Geometry, TArray<UOptiXMaterial*> Materials)
{
	// Create the object - maybe move outer to the caller of CreateGeometry so the actor actually owns the Geometry? TODO
	UOptiXGeometryInstance* Instance = NewObject<UOptiXGeometryInstance>(this, UOptiXGeometryInstance::StaticClass());
	optix::GeometryInstance NativeInstance = NativeContext->createGeometryInstance();
	Instance->SetNativeInstance(NativeInstance);
	Instance->SetGeometry(Geometry);
	for (UOptiXMaterial* Mat : Materials)
	{
		Instance->AddMaterial(Mat);
	}
	return Instance;
}

UOptiXGeometryInstance * UOptiXContext::CreateGeometryInstance(UOptiXGeometry* Geometry, UOptiXMaterial* Material)
{
	// Create the object - maybe move outer to the caller of CreateGeometry so the actor actually owns the Geometry? TODO
	UOptiXGeometryInstance* Instance = NewObject<UOptiXGeometryInstance>(this, UOptiXGeometryInstance::StaticClass());
	optix::GeometryInstance NativeInstance = NativeContext->createGeometryInstance();
	Instance->SetNativeInstance(NativeInstance);
	Instance->SetGeometry(Geometry);

	Instance->AddMaterial(Material);
	return Instance;
}

UOptiXGeometryGroup * UOptiXContext::CreateGeometryGroup()
{
	UOptiXGeometryGroup* Group = NewObject<UOptiXGeometryGroup>(this, UOptiXGeometryGroup::StaticClass());
	try
	{
		Group->SetNativeGroup(NativeContext->createGeometryGroup());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Fatal, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}	
	return Group;
}

UOptiXGroup * UOptiXContext::CreateGroup()
{
	UOptiXGroup* Group = NewObject<UOptiXGroup>(this, UOptiXGroup::StaticClass());
	try
	{
		Group->SetNativeGroup(NativeContext->createGroup());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Fatal, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Group;
}

UOptiXTransform * UOptiXContext::CreateTransform()
{
	UOptiXTransform* Transform = NewObject<UOptiXTransform>(this, UOptiXTransform::StaticClass());
	Transform->SetNativeGroup(NativeContext->createTransform());
	TransformMap.Add(Transform); // TODO Fix me 
	return Transform;
}

UOptiXAcceleration * UOptiXContext::CreateAcceleration(FString Builder)
{
	std::string B = std::string(TCHAR_TO_ANSI(*Builder));

	UOptiXAcceleration* Accel = NewObject<UOptiXAcceleration>(this, UOptiXAcceleration::StaticClass());
	Accel->SetNativeAcceleration(NativeContext->createAcceleration(B));
	return Accel;
}

/*
Various setter and getter methods to manipulate the current context and pass variables to the shaders.
*/

void UOptiXContext::SetFloat(FString string, float Var)
{
	try 
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var);
	} 
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetFloat2D(FString string, float Var1, float Var2)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}

}

void UOptiXContext::SetFloat3DVector(FString string, FVector Var)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetFloat3D(FString string, float Var1, float Var2, float Var3)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}

}

void UOptiXContext::SetFloat4DVector(FString string, FVector4 Var)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z, Var.W);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3, Var4);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetInt(FString string, int32 Var)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetInt2D(FString string, int32 Var1, int32 Var2)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetInt3DVector(FString string, FIntVector Var)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var.X, Var.Y, Var.Z);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2, Var3);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

//void UOptiXContext::SetInt4DVector(FString string, FIntVector4 Var)
//{
//	NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var.X, Var.Y, Var.Z, Var.W);
//
//}

void UOptiXContext::SetInt4D(FString string, int32 Var1, int32 Var2, int32 Var3, int32 Var4)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2, Var3, Var4);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetUint(FString string, uint8 Var)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetUint2D(FString string, uint8 Var1, uint8 Var2)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetUint3D(FString string, uint8 Var1, uint8 Var2, uint8 Var3)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2, Var3);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetUint4D(FString string, uint8 Var1, uint8 Var2, uint8 Var3, uint8 Var4)
{
	try
	{
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2, Var3, Var4);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetMatrix(FString string, FMatrix Matrix)
{
	try
	{
		// According to the optix doc false == row major
		NativeContext[std::string(TCHAR_TO_ANSI(*string))]->setMatrix4x4fv(true, &Matrix.M[0][0]); // TODO - find out if false or true is column or row major
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

float UOptiXContext::GetFloat(FString string)
{
	float F = 0;
	try
	{
		F = NativeContext[std::string(TCHAR_TO_ANSI(*string))]->getFloat();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return F;
}

// TODO: COMPLETE ERROR HANDLING

FVector2D UOptiXContext::GetFloat2D(FString string)
{
	optix::float2 V = optix::make_float2(0, 0);
	try
	{
		V = NativeContext[std::string(TCHAR_TO_ANSI(*string))]->getFloat2();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}

	return FVector2D(V.x, V.y);
}

FVector UOptiXContext::GetFloat3D(FString string)
{
	optix::float3 V = optix::make_float3(0, 0, 0);;
	try
	{
		V = NativeContext[std::string(TCHAR_TO_ANSI(*string))]->getFloat3();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return FVector(V.x, V.y, V.z);
}

FVector4 UOptiXContext::GetFloat4D(FString string)
{
	optix::float4 V = optix::make_float4(0, 0, 0, 0);
	try
	{
		V = NativeContext[std::string(TCHAR_TO_ANSI(*string))]->getFloat4();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return FVector4(V.x, V.y, V.z, V.w);
}

int32 UOptiXContext::GetInt(FString string)
{
	int32 Result = 0;
	try
	{
		Result = NativeContext[std::string(TCHAR_TO_ANSI(*string))]->getInt();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Result;
}

FIntPoint UOptiXContext::GetInt2D(FString string)
{
	optix::int2 V = optix::make_int2(0, 0);
	try
	{
		V = NativeContext[std::string(TCHAR_TO_ANSI(*string))]->getInt2();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return FIntPoint(V.x, V.y);
}

FIntVector UOptiXContext::GetInt3D(FString string)
{
	optix::int3 V = optix::make_int3(0, 0, 0);
	try
	{
		V = NativeContext[std::string(TCHAR_TO_ANSI(*string))]->getInt3();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return FIntVector(V.x, V.y, V.z);
}


void UOptiXContext::Validate()
{
	try
	{
		NativeContext->validate();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

uint32 UOptiXContext::GetDeviceCount()
{
	uint32 DeviceCount = 0;
	try
	{
		DeviceCount = NativeContext->getDeviceCount();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return DeviceCount;
}

FString UOptiXContext::GetDeviceName(int32 Ordinal)
{
	FString DeviceName = "";
	try
	{
		DeviceName = FString(NativeContext->getDeviceName(Ordinal).c_str());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return DeviceName;
}

void UOptiXContext::SetStackSize(int32 StackSize)
{
	if (StackSize < 0)
	{
		FString Message = "Trying to set negative stack size!";
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		return; // TODO Throw error
	}
	try
	{
		NativeContext->setStackSize(static_cast<uint32>(StackSize));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

int32 UOptiXContext::GetStackSize()
{
	// Casting a uint32 into an int32 is a bit dangerous here...
	// TODO Check precision
	int32 Size = 0;
	try
	{
		Size = static_cast<int32>(NativeContext->getStackSize());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Size;
}

void UOptiXContext::SetStackSize64(uint64 StackSize)
{
	try
	{
		NativeContext->setStackSize(StackSize);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

uint64 UOptiXContext::GetStackSize64()
{
	uint64 Size = 0;
	try
	{
		Size = NativeContext->getStackSize();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Size;
}

void UOptiXContext::SetEntryPointCount(int32 NumEntryPoints)
{
	int32 Diff = NumEntryPoints - GetEntryPointCount();
	if (Diff > 0)
	{
		RayGenerationPrograms.SetNum(NumEntryPoints);
		ExceptionPrograms.SetNum(NumEntryPoints);
	}
	// TODO delete if diff < 0
	else
	{
		for (int32 i = NumEntryPoints; i < GetEntryPointCount(); i++)
		{
			RayGenerationPrograms.RemoveAt(i);
			ExceptionPrograms.RemoveAt(i);
		}
		RayGenerationPrograms.SetNum(NumEntryPoints);
		ExceptionPrograms.SetNum(NumEntryPoints);

		FString Message = FString("Shrinking Entry Point Count!");
		UE_LOG(OptiXPluginContext, Warning, TEXT("Error: %s"), *Message);
	}
	try
	{
		NativeContext->setEntryPointCount(static_cast<uint32>(NumEntryPoints));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

int32 UOptiXContext::GetEntryPointCount()
{
	int32 EntryPointCount = 0;
	try
	{
		EntryPointCount = static_cast<int32>(NativeContext->getEntryPointCount());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return EntryPointCount;
}


void UOptiXContext::SetRayGenerationProgram(int32 EntryPointIndex, UOptiXProgram* Program)
{
	if (!RayGenerationPrograms.IsValidIndex(EntryPointIndex))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXContext: Trying to insert program in non-existing place! \n"));
		return;
	}
	try
	{
		NativeContext->setRayGenerationProgram(EntryPointIndex, Program->GetNativeProgram());
		RayGenerationPrograms.Insert(Program, EntryPointIndex);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}

}

UOptiXProgram* UOptiXContext::GetRayGenerationProgram(int32 EntryPointIndex)
{
	UOptiXProgram* Program = nullptr;
	if (!RayGenerationPrograms.IsValidIndex(EntryPointIndex))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXContext: Trying to get ray generation program from non-existing place! \n"));
	}
	try
	{
		NativeContext->getRayGenerationProgram(EntryPointIndex);
		Program = RayGenerationPrograms[EntryPointIndex];
		// Todo: this could use some sweet little love
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Program;
}

void UOptiXContext::SetExceptionProgram(int32 EntryPointIndex, UOptiXProgram* Program)
{
	if (!ExceptionPrograms.IsValidIndex(EntryPointIndex))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXContext: Trying to insert program in non-existing place! \n"));
		return;
	}
	try
	{
		NativeContext->setExceptionProgram(EntryPointIndex, Program->GetNativeProgram());
		ExceptionPrograms.Insert(Program, EntryPointIndex);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}

}

UOptiXProgram* UOptiXContext::GetExceptionProgram(int32 EntryPointIndex)
{
	UOptiXProgram* Program = nullptr;
	if (!ExceptionPrograms.IsValidIndex(EntryPointIndex))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXContext: Trying to get ray exception program from non-existing place! \n"));
	}
	try
	{
		NativeContext->getExceptionProgram(EntryPointIndex);
		Program = ExceptionPrograms[EntryPointIndex];
		// Todo: this could use some sweet little love
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Program;
}

void UOptiXContext::SetExceptionEnabled(RTexception Exception, bool Enabled)
{
	try
	{
		NativeContext->setExceptionEnabled(Exception, Enabled);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

bool UOptiXContext::GetExceptionEnabled(RTexception Exception)
{
	bool V = false; // TODO Look at all those shitty default values
	try
	{
		V = NativeContext->getExceptionEnabled(Exception);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return V;
}

// Uh oh

void UOptiXContext::SetRayTypeCount(int32 NumRayTypes)
{
	int32 Diff = NumRayTypes - GetRayTypeCount();
	if (Diff > 0)
	{
		MissPrograms.SetNum(NumRayTypes);
	}
	// TODO delete if diff < 0
	try
	{
		NativeContext->setRayTypeCount(static_cast<uint32>(NumRayTypes));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

int32 UOptiXContext::GetRayTypeCount()
{
	// TODO CAST
	int32 RTC = 0; static_cast<int32>(NativeContext->getRayTypeCount());
	try
	{
		RTC = static_cast<int32>(NativeContext->getRayTypeCount());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return RTC;
}


void UOptiXContext::SetMissProgram(int32 RayTypeIndex, UOptiXProgram* Program)
{
	if (!MissPrograms.IsValidIndex(RayTypeIndex))
	{
		UE_LOG(LogTemp, Error, TEXT("UOptiXContext: Trying to insert program in non-existing place! \n"));
		return;
	}
	try
	{
		NativeContext->setMissProgram(RayTypeIndex, Program->GetNativeProgram());
		MissPrograms.Insert(Program, RayTypeIndex);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXProgram* UOptiXContext::GetMissProgram(int32 RayTypeIndex)
{
	UOptiXProgram* Program = nullptr;
	if (!MissPrograms.IsValidIndex(RayTypeIndex))
	{
		UE_LOG(LogTemp, Error, TEXT("OptiXContext: Trying to get ray miss program from non-existing place! \n"));
	}
	try
	{
		NativeContext->getMissProgram(RayTypeIndex);
		Program = MissPrograms[RayTypeIndex];
		// Todo: this could use some sweet little love
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Program;
}

void UOptiXContext::Compile()
{
	try
	{
		NativeContext->compile();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::Launch(int32 EntryPointIndex, uint64 ImageWidth)
{
	try
	{
		NativeContext->launch(EntryPointIndex, ImageWidth);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::Launch(int32 EntryPointIndex, uint64 ImageWidth, uint64 ImageHeight)
{
	//UE_LOG(OptiXPluginContext, Warning, TEXT("Child count: %i"), GeometryGroupMap["top_object"]->GetChildCount());

	try
	{
		NativeContext->launch(EntryPointIndex, ImageWidth, ImageHeight);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::Launch(int32 EntryPointIndex, uint64 ImageWidth, uint64 ImageHeight, uint64 ImageDepth)
{
	try
	{
		NativeContext->launch(EntryPointIndex, ImageWidth, ImageHeight, ImageDepth);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetBuffer(FString Name, UOptiXBuffer * Buffer)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		NativeContext[N]->set(Buffer->GetNativeBuffer());
		Buffer->Name = Name;
		BufferMap.Add(Name, Buffer);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetGeometryGroup(FString Name, UOptiXGeometryGroup * GeoGroup)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		NativeContext[N]->set(GeoGroup->GetNativeGroup());
		GeometryGroupMap.Add(Name, GeoGroup);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetGroup(FString Name, UOptiXGroup * Group)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		NativeContext[N]->set(Group->GetNativeGroup());
		GroupMap.Add(Name, Group);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXProgram * UOptiXContext::CreateProgramFromPTXFile(FString Path, FString Name)
{
	UOptiXProgram* ProgramPtr = nullptr;
	try
	{
		ProgramPtr = NewObject<UOptiXProgram>(this, UOptiXProgram::StaticClass());
		std::string P = std::string(TCHAR_TO_ANSI(*Path));
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		optix::Program Prog = NativeContext->createProgramFromPTXFile(P, N);
		ProgramPtr->SetProgram(Prog);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return ProgramPtr;
}

UOptiXBuffer * UOptiXContext::CreateBuffer(uint8 Type, RTformat Format, RTsize Width)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type, Format, Width));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateBuffer(uint8 Type, RTformat Format, RTsize Width, RTsize Height)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type, Format, Width, Height));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateBuffer(uint8 Type, RTformat Format, RTsize Width, RTsize Height, RTsize Depth)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type, Format, Width, Height, Depth));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateBufferSimple(uint8 Type)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateBufferUByte(uint8 Type)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type, RT_FORMAT_UNSIGNED_BYTE4));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateBufferUByte1D(uint8 Type, int32 Width)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type, RT_FORMAT_UNSIGNED_BYTE4, Width));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateBufferUByte2D(uint8 Type, int32 Width, int32 Height)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type, RT_FORMAT_UNSIGNED_BYTE4, Width, Height));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateBufferUByte3D(uint8 Type, int32 Width, int32 Height, int32 Depth)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(Type, RT_FORMAT_UNSIGNED_BYTE4, Width, Height, Depth));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateOutputBufferIntersections(int32 Width, int32 Height)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBufferForCUDA(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL, RT_FORMAT_FLOAT4, Width, Height));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}


UOptiXBuffer * UOptiXContext::CreateOutputBufferColor(int32 Width, int32 Height)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBufferForCUDA(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL, RT_FORMAT_FLOAT4, Width, Height));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateOutputBufferUByte3D(int32 Width, int32 Height, int32 Depth)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(RT_BUFFER_OUTPUT, RT_FORMAT_UNSIGNED_BYTE4, Width, Height, Depth));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer* UOptiXContext::CreateOutputBufferDepth(int32 Width, int32 Height)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		//BufferPtr->SetBuffer(NativeContext->createBuffer(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL, RT_FORMAT_FLOAT, Width, Height));
		BufferPtr->SetBuffer(NativeContext->createBufferForCUDA(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL, RT_FORMAT_FLOAT, Width, Height));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateInputBufferFloat(int32 Width)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, Width));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateInputBufferFloat2D(int32 Width, int32 Height)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, Width, Height));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateInputBufferFloat3D(int32 Width, int32 Height, int32 Depth)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, Width, Height, Depth));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

UOptiXBuffer * UOptiXContext::CreateCubemapBuffer(int32 Width, int32 Height)
{
	UOptiXBuffer* BufferPtr = nullptr;
	try
	{
		// https://github.com/nvpro-samples/optix_advanced_samples/blob/master/src/optixIntroduction/optixIntro_07/src/Texture.cpp
		BufferPtr = NewObject<UOptiXBuffer>(this, UOptiXBuffer::StaticClass());
		BufferPtr->SetBuffer(NativeContext->createBuffer(RT_BUFFER_INPUT | RT_BUFFER_CUBEMAP, RT_FORMAT_UNSIGNED_BYTE4, Width, Height, 6)); // 6 slices
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return BufferPtr;
}

void UOptiXContext::SetMaxTraceDepth(int32 Depth)
{
	try
	{
		NativeContext->setMaxTraceDepth(static_cast<unsigned int>(Depth));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

int32 UOptiXContext::GetMaxTraceDepth()
{
	int32 Depth = 0;
	try
	{
		Depth = static_cast<int32>(NativeContext->getMaxTraceDepth()); // Loss of precision but won't happen ever anyway
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Depth;
}

void UOptiXContext::SetFloat3DVectorThreadsafe(FString string, FVector Var)
{
	VectorCache.Add(string, Var);
}

void UOptiXContext::SetMatrixThreadsafe(FString string, FMatrix Matrix)
{
	MatrixCache.Add(string, Matrix);
}

UOptiXTextureSampler * UOptiXContext::CreateTextureSampler()
{
	UOptiXTextureSampler* SamplerPtr = nullptr;
	try
	{
		SamplerPtr = NewObject<UOptiXTextureSampler>(this, UOptiXTextureSampler::StaticClass());
		SamplerPtr->SetTextureSampler(NativeContext->createTextureSampler());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return SamplerPtr;
}

void UOptiXContext::SetTextureSampler(FString Name, UOptiXTextureSampler * Sampler)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		NativeContext[N]->set(Sampler->GetNativeTextureSampler());
		TextureSamplerMap.Add(Name, Sampler);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXContext::SetSkybox(FString Name, UOptiXTextureSampler * Sampler)
{
	SetInt(Name, Sampler->GetId());
	TextureSamplerMap.Add(Name, Sampler);
}

UOptiXBuffer * UOptiXContext::GetBuffer(FString Name)
{
	if (BufferMap.Contains(Name))
	{
		return BufferMap[Name];
	}
	else
	{
		FString Message = "Buffer doesn't exist!";
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
		return nullptr;
	}
}

UOptiXGeometryGroup * UOptiXContext::GetGeometryGroup(FString Name)
{
	if (GeometryGroupMap.Contains(Name))
	{
		// Todo - check if that's actually the correct one! (GetNative etc)
		return GeometryGroupMap[Name];
	}
	else
	{
		FString Message = "Geometry Group doesn't exist!";
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
		return nullptr;
	}
}

UOptiXGroup * UOptiXContext::GetGroup(FString Name)
{
	if (GroupMap.Contains(Name))
	{
		// Todo - check if that's actually the correct one! (GetNative etc)
		return GroupMap[Name];
	}
	else
	{
		FString Message = "Group doesn't exist!";
		UE_LOG(OptiXPluginContext, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
		return nullptr;
	}
}

// Copy Paste too strong, could have shortened that quite a bit 

#include "OptiXGeometry.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXGeometry.h"
#include "OptiXModule.h"

// Needed for debugging
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

DEFINE_LOG_CATEGORY(OptiXPluginGeometry);

void UOptiXGeometry::BeginDestroy()
{
	// Tell optix to clean up
	UE_LOG(LogTemp, Display, TEXT("OptiX Geometry BeginDestroy"));
	// Remove all the children:
	DestroyOptiXObject();
	Super::BeginDestroy();
}

void UOptiXGeometry::DestroyOptiXObject()
{
	if (NativeGeometry != NULL)
	{
		//NativeGeometry->destroy();
		FOptiXModule::Get().GetOptiXContextManager()->GeometriesToDeleteQueue.Enqueue(NativeGeometry);

	}

	BufferMap.Empty();
	IntersectionProgram = nullptr;
	BoundingBoxProgram = nullptr;
	NativeGeometry = NULL;
}

void UOptiXGeometry::SetFloat(FString string, float Var)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetFloat2D(FString string, float Var1, float Var2)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetFloat3DVector(FString string, FVector Var)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetFloat3D(FString string, float Var1, float Var2, float Var3)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetFloat4DVector(FString string, FVector4 Var)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z, Var.W);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3, Var4);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetInt(FString string, int32 Var)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetInt2D(FString string, int32 Var1, int32 Var2)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetInt3DVector(FString string, FIntVector Var)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var.X, Var.Y, Var.Z);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2, Var3);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

//void UOptiXGeometry::SetInt4DVector(FString string, FIntVector4 Var)
//{
//	NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var.X, Var.Y, Var.Z, Var.W);
//
//}

void UOptiXGeometry::SetInt4D(FString string, int32 Var1, int32 Var2, int32 Var3, int32 Var4)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2, Var3, Var4);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetUint(FString string, uint8 Var)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetUint2D(FString string, uint8 Var1, uint8 Var2)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetUint3D(FString string, uint8 Var1, uint8 Var2, uint8 Var3)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2, Var3);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

void UOptiXGeometry::SetUint4D(FString string, uint8 Var1, uint8 Var2, uint8 Var3, uint8 Var4)
{
	try
	{
		NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2, Var3, Var4);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

// Todo: try-catch stuff for getters. Pretty low prio all in all

float UOptiXGeometry::GetFloat(FString string)
{
	return NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getFloat();
}

FVector2D UOptiXGeometry::GetFloat2D(FString string)
{
	optix::float2 V = NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getFloat2();
	return FVector2D(V.x, V.y);
}

FVector UOptiXGeometry::GetFloat3D(FString string)
{
	optix::float3 V = NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getFloat3();
	return FVector(V.x, V.y, V.z);
}

FVector4 UOptiXGeometry::GetFloat4D(FString string)
{
	optix::float4 V = NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getFloat4();
	return FVector4(V.x, V.y, V.z, V.w);
}

int32 UOptiXGeometry::GetInt(FString string)
{
	return NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getInt();
}

FIntPoint UOptiXGeometry::GetInt2D(FString string)
{
	optix::int2 V = NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getInt2();
	return FIntPoint(V.x, V.y);
}

FIntVector UOptiXGeometry::GetInt3D(FString string)
{
	optix::int3 V = NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getInt3();
	return FIntVector(V.x, V.y, V.z);
}

//FIntVector4 UOptiXGeometry::GetInt4D(FString string)
//{
//	optix::int4 V = NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getInt4();
//	return FIntVector4(V.x, V.y, V.z, V.w);
//}

//uint8 UOptiXGeometry::GetUint(FString string)
//{
//	return static_cast<uint8>(NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getUint());
//}
//
//void UOptiXGeometry::GetUint2D(FString & string, uint8 & Var1, uint8 & Var2)
//{
//	NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getUint(Var1, Var2);
//
//}
//
//void UOptiXGeometry::GetUint3D(FString string, uint8 & Var1, uint8 & Var2, uint8 & Var3)
//{
//	NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getUint(Var1, Var2, Var3);
//}
//
//FUintVector4 UOptiXGeometry::GetUint4D(FString string)
//{
//	optix::uint4 V = NativeGeometry[std::string(TCHAR_TO_ANSI(*string))]->getUint4();
//	return FUintVector4(V.x, V.y, V.z, V.w);
//}


optix::Geometry UOptiXGeometry::GetNativeGeometry()
{
	return NativeGeometry;
}

void UOptiXGeometry::SetPrimitiveCount(int32 NumPrimitives)
{
	if (NumPrimitives < 0)
	{
		UE_LOG(OptiXPluginGeometry, Error, TEXT("Trying to set negative primitive count in geometry: %s"));
		return;
	}
	try
	{
		NativeGeometry->setPrimitiveCount(static_cast<uint32>(NumPrimitives));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

int32 UOptiXGeometry::GetPrimitiveCount()
{
	int32 PrimitiveCount = 0;
	try
	{
		PrimitiveCount = static_cast<int32>(NativeGeometry->getPrimitiveCount());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return PrimitiveCount;
}

void UOptiXGeometry::SetPrimitiveIndexOffset(int32 IndexOffsets)
{
	if (IndexOffsets < 0)
	{
		UE_LOG(OptiXPluginGeometry, Error, TEXT("Trying to set negative primitive index offset in geometry!"));
		return; // TODO Throw error
	}
	try
	{
		NativeGeometry->setPrimitiveIndexOffset(static_cast<uint32>(IndexOffsets));
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

int32 UOptiXGeometry::GetPrimitiveIndexOffset()
{
	int32 PrimitiveIndexOffset = 0;
	try
	{
		PrimitiveIndexOffset = static_cast<int32>(NativeGeometry->getPrimitiveIndexOffset());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return PrimitiveIndexOffset;
}

void UOptiXGeometry::SetPrimitiveCountUint(uint32 NumPrimitives)
{
	try
	{
		NativeGeometry->setPrimitiveCount(NumPrimitives);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

uint32 UOptiXGeometry::GetPrimitiveCountUint()
{
	uint32 PrimitiveCount = 0;
	try
	{
		PrimitiveCount = NativeGeometry->getPrimitiveCount();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return PrimitiveCount;
}

void UOptiXGeometry::SetPrimitiveIndexOffsetUint(uint32 IndexOffsets)
{
	try
	{
		NativeGeometry->setPrimitiveIndexOffset(IndexOffsets);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

uint32 UOptiXGeometry::GetPrimitiveIndexOffsetUint()
{
	uint32 PrimitiveIndexOffset = 0;
	try
	{
		PrimitiveIndexOffset = NativeGeometry->getPrimitiveIndexOffset();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return PrimitiveIndexOffset;
}

void UOptiXGeometry::SetMotionRange(float TimeBegin, float TimeEnd)
{
	try
	{
		NativeGeometry->setMotionRange(TimeBegin, TimeEnd);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

// Todo: Add try-catch stuff, but the motion range things are fairly low prio as well for now.

FVector2D UOptiXGeometry::GetMotionRange()
{
	float f1, f2;
	NativeGeometry->getMotionRange(f1, f2);
	return FVector2D(f1, f2);
}

void UOptiXGeometry::SetMotionBorderMode(RTmotionbordermode BeginMode, RTmotionbordermode EndMode)
{
	NativeGeometry->setMotionBorderMode(BeginMode, EndMode);
}

void UOptiXGeometry::GetMotionBorderMode(RTmotionbordermode & BeginMode, RTmotionbordermode & EndMode)
{
	NativeGeometry->getMotionBorderMode(BeginMode, EndMode);
}

void UOptiXGeometry::SetMotionSteps(int32 N)
{
	if (N < 0)
	{
		return; // TODO Throw error
	}
	NativeGeometry->setMotionSteps(static_cast<uint32>(N));
}

int32 UOptiXGeometry::GetMotionSteps()
{
	return static_cast<int32>(NativeGeometry->getMotionSteps());
}

void UOptiXGeometry::SetMotionStepsUint(uint32 N)
{
	NativeGeometry->setMotionSteps(N);
}

uint32 UOptiXGeometry::GetMotionStepsUint()
{
	return NativeGeometry->getMotionSteps();
}

void UOptiXGeometry::SetBoundingBoxProgram(UOptiXProgram* Program)
{
	try
	{
		BoundingBoxProgram = Program;
		NativeGeometry->setBoundingBoxProgram(Program->GetNativeProgram());	
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXProgram* UOptiXGeometry::GetBoundingBoxProgram()
{
	// Done to see if the optix bb prog is actually valid.
	UOptiXProgram* Prog = nullptr;
	try
	{
		NativeGeometry->getBoundingBoxProgram(); // todo: comparison check
		Prog = BoundingBoxProgram;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Prog;
}

void UOptiXGeometry::SetIntersectionProgram(UOptiXProgram* Program)
{
	try
	{
		IntersectionProgram = Program;
		NativeGeometry->setIntersectionProgram(Program->GetNativeProgram());
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

UOptiXProgram* UOptiXGeometry::GetIntersectionProgram()
{
	UOptiXProgram* Prog = nullptr;
	try
	{
		NativeGeometry->getIntersectionProgram(); // todo: comparison check
		Prog = IntersectionProgram;
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Prog;
}

void UOptiXGeometry::MarkDirty()
{
	try
	{
		NativeGeometry->markDirty();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

bool UOptiXGeometry::IsDirty()
{
	bool Dirty = false;
	try
	{
		Dirty = NativeGeometry->isDirty();
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
	return Dirty;
}

void UOptiXGeometry::SetBuffer(FString Name, UOptiXBuffer* Buffer)
{
	try
	{
		std::string N = std::string(TCHAR_TO_ANSI(*Name));
		NativeGeometry[N]->setBuffer(Buffer->GetNativeBuffer());
		BufferMap.Add(Name, Buffer);
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(OptiXPluginGeometry, Error, TEXT("OptiX Error: %s"), *Message);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OptiX Error %s"), *Message));
	}
}

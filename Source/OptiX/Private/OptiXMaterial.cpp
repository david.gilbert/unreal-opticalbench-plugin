#include "OptiXMaterial.h"

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"

#include "OptiXModule.h"



void UOptiXMaterial::BeginDestroy()
{
	// Tell optix to clean up
	UE_LOG(LogTemp, Warning, TEXT("OptiX Material BeginDestroy"));

	DestroyOptiXObject();

	Super::BeginDestroy();
}

void UOptiXMaterial::DestroyOptiXObject()
{
	if (NativeMaterial != NULL)
	{
		//NativeMaterial->destroy();
		FOptiXModule::Get().GetOptiXContextManager()->MaterialsToDeleteQueue.Enqueue(NativeMaterial);
	}

	NativeMaterial = NULL;
	ClosestHitPrograms.Empty();
	AnyHitProgram = nullptr;
}

void UOptiXMaterial::SetFloat(FString string, float Var)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var);
}

void UOptiXMaterial::SetFloat2D(FString string, float Var1, float Var2)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2);

}

void UOptiXMaterial::SetFloat3DVector(FString string, FVector Var)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z);

}

void UOptiXMaterial::SetFloat3D(FString string, float Var1, float Var2, float Var3)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3);

}

void UOptiXMaterial::SetFloat4DVector(FString string, FVector4 Var)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var.X, Var.Y, Var.Z, Var.W);

}

void UOptiXMaterial::SetFloat4D(FString string, float Var1, float Var2, float Var3, float Var4)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setFloat(Var1, Var2, Var3, Var4);

}

void UOptiXMaterial::SetInt(FString string, int32 Var)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var);

}

void UOptiXMaterial::SetInt2D(FString string, int32 Var1, int32 Var2)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2);

}

void UOptiXMaterial::SetInt3DVector(FString string, FIntVector Var)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var.X, Var.Y, Var.Z);

}

void UOptiXMaterial::SetInt3D(FString string, int32 Var1, int32 Var2, int32 Var3)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2, Var3);

}

//void UOptiXMaterial::SetInt4DVector(FString string, FIntVector4 Var)
//{
//	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var.X, Var.Y, Var.Z, Var.W);
//
//}

void UOptiXMaterial::SetInt4D(FString string, int32 Var1, int32 Var2, int32 Var3, int32 Var4)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setInt(Var1, Var2, Var3, Var4);

}

void UOptiXMaterial::SetUint(FString string, uint8 Var)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var);
}

void UOptiXMaterial::SetUint2D(FString string, uint8 Var1, uint8 Var2)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2);

}

void UOptiXMaterial::SetUint3D(FString string, uint8 Var1, uint8 Var2, uint8 Var3)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2, Var3);

}

void UOptiXMaterial::SetUint4D(FString string, uint8 Var1, uint8 Var2, uint8 Var3, uint8 Var4)
{
	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->setUint(Var1, Var2, Var3, Var4);

}

float UOptiXMaterial::GetFloat(FString string)
{
	return NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getFloat();
}

FVector2D UOptiXMaterial::GetFloat2D(FString string)
{
	optix::float2 V = NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getFloat2();
	return FVector2D(V.x, V.y);
}

FVector UOptiXMaterial::GetFloat3D(FString string)
{
	optix::float3 V = NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getFloat3();
	return FVector(V.x, V.y, V.z);
}

FVector4 UOptiXMaterial::GetFloat4D(FString string)
{
	optix::float4 V = NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getFloat4();
	return FVector4(V.x, V.y, V.z, V.w);
}

int32 UOptiXMaterial::GetInt(FString string)
{
	return NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getInt();
}

FIntPoint UOptiXMaterial::GetInt2D(FString string)
{
	optix::int2 V = NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getInt2();
	return FIntPoint(V.x, V.y);
}

FIntVector UOptiXMaterial::GetInt3D(FString string)
{
	optix::int3 V = NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getInt3();
	return FIntVector(V.x, V.y, V.z);
}

//FIntVector4 UOptiXMaterial::GetInt4D(FString string)
//{
//	optix::int4 V = NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getInt4();
//	return FIntVector4(V.x, V.y, V.z, V.w);
//}

//uint8 UOptiXMaterial::GetUint(FString string)
//{
//	return static_cast<uint8>(NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getUint());
//}
//
//void UOptiXMaterial::GetUint2D(FString & string, uint8 & Var1, uint8 & Var2)
//{
//	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getUint(Var1, Var2);
//
//}
//
//void UOptiXMaterial::GetUint3D(FString string, uint8 & Var1, uint8 & Var2, uint8 & Var3)
//{
//	NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getUint(Var1, Var2, Var3);
//}
//
//FUintVector4 UOptiXMaterial::GetUint4D(FString string)
//{
//	optix::uint4 V = NativeMaterial[std::string(TCHAR_TO_ANSI(*string))]->getUint4();
//	return FUintVector4(V.x, V.y, V.z, V.w);
//}


void UOptiXMaterial::SetClosestHitProgram(uint32 RayTypeIndex, UOptiXProgram* Program)
{
	try
	{
		NativeMaterial->setClosestHitProgram(RayTypeIndex, Program->GetNativeProgram());
		if (ClosestHitPrograms.IsValidIndex(RayTypeIndex))
		{
			ClosestHitPrograms[RayTypeIndex] = Program;
		}
		else
		{
			ClosestHitPrograms.SetNum(RayTypeIndex + 1);
			ClosestHitPrograms[RayTypeIndex] = Program;
		}
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(LogTemp, Error, TEXT("OptiX Error: %s"), *Message);
	}
}

UOptiXProgram* UOptiXMaterial::GetClosestHitProgram(uint32 RayTypeIndex)
{
	UOptiXProgram* P = nullptr;
	try
	{
		optix::Program Native = NativeMaterial->getClosestHitProgram(RayTypeIndex);
		if (ClosestHitPrograms.IsValidIndex(RayTypeIndex))
		{
			P = ClosestHitPrograms[RayTypeIndex];
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("OptiX Error: Wrong index in GetClosestHitProgram"));
		}
	}
	catch (optix::Exception& E)
	{
		FString Message = FString(E.getErrorString().c_str());
		UE_LOG(LogTemp, Error, TEXT("OptiX Error: %s"), *Message);
	}
	return P;
}

void UOptiXMaterial::SetAnyHitProgram(uint32 RayTypeIndex, UOptiXProgram* Program)
{
	AnyHitProgram = Program;
	NativeMaterial->setAnyHitProgram(RayTypeIndex, Program->GetNativeProgram());
}

UOptiXProgram* UOptiXMaterial::GetAnyHitProgram(uint32 RayTypeIndex)
{
	return AnyHitProgram;
}
